package com.likethatapps.services.api.model.abtest;

/**
 * Created by eladyarkoni on 11/12/14.
 */
public class UserDataModel {

    private String userId;
    private TierModel tier1;
    private TierModel tier2;

    public String getUserId() {
        return userId;
    }

    public TierModel getTier1() {
        return tier1;
    }

    public TierModel getTier2() {
        return tier2;
    }
}
