package com.tapreason.view.util;

import java.util.List;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PorterDuff.Mode;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.widget.TextView;

import com.tapreason.sdk.TapReason;
import com.tapreason.sdk.TapReasonGeneralCons.TapReasonAppType;

public class TapReasonUtils
{
	public static final String TAP_REASON_SDK_LOG_TAG = "TapReason";
	private static final String[] EMAIL_PACKAGES = new String[]{TapReasonAppType.EMAIL.getDefaultPackage(),"com.yahoo.mobile.client.android.mail","com.outlook.Z7", "ru.mail.mailapp"}; 
	
    public static boolean isOutOfBounds(MotionEvent event, Context context, Window window ) 
    {
        final int x = (int) event.getX();
        final int y = (int) event.getY();
        final int slop = ViewConfiguration.get(context).getScaledWindowTouchSlop();
        final View decorView = window.getDecorView();
        return (x < -slop) || (y < -slop)
                || (x > (decorView.getWidth()+slop))
                || (y > (decorView.getHeight()+slop));
    }
    
    public static void setTextViewStyle( TextView text, boolean isBold )
    {
		if ( isBold )
		{
			text.setTypeface(text.getTypeface(), Typeface.BOLD); 
		}
		else
		{
			text.setTypeface(text.getTypeface(), Typeface.NORMAL); 
		}	
    }

	public static void setBackgroundColor( View view, int newColor )
    {
    	setBackgroundColor( view, newColor, 0 );
    }
    
	public static void setBackgroundColor( View view, 
			int newColor,
			int selectionColor )
	{
		setBackgroundColor( view, newColor, selectionColor, 0 );
	}
			
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@SuppressWarnings("deprecation")
	public static void setBackgroundColor( View view, 
											int bgColor, 
											int selectionColor,
											int strokeColorResource )
    {
		GradientDrawable gd = new GradientDrawable( );
		gd.mutate();
		gd.setShape( GradientDrawable.RECTANGLE );
		gd.setColor(bgColor);
		
		if ( strokeColorResource != 0  )
		{
			gd.setStroke(1, view.getResources().getColor( strokeColorResource ) );	
		}
		
		StateListDrawable states = new StateListDrawable();
		
		if ( view.isClickable() )
		{	        	
	        states.addState(new int[] {android.R.attr.state_pressed}, new ColorDrawable( selectionColor ) );
		}
		
        states.addState(new int[] {}, gd );
		
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN )
		{
        	view.setBackground( states );
		}
        else
        {
        	view.setBackgroundDrawable(states);
        }		
    }
    
	public static Intent findIntentForPackage( final PackageManager packageManager, String packageName ) 
	{
		if ( packageName == null )
		{
			return null;
		}
		
		Intent intent = new Intent( Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.setPackage( packageName );
		
		List<ResolveInfo> intentActivitiesList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
 
		if ( intentActivitiesList != null && !intentActivitiesList.isEmpty() )
		{
			return intent;
		}
				
		return null;
	}	
	
	public static String getDefaultEmailPackage( final PackageManager packageManager ) 
	{
		String foundPackageName = null;
		for ( String packageName : EMAIL_PACKAGES )
		{
			if ( findIntentForPackage( packageManager, packageName ) != null )
			{
				foundPackageName = packageName;
				break;
			}
		}	
		return foundPackageName;
	}
	
	public static void changeDrawableColor( Drawable drawable, int newColor )
	{
		drawable.setColorFilter( newColor, Mode.MULTIPLY );
	}
	
	public static Intent generateFeedbackEmailIntent( final PackageManager packageManager, String packageName, String title )
	{
		Intent emailIntent = findIntentForPackage( packageManager, packageName );
		
		if ( emailIntent == null )
		{
			return null;
		}		
		
		emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{TapReason.getConf().getSupportEmail()});
		
		if ( TapReason.getConf().isMonitorUserFeedback() && !TapReason.getConf().isDefaultSupportMail() )
		{
			emailIntent.putExtra(Intent.EXTRA_CC, new String[]{TapReason.getConf().getDefaultSupportEmail()});
		}
		
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, title );
		emailIntent.setType("message/text");
		
		return emailIntent;
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@SuppressWarnings("deprecation")
	public static void createRoundView( View view, int backgroundColor, int strokeColor )
	{
		GradientDrawable gd = new GradientDrawable( );
		gd.mutate();
		gd.setShape( GradientDrawable.OVAL );
		gd.setColor(backgroundColor);
		
		if ( strokeColor != 0 )
		{
			gd.setStroke(3, strokeColor);			
		}		
	        
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN )
		{
        	view.setBackground( gd );
		}
        else
        {
        	view.setBackgroundDrawable( gd );
        }
	
	}
	
	public static boolean isExternalStorageReadable( PackageManager pm, String appPackage ) 
	{
	    try
		{
			// Only from KitKat you don't have to request the permission of android.permission.WRITE_EXTERNAL_STORAGE in order to
			// write to external storage. Important to write to external, since we need this file to be
			// available for the app that we are sharing to
			if ( canAccessExternalStorage( pm, appPackage )  )
			{
				String state = Environment.getExternalStorageState();
				if (Environment.MEDIA_MOUNTED.equals(state) ||
				    Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) 
				{
				    return true;
				}
			}
		}
		catch ( Throwable tr ){}
	    
	    return false;
	}	
	
	private static boolean canAccessExternalStorage( PackageManager pm, String appPackage )
	{
		boolean hasPermission = false;
		
		/*
		 * Any app that declares the WRITE_EXTERNAL_STORAGE permission is implicitly granted this permission.
			This permission is enforced starting in API level 19. Before API level 19, this permission is not enforced and all apps still have access to read from external storage. You can test your app with the permission enforced by enabling Protect USB storage under Developer options in the Settings app on a device running Android 4.1 or higher.
			Also starting in API level 19, this permission is not required to read/write files in your application-specific directories returned by getExternalFilesDir(String) and getExternalCacheDir().
		 */
		if ( Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT && pm.checkPermission( Manifest.permission.WRITE_EXTERNAL_STORAGE, appPackage ) == PackageManager.PERMISSION_GRANTED)
		{
			hasPermission = true;
		}
		// The permission is written in string since android.permission.READ_EXTERNAL_STORAGE is available only from 16		
		else if (( pm.checkPermission( Manifest.permission.WRITE_EXTERNAL_STORAGE, appPackage ) == PackageManager.PERMISSION_GRANTED ) &&
				(pm.checkPermission( "android.permission.READ_EXTERNAL_STORAGE", appPackage ) == PackageManager.PERMISSION_GRANTED ))
		{
			hasPermission = true;
		}
				
		if  (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT || hasPermission) 
		{
			return true;
		}
		
		return false;
	}	
}
