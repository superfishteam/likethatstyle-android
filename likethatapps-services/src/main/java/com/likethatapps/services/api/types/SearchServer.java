package com.likethatapps.services.api.types;

/**
 * Created by eladyarkoni on 9/8/14.
 */
public enum SearchServer {
    MARLIN,
    GINN,
    GARDENING,
    TATTOO,
    STYLE
}
