package com.likethatapps.services.api;

import android.content.Context;
import android.util.Log;

import com.likethatapps.services.api.http.Ajax;
import com.likethatapps.services.api.model.abtest.UserDataModel;

import org.json.JSONObject;

/**
 * Created by eladyarkoni on 5/21/14.
 */
public class StyleReportServiceImpl extends BaseService implements IReportService {

    private static final String REPORT_ACTION_URL = "http://www.likethatapps.com/style/action";
    private static final String TAG = "report-service";

    public StyleReportServiceImpl(Context context, String osVersion, String userId, String appName, String appVersion, String deviceModel) {
        super(context, osVersion, appName, appVersion, userId, deviceModel);
    }

    @Override
    public void reportActionEvent(int id, String action, String sessionId, String searchId, String imageId, String category, String extraData) {
        JSONObject obj = new JSONObject();
        try {
            if (id != 0) {
                obj.put("id", id);
            }
            obj.put("action", action);
            if (sessionId != null) {
                obj.put("sessionId", sessionId);
            }
            if (searchId != null) {
                obj.put("searchId", searchId);
            }
            if (imageId != null) {
                obj.put("imageId", imageId);
            }
            if (category != null) {
                obj.put("category", category);
            }
            if (extraData != null) {
                obj.put("extraData", extraData);
            }
            if (SFApi.getInstance().getAbTestService() != null) {
                UserDataModel userData = SFApi.getInstance().getAbTestService().getUserData();
                if (userData != null) {
                    try {
                        obj.put("tier1_bucket", userData.getTier1().getBucket());
                        obj.put("tier2_bucket", userData.getTier2().getBucket());
                        obj.put("tier1_curr_group", userData.getTier1().getGroup());
                        obj.put("tier2_curr_group", userData.getTier2().getGroup());
                        obj.put("tier1_prev_group", userData.getTier1().getPreviousGroup());
                        obj.put("tier2_prev_group", userData.getTier2().getPreviousGroup());
                        obj.put("tier1_next_group", userData.getTier1().getNextGroup());
                        obj.put("tier2_next_group", userData.getTier2().getNextGroup());
                    } catch (NullPointerException ex){
                        Log.e(TAG, "abtest data is corrupted");
                    }
                }
            }
            buildObjectParams(obj);
            Ajax.post(REPORT_ACTION_URL, obj);
            StringBuilder log = new StringBuilder();
            log.append("(").append(id).append(")").append(action).append(": ");
            log.append(" searchId: ").append(searchId);
            log.append(" imageId: ").append(imageId);
            log.append(" category: ").append(category);
            log.append(" extraData: ").append(extraData);
            Log.d(TAG, log.toString());
        } catch (Exception ex) {}
    }
}
