package com.likethatapps.style;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.etsy.android.grid.StaggeredGridView;
import com.likethatapps.style.model.PhotoModel;
import com.likethatapps.style.service.ReportService;
import com.likethatapps.style.service.WatchListService;

import java.util.List;

/**
 * Created by eladyarkoni on 7/6/15.
 */
public class WatchListActivity extends StyleBaseActivity implements AdapterView.OnItemClickListener, ResultImageAdapter.ResultImageAdapterActivity {

    private ResultImageAdapter mAdapter;
    private List<PhotoModel> mWatchList;
    private View mEmptyMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        onCreateBaseActivity(this, savedInstanceState, R.layout.activity_watch_list);
        mEmptyMessage = findViewById(R.id.empty_message);
        render();
    }

    @Override
    protected String getBarTitle() {
        return getString(R.string.title_watch_list_activity);
    }

    @Override
    public void onReachedBottomList() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        ReportService.getInstance().report(3039, "watchlist_screen_opened");
        updateList();
    }

    public void updateList() {
        mWatchList = WatchListService.getInstance().getWatchList();
        mAdapter.notifyDataSetChanged();
        if (mWatchList.size() == 0) {
            mEmptyMessage.setVisibility(View.VISIBLE);
        } else {
            mEmptyMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFirstImageLoaded() {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        PhotoModel photoModel = mWatchList.get(i);
        Bundle mBundle = new Bundle();
        mBundle.putParcelable(Consts.EXTRA_DATA_PHOTO_MODEL_SERIALIZABLE, photoModel);
        navigateToActivity(ProductActivity.class, mBundle, true, -1);
    }


    private void render() {
        mWatchList = WatchListService.getInstance().getWatchList();
        StaggeredGridView gridView = (StaggeredGridView)findViewById(R.id.grid_view);
        mAdapter = new ResultImageAdapter(this, mWatchList, false, false, false);
        gridView.setOnItemClickListener(this);
        gridView.setAdapter(mAdapter);
        if (mWatchList.size() == 0) {
            mEmptyMessage.setVisibility(View.VISIBLE);
        } else {
            mEmptyMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onWatchListItemRemoved(PhotoModel photo) {
        render();
    }

    @Override
    public void onWatchListItemInserted(PhotoModel photo) {
        render();
    }

    @Override
    public void onAskExpertButtonClicked() {

    }
}
