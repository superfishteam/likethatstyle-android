package com.likethatapps.services.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by eladyarkoni on 4/29/15.
 */
public class Utils {

    public static void openAppByPackageName(Context context, String appPackageName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
