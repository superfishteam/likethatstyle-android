package com.likethatapps.services.api.model.gallery;

import java.util.List;

/**
 * Created by eladyarkoni on 9/8/14.
 */
public class GalleryModel {

    private String name;
    private List<String> photos;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }
}
