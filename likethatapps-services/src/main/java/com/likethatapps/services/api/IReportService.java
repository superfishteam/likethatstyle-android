package com.likethatapps.services.api;

/**
 * Created by eladyarkoni on 5/21/14.
 */
public interface IReportService {

    public void reportActionEvent(int id, String action, String sessionId, String searchId, String imageId, String category, String extraData);
}
