package com.likethatapps.style;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.etsy.android.grid.StaggeredGridView;
import com.google.gson.Gson;
import com.likethatapps.services.api.SFApi;
import com.likethatapps.services.api.http.AjaxCallback;
import com.likethatapps.services.api.types.SearchSource;
import com.likethatapps.services.utils.ImageUtils;
import com.likethatapps.style.model.PhotoModel;
import com.likethatapps.style.model.ResultModel;
import com.likethatapps.style.service.ReportService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class StyleResultsActivity extends StyleBaseActivity implements AdapterView.OnItemClickListener, ResultImageAdapter.ResultImageAdapterActivity {

    private StaggeredGridView mGridView;
    private ResultImageAdapter mAdapter;
    private String mSearchId;
    private SearchSource mSearchSource;
    private String mSearchCroppedImageUrl;
    private View mSearchLoadingView;
    private Spinner mSortSpinner;
    private Spinner mSizesSpinner;
    private int mNumOfSearches;
    private String mCurrentScope;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreateBaseActivity(this, savedInstanceState, R.layout.activity_style_results);
        mCurrentScope = SFApi.getInstance().getSearchService().getScope();
        mNumOfSearches = 1;
        mSearchLoadingView = findViewById(R.id.loading_view);
        boolean isFirstTime = getIntent().getBooleanExtra(Consts.EXTRA_DATA_FIRST_TIME_BOOLEAN, false);
        String results = getIntent().getStringExtra(Consts.EXTRA_DATA_RESULTS_JSON);
        String source = getIntent().getStringExtra(Consts.EXTRA_DATA_SEARCH_SOURCE);
        mSearchSource = source != null ? SearchSource.parse(source) : SearchSource.CAMERA;
        ResultModel resultsModel = new Gson().fromJson(results, ResultModel.class);
        mSearchId = resultsModel.getSearchId();
        Log.d("style-result-activity", "search id: " + mSearchId);
        mSearchCroppedImageUrl = resultsModel.getCroppedImageUrl();
        mGridView =  ((StaggeredGridView)findViewById(R.id.grid_view));
        mGridView.addHeaderView(getHeaderLayout(resultsModel, getIntent().getByteArrayExtra(Consts.EXTRA_DATA_IMAGE_BYTE_ARRAY)), null, false);
        mGridView.addFooterView(getFooterLayout(), null, false);
        mAdapter = new ResultImageAdapter(this, resultsModel.getPhotos(), true, true, true);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(this);
        if (isFirstTime) {
            showTooltip(R.layout.srp_first_tooltip, getCameraSearchButton(), false);
        }
    }

    protected void searchAgain(String newScope) {
        mGridView.setVisibility(View.GONE);
        mSearchLoadingView.setVisibility(View.VISIBLE);
        mNumOfSearches++;
        SFApi.getInstance().getSearchService().setScope(newScope);
        mCurrentScope = newScope;
        SFApi.getInstance().getSearchService().startSearch(mSearchCroppedImageUrl, mSearchSource, null, null, null, null, 100, null, new AjaxCallback() {
            @Override
            public void success(String result) {
                onSearchSuccess(result);
            }

            @Override
            public void error(String error, int code) {
                super.error(error, code);
                onSearchError();
            }

            @Override
            public void timeout(int retry) {
                super.timeout(retry);
                ReportService.getInstance().report(3037, "connection_timeout_event");
                onSearchError();
            }

            @Override
            public void retry(int retry) {
                super.retry(retry);
                ReportService.getInstance().report(3038, "connection_retry_event");
                onSearchError();
            }
        });
    }

    protected void onSearchSuccess(String result) {
        final ResultModel resultsModel = new Gson().fromJson(result, ResultModel.class);
        mSearchId = resultsModel.getSearchId();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mGridView.setVisibility(View.VISIBLE);
                mSearchLoadingView.setVisibility(View.GONE);
                mAdapter = new ResultImageAdapter(StyleResultsActivity.this, resultsModel.getPhotos(), true, true, true);
                mGridView.setAdapter(mAdapter);
                mSortSpinner.setSelection(0);
            }
        });
    }

    protected void onSearchError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mGridView.setVisibility(View.VISIBLE);
                mSearchLoadingView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onFirstImageLoaded() {
        long pageLoadingTime = System.currentTimeMillis();
        long startSearchTime = getIntent().getLongExtra(Consts.EXTRA_DATA_SERVER_START_TIME_LONG, -1);
        long endSearchTime = getIntent().getLongExtra(Consts.EXTRA_DATA_SERVER_RESPONSE_TIME_LONG, -1);
        if (startSearchTime != -1 && endSearchTime != -1 && mNumOfSearches == 1) {
            Map<String, String> extraData = new HashMap<>();
            extraData.put("ServerResponseTime", (endSearchTime - startSearchTime) + "");
            extraData.put("PageLoadingTime", (pageLoadingTime - startSearchTime)+"");
            extraData.put("NetworkType", SFApi.getInstance().getNetworkType(this));
            ReportService.getInstance().report(3032, "srp_loading_time_event", mSearchId, null, extraData);
        }
    }

    @Override
    public void onWatchListItemInserted(PhotoModel photo) {
        showTooltip(R.layout.watchlist_eye_tooltip, findViewById(R.id.action_bar_btn_eye), true);
    }

    @Override
    public void onWatchListItemRemoved(PhotoModel photo) {
        showTooltip(R.layout.watchlist_eye_tooltip_removed, findViewById(R.id.action_bar_btn_eye), true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ReportService.getInstance().report(3025, "srp_screen_opened");
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PhotoModel photo = (PhotoModel) mAdapter.getItem(position);
        if (photo != null) {
            Map<String, String> extraData = new HashMap<String, String>();
            extraData.put("TapPosition", position+"");
            extraData.put("DisplayLocation", "(1," + position +")");
            extraData.put("ImageURL", photo.getUrl());
            ReportService.getInstance().report(3020, "srp_photo_clicked", mSearchId, photo.getStatsImageId(), extraData);
            Bundle intentBundle = new Bundle();
            intentBundle.putSerializable(Consts.EXTRA_DATA_PHOTO_MODEL_SERIALIZABLE, photo);
            intentBundle.putString(Consts.EXTRA_DATA_SEARCH_ID_STRING, mSearchId);
            navigateToActivity(ProductActivity.class, intentBundle, false, -1);
        }
    }

    @Override
    protected String getBarTitle() {
        return getString(R.string.srp_page_title);
    }

    protected View getHeaderLayout(ResultModel resultsModel, byte[] searchBitmap) {
        View convertView = getLayoutInflater().inflate(R.layout.result_based_image_layout, mGridView, false);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
        RelativeLayout imageRelativeContainer = (RelativeLayout) convertView.findViewById(R.id.image_relative);
        if (searchBitmap != null) {
            imageView.setImageBitmap(ImageUtils.getBitmap(searchBitmap));
        } else {
            StyleApplication.imageLoader.DisplayImage(resultsModel.getCroppedImageUrl(), imageView);
        }
        ((TextView)convertView.findViewById(R.id.we_found_title)).setText(getString(R.string.we_found_string, resultsModel.getPhotos().size()+""));
        // render sort spinner
        mSortSpinner = (Spinner)convertView.findViewById(R.id.sort_spinner);
        ArrayAdapter spinnerAdapter = new ArrayAdapter<String>(this, R.layout.sort_by_spinner_item, Arrays.asList(
                getString(R.string.sort_by_similarity),
                getString(R.string.sort_by_price_low_to_high),
                getString(R.string.sort_by_price_high_to_low)
        ));
        spinnerAdapter.setDropDownViewResource(R.layout.sort_by_spinner_dropdown_item);
        mSortSpinner.setAdapter(spinnerAdapter);
        mSortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mAdapter.sortBy(ResultImageAdapter.SORT_BY_SIMILARITY);
                } else if (position == 1) {
                    mAdapter.sortBy(ResultImageAdapter.SORT_BY_PRICE_LOW_TO_HIGH);
                } else if (position == 2) {
                    mAdapter.sortBy(ResultImageAdapter.SORT_BY_PRICE_HIGH_TO_LOW);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // render sizes spinner
        mSizesSpinner = (Spinner)convertView.findViewById(R.id.sizes_spinner);
        ArrayAdapter sizesAdapter = new ArrayAdapter<String>(this, R.layout.sizes_spinner_item, Arrays.asList(
                getString(R.string.sizes_standard),
                getString(R.string.sizes_plus),
                getString(R.string.sizes_maternity)
        ));
        sizesAdapter.setDropDownViewResource(R.layout.sort_by_spinner_dropdown_item);
        mSizesSpinner.setAdapter(sizesAdapter);
        if (mCurrentScope.equals("style")) {
            mSizesSpinner.setSelection(0);
        } else if (mCurrentScope.equals("xxl")) {
            mSizesSpinner.setSelection(1);
        } else if (mCurrentScope.equals("maternity")) {
            mSizesSpinner.setSelection(2);
        }
        mSizesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    if (!mCurrentScope.equals("style")) {
                        ReportService.getInstance().report(3074, "size_standard_tapped");
                        searchAgain("style");
                    }
                } else if (position == 1) {
                    if (!mCurrentScope.equals("xxl")) {
                        ReportService.getInstance().report(3075, "size_plus_tapped");
                        searchAgain("xxl");
                    }
                } else if (position == 2) {
                    if (!mCurrentScope.equals("maternity")) {
                        ReportService.getInstance().report(3076, "size_maternity_tapped");
                        searchAgain("maternity");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return convertView;
    }

    @Override
    public void onReachedBottomList() {
        ReportService.getInstance().report(3072, "bottom_SRP_viewed", mSearchId, null, null);
    }

    protected View getFooterLayout() {
        View convertView = getLayoutInflater().inflate(R.layout.srp_footer, mGridView, false);
        convertView.findViewById(R.id.try_again_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReportService.getInstance().report(3014, "feed_camera_clicked");
                navigateToActivity(CameraActivity.class, null, true, Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
        });
        return convertView;
    }

    @Override
    public void onAskExpertButtonClicked() {
        ReportService.getInstance().report(3078, "SRP_aae_tapped", mSearchId, null, null);
        StyleApplication.askExpertMail(this, mSearchId, mSearchCroppedImageUrl);
    }

}
