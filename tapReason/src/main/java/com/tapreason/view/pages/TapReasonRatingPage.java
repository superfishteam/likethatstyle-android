package com.tapreason.view.pages;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.tapreason.R;
import com.tapreason.sdk.TapReason;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventResult;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventSubResult;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventTypes;
import com.tapreason.sdk.TapReasonConfiguration.TapReasonPageConf;
import com.tapreason.view.base.TapReasonActivity;
import com.tapreason.view.util.TapReasonUtils;

final class TapReasonRatingPage extends TapReasonRulePageBase
{
	public TapReasonRatingPage( Bundle params, TapReasonEventTypes eventType )
	{
		super( params, eventType, TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_1 );
	}

	@SuppressLint("ResourceAsColor")
	@Override
	protected void configurePage( TapReasonActivity activity )
	{
		try
		{
			TapReasonPageConf pageConf = TapReason.getConf().getUiConfByEventType( getEventType() );			
			TapReasonStyledPageConfiguration conf = new TapReasonStyledPageConfiguration();
			
			// Default initializations
			// Top Icon						
			conf.setTopIconDrawable( pageConf, R.drawable.tap_reason_rate_stars, activity );
			
			// Default selection color
			conf.setSelectionBGColor( pageConf, R.color.tap_reason_default_selected_color, activity );
									
			// Main title
			conf.setMainTitle( pageConf, R.string.tap_reason_rate_dialog_main_title_text, activity, true );
			
			// Secondary title
			conf.setSecondaryTitle( pageConf, activity, R.string.tap_reason_rate_dialog_secondary_title_text );

			
			// Option 1 Btn				
			conf.setOption1BtnText( pageConf, activity, R.string.tap_reason_rate_dialog_rate_app );
			conf.setOption1BtnTextIsBold( pageConf.isOption1BtnTextIsBold() );
			conf.setOption1BtnTextShowIcon( pageConf.isOption1BtnTextShowIcon() );
			conf.setOption1BtnDrawable( pageConf, R.drawable.tap_reason_google_play, activity );			

			// Option 2 Btn
			conf.setOption2BtnText( pageConf, activity, R.string.tap_reason_rate_dialog_needs_improvement );
			conf.setOption2BtnTextIsBold( pageConf.isOption2BtnTextIsBold() );
			conf.setShowOption2Btn( pageConf.isShowOption2Btn() );
			
			// Option 3 Btn
			conf.setOption3BtnText( pageConf, activity, R.string.tap_reason_rate_dialog_remind_later_option );
			conf.setOption3BtnTextIsBold( pageConf.isOption3BtnTextIsBold() );
			conf.setShowOption3Btn( pageConf.isShowOption3Btn() );
			
			
			switch ( getStyleTypeId() )
			{
				case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_1:
				{
					// Top Icon
					conf.setShowTopIcon( pageConf.isShowTopIcon() );
					
					// Header BG
					conf.setHeaderBGColor( pageConf, R.color.tap_reason_green, activity );
																	
					// Main title text color
					conf.setMainTitleTextColor( pageConf, R.color.tap_reason_white, activity );						

					// Secondary title text color
					conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_white, activity );
					
					// Option 1 Btn				
					conf.setOption1BtnBGColor( pageConf, R.color.tap_reason_white, activity );
					conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_black, activity );

					// Option 2 Btn
					conf.setOption2BtnBGColor( pageConf, R.color.tap_reason_white, activity );
					conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_black, activity );
					
					// Option 3 Btn
					conf.setOption3BtnBGColor( pageConf, R.color.tap_reason_white, activity );
					conf.setOption3BtnTextColor( pageConf, R.color.tap_reason_black, activity );						
					break;
				}
				case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_2:
				{
					// Top Icon						
					conf.setTopIconDrawable( pageConf, R.drawable.tap_reason_9, activity );
					
					// Top Icon
					conf.setShowTopIcon( true );
					
					// Header BG
					conf.setHeaderBGColor( pageConf, android.R.color.transparent, activity );
					
					// Body BG color
					conf.setBodyBGColor( pageConf, R.color.tap_reason_white, activity );
																	
					// Main title text color
					conf.setMainTitleTextColor( pageConf, R.color.tap_reason_pomegranate_red, activity );						

					// Secondary title text color
					conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_concrete_grey, activity );
					
					// Option 1 Btn				
					conf.setOption1BtnBGColor( pageConf, R.color.tap_reason_nephritis_green, activity );
					conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_3_btn_style2_btn_text_color, activity );

					// Option 2 Btn
					conf.setOption2BtnBGColor( pageConf, R.color.tap_reason_orange_orange, activity );
					conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_3_btn_style2_btn_text_color, activity );
					
					// Option 3 Btn
					conf.setOption3BtnBGColor( pageConf, R.color.tap_reason_midnight_blue_grey, activity );
					conf.setOption3BtnTextColor( pageConf, R.color.tap_reason_3_btn_style2_btn_text_color, activity );										
					break;
				}
				case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_1:
				{
					// Top Icon
					conf.setShowTopIcon( pageConf.isShowTopIcon() );
					
					// Main title text color
					conf.setMainTitleTextColor( pageConf, R.color.tap_reason_white, activity );						

					// Secondary title text color
					conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_black, activity );				

					// Header BG
					conf.setHeaderBGColor( pageConf, R.color.tap_reason_green, activity );
					
					// Body BG color
					conf.setBodyBGColor( pageConf, R.color.tap_reason_white, activity );						
					
					// Option 1 Btn				
					conf.setOption1BtnBGColor( pageConf, R.color.tap_reason_green, activity );
					conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_white, activity );

					// Option 2 Btn
					conf.setOption2BtnBGColor( pageConf, android.R.color.transparent, activity );
					conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_black, activity );	
					conf.setOption2BtnDrawable( pageConf, R.drawable.tap_reason_29, activity );
					conf.setOption2BtnText( pageConf, activity, R.string.tap_reason_rate_dialog_remind_later_option );
			
					conf.setOptionsSeparatorText( pageConf, activity, R.string.tap_reason_options_separator_text );					
					break;
				}
				case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_2:
				{
					// Top Icon 
					conf.setTopIconDrawable( pageConf, R.drawable.tap_reason_pink_heart, activity );
					
					conf.setMainTitle( pageConf, R.string.tap_reason_rate_dialog_main_title_text, activity, false );
					
					// Main title text color
					conf.setMainTitleTextColor( pageConf, R.color.tap_reason_black, activity );								

					// Secondary title text color
					conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_black, activity );				

					// Header BG
					conf.setHeaderBGColor( pageConf, android.R.color.transparent, activity );
					
					// Body BG color
					conf.setBodyBGColor( pageConf, R.color.tap_reason_white, activity );						
					
					// Option 1 Btn				
					conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_midnight_blue_grey, activity );

					// Option 2 Btn
					conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_midnight_blue_grey, activity );	
					conf.setOption2BtnDrawable( pageConf, R.drawable.tap_reason_29, activity );
					conf.setOption2BtnText( pageConf, activity, R.string.tap_reason_rate_dialog_remind_later_option );					
					break;
				}				
			}
			
			configurePage( activity, conf );
		}
		catch ( Exception exception )
		{
			Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "Error parsing rating page configuration", exception );
		}
	}	
	
	@Override
	public void onClick(TapReasonActivity activity, View v)
	{
		switch ( getStyleTypeId() )
		{
			case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_1:
			case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_2:				
			{
				activity.finish();
				
				int id = v.getId();
				if ( id == R.id.tapReasonOption1BtnLayout )
				{
					onRateAppClicked( activity );
				}
				else if ( id == R.id.tapReasonOption2BtnLayout )
				{		
					onRemindMeLaterClick( activity );
				}						
				break;
			}
			case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_1:
			case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_2:				
			{
				activity.finish();
				
				int id = v.getId();
				if ( id == R.id.tapReasonOption1Btn )
				{
					onRateAppClicked( activity );
				}
				else if ( id == R.id.tapReasonOption2Btn )
				{			
					onLeaveFeedbackClick( activity );
				}
				else if ( id == R.id.tapReasonOption3Btn )
				{		
					onRemindMeLaterClick( activity );
				}				
				break;
			}
		}
	}

	private void onRateAppClicked(TapReasonActivity activity)
	{
		Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(TapReason.getConf().getGooglePlayIntentUri( activity )));
		List<ResolveInfo> activities = activity.getPackageManager().queryIntentActivities(intent, 0);
		if ( activities != null && !activities.isEmpty() )
		{	
			notifyEventResult( TapReasonEventTypes.REQUEST_FOR_RATE_EVENT, TapReasonEventResult.POSITIVE );
			TapReason.reportEventResult( TapReasonEventTypes.REQUEST_FOR_RATE_EVENT, TapReasonEventResult.POSITIVE );					
			activity.startActivity( intent );
			return;
		}
		else
		{
			// There is no activity that answers google market intent
			TapReason.reportEventResult( TapReasonEventTypes.REQUEST_FOR_RATE_EVENT, TapReasonEventResult.ERROR, TapReasonEventSubResult.ERROR_OPENING_APPSTORE );
		}
	}
	
	@Override
	public boolean parseBundle()
	{
		return true;
	}	
}
