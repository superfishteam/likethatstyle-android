package com.likethatapps.style;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.likethatapps.style.model.HistoryModel;
import com.likethatapps.style.service.UserHistoryService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by eladyarkoni on 7/7/15.
 */
public class HistoryActivity extends StyleBaseActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private HistoryAdapter mAdapter;
    private List<HistoryModel> mHistory;
    private View mEmptyMessage;
    private boolean historyItemClicked = false;
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreateBaseActivity(this, savedInstanceState, R.layout.history_activity);
        mEmptyMessage = (View)findViewById(R.id.empty_message);
        render();
    }

    @Override
    protected void onResume() {
        super.onResume();
        historyItemClicked = false;
        updateList();
    }

    public void updateList() {
        mHistory = UserHistoryService.getInstance().getHistory();
        mAdapter.notifyDataSetChanged();
        if (mHistory.size() == 0) {
            mEmptyMessage.setVisibility(View.VISIBLE);
        } else {
            mEmptyMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        historyItemClicked = true;
        HistoryModel historyModel = mHistory.get(i);
        Bundle bundle = new Bundle();
        bundle.putString(Consts.EXTRA_DATA_RESULTS_JSON, UserHistoryService.getInstance().getHistorySearchFile(historyModel.searchId));
        bundle.putBoolean(Consts.EXTRA_DATA_FIRST_TIME_BOOLEAN, false);
        navigateToActivity(StyleResultsActivity.class, bundle, true, -1);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long l) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        HistoryModel model = mHistory.get(i);
                        UserHistoryService.getInstance().remove(model.searchId);
                        updateList();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        builder.setMessage(getResources().getString(R.string.history_screen_delete)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
        return true;
    }

    private void render() {
        mHistory = UserHistoryService.getInstance().getHistory();
        ListView listView = (ListView)findViewById(R.id.list_view);
        mAdapter = new HistoryAdapter();
        mAdapter.init(mHistory, this);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
        listView.setAdapter(mAdapter);
        if (mHistory.size() == 0) {
            mEmptyMessage.setVisibility(View.VISIBLE);
        } else {
            mEmptyMessage.setVisibility(View.GONE);
        }
    }

    private class HistoryAdapter extends BaseAdapter {

        private List<HistoryModel> favorites;
        private Context context;

        public void init(List<HistoryModel> favorites, Context context) {
            this.favorites = favorites;
            this.context = context;
        }

        @Override
        public int getCount() {
            return this.favorites.size();
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolder holder;
            if (convertView == null) {
                LayoutInflater layoutInflator = LayoutInflater.from(context);
                convertView = layoutInflator.inflate(R.layout.history_list_item, null);
                holder = new ViewHolder();
                holder.imageView = (ImageView)convertView.findViewById(R.id.image_view);
                holder.titleTextView = (TextView)convertView.findViewById(R.id.image_title);
                holder.sourceTextView = (TextView)convertView.findViewById(R.id.image_source);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            HistoryModel historyModel = this.favorites.get(i);
            StyleApplication.imageLoader.DisplayImage(historyModel.imageUrl, holder.imageView);
            holder.titleTextView.setText(getDateStr(historyModel.date));
            holder.sourceTextView.setText(readHour(historyModel.date));
            return convertView;
        }

        public String readHour(Date date) {
            GregorianCalendar g = new GregorianCalendar();
            g.setTimeZone(TimeZone.getDefault());
            g.setTime(date);
            int hour = g.get(Calendar.HOUR_OF_DAY);
            int minutes = g.get(Calendar.MINUTE);
            String hourStr = hour > 9 ? hour+"" : "0" + hour;
            String minStr = minutes > 9 ? minutes+"" : "0" + minutes;
            return hourStr + ":" + minStr;
        }

        public String getDateStr(Date date) {
            Time startTime = new Time();
            startTime.set(date.getTime());
            int startDay = Time.getJulianDay(date.getTime(), startTime.gmtoff);

            Time currentTime = new Time();
            currentTime.set(System.currentTimeMillis());
            int currentDay = Time.getJulianDay(System.currentTimeMillis(), currentTime.gmtoff);
            int days = Math.abs(currentDay - startDay);
            if (days == 0) {
                return "Today, " + sdf.format(date);
            } else {
                return sdf.format(date);
            }

        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public Object getItem(int i) {
            return this.favorites.get(i);
        }
    }

    class ViewHolder {
        ImageView imageView;
        TextView titleTextView;
        TextView sourceTextView;
    }

    @Override
    protected String getBarTitle() {
        return getString(R.string.activity_history_title);
    }
}
