package com.likethatapps.style;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.likethatapps.services.api.SFApi;
import com.likethatapps.services.api.types.SearchSource;
import com.likethatapps.services.camera.CameraPreview;
import com.likethatapps.services.utils.ImageUtils;
import com.likethatapps.style.service.ReportService;
import com.nhaarman.supertooltips.ToolTipView;
import com.tapreason.sdk.TapReasonAnnotations;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

@TapReasonAnnotations.TapReasonDontInvokeEventsForListener
public class CameraActivity extends StyleBaseActivity {


    ImageButton changeFlashButton;
    private static final String TAG = "gardening-camera";
    OrientationEventListener myOrientationEventListener;
    int orientation = 0;
    int layoutOrientation = 0;
    private SensorManager mSensorManager;
    private Sensor mLightSensor;
    private float mLightQuantity = 100;
    private SensorEventListener sensorEventListener;
    private static final int MINIMUM_LIGHT_QUALITY = 10;
    private static boolean mCameraFlashOn = false;
    private View mCropArea;
    private ImageButton mCameraBtn;
    private CameraPreview mPreview;
    private RelativeLayout mCameraContainer;
    private boolean mLockCamera = false;
    private int currentCameraId = -1;
    private boolean isFrontCamera = false;
    private View mCameraRollBtn;
    private View mCameraRailView;
    private View mHelpBtn;
    private ToolTipView mHelpTip;

    @Override
    protected void onResume() {
        super.onResume();
        ReportService.getInstance().report(3016, "camera_screen_opened");
        if (currentCameraId == -1) {
            currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        }
        loadCamera(currentCameraId);
        if (StyleApplication.getCounter("camera_enter") == 0) {
            mCameraRailView.setVisibility(View.VISIBLE);
            ReportService.getInstance().report(3056, "FTUE_camera_rail_opened");
        } else {
            mCameraRailView.setVisibility(View.GONE);
        }
        StyleApplication.increaseCounter("camera_enter");
    }

    @Override
    protected boolean hasCameraSearchBtn() {
        return false;
    }

    private void loadCamera(int cameraId) {
        mPreview = new CameraPreview(this, cameraId, CameraPreview.LayoutMode.FitToParent);
        mPreview.setMinWidth(800);
        mPreview.setOnPreviewReady(new CameraPreview.PreviewReadyCallback() {
            @Override
            public void onPreviewReady() {
                setCameraAction();
            }
        });
        ViewGroup.LayoutParams previewLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mCameraContainer.addView(mPreview, 0, previewLayoutParams);
    }

    private void releaseCamera() {
        mPreview.stop();
        mCameraContainer.removeView(mPreview); // This is necessary.
        mPreview = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
    }

    private void setCameraAction() {
        final Camera camera = mPreview.getCamera();
        if (camera == null) {return;}
        mCameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shot();
            }
        });
    }

    public void closeFtueScreen(View view) {
        mCameraRailView.setVisibility(View.GONE);
        if (view.getId() == R.id.ftue_roll_btn) {
            getCameraRollPhoto();
        } else {
            helpBtnClicked(null);
        }
    }

    public void getCameraRollPhoto() {
        selectPhotoFromGallery();
    }

    @Override
    protected boolean showBar() {
        return false;
    }

    public void helpBtnClicked(View view) {
        if (mHelpTip == null || mHelpTip.getParent() == null) {
            ReportService.getInstance().report(3057, "FTUE_camera_education_opened");
            mHelpTip = showTooltip(R.layout.camera_help_tip, mHelpBtn, false);
            mHelpTip.findViewById(R.id.tip_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ReportService.getInstance().report(3059, "FTUE_camera_education_searched");
                    Bundle bundle = new Bundle();
                    bundle.putString(Consts.EXTRA_DATA_SEARCH_SOURCE, SearchSource.FTUE.toString());
                    bundle.putInt(Consts.EXTRA_DATA_IMAGE_RESOURCE_ID, R.drawable.tipcameraimage);
                    navigateToActivity(CropImageActivity.class, bundle, true, -1);
                }
            });
            mHelpTip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mHelpTip.remove();
                    mHelpTip = null;
                    ReportService.getInstance().report(3058, "FTUE_camera_education_skipped");
                }
            });
        } else {
            mHelpTip.remove();
            mHelpTip = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreateBaseActivity(this, savedInstanceState, R.layout.activity_camera);
        mCameraRailView = findViewById(R.id.camera_rail);
        mHelpBtn = findViewById(R.id.help_btn);
        mCameraContainer = (RelativeLayout)findViewById(R.id.camera_surface_container);
        changeFlashButton = (ImageButton) findViewById(R.id.changeFlashButton);
        changeFlashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchFlash();
            }
        });
        findViewById(R.id.changeCameraButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchCamera();
            }
        });
        mCropArea = findViewById(R.id.crop_area);
        mCameraBtn = (ImageButton)findViewById(R.id.camera_btn);
        myOrientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int i) {
                if (i > 315 || i <= 45) {
                    orientation = 0;
                } else if (i > 45 && i <= 135) {
                    orientation = 90;
                }
                else if (i <= 225 && i > 135) {
                    orientation = 180;
                }
                else if (i > 225 && i <= 315) {
                    orientation = 270;
                }
            }
        };
        myOrientationEventListener.enable();
        try {
            mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            mLightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        } catch (Exception ex){}
        sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                mLightQuantity = sensorEvent.values[0];
                if ((mLightQuantity < MINIMUM_LIGHT_QUALITY) && (!mCameraFlashOn)) {

                } else {

                }
            }
            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };
        try {
            mSensorManager.registerListener(sensorEventListener, mLightSensor, SensorManager.SENSOR_DELAY_UI);
        } catch (Exception ex) {}
        new FirstGalleryImageAsyncTask(this, (ImageView)findViewById(R.id.cameraroll_btn)).execute();
        mCameraRollBtn = findViewById(R.id.cameraroll_btn);
        mCameraRollBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ReportService.getInstance().report(1013, "cameraroll_screen_opened", null, null, null, null);
                getCameraRollPhoto();
            }
        });
    }

    public void switchCamera() {
        int cameras = Camera.getNumberOfCameras();
        if (cameras > 1) {
            if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
                currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                isFrontCamera = true;
            } else {
                currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                isFrontCamera = false;
            }
            releaseCamera();
            loadCamera(currentCameraId);
        }
    }

    public void shot() {
        if (mLockCamera){return;}
        mLockCamera = true;
        final Camera camera = mPreview.getCamera();
        try {
            camera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean b, Camera camera) {
                    camera.takePicture(null, null, null, mPictureCallback);
                }
            });
        } catch (RuntimeException ex) {
            Log.e(TAG, "autofocus is failed, taking a picture...");
            camera.takePicture(null, null, null, mPictureCallback);
        }
    }

    Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            ReportService.getInstance().report(3018, "photo_captured");
            int backCameraOrientation = (90 + orientation + layoutOrientation) % 360;
            int frontCameraOrientation = (360 - backCameraOrientation) % 360;
            int calculatedOrientation = isFrontCamera ? frontCameraOrientation : backCameraOrientation;
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            Log.d(TAG, "captured image size: " + bitmap.getWidth() + "," + bitmap.getHeight());
            if (calculatedOrientation != 0) {
                Matrix mtx = new Matrix();
                mtx.postRotate(calculatedOrientation);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, false);
            }
            try {
                data = ImageUtils.getImageByteArray(bitmap,80);
            } catch (IOException ex) {

            } finally {
                StyleApplication.increaseCounter(StyleApplication.COUNTER_SEARCH_BY_CAMERA);
                String uri = saveToCameraRoll(data);
                Bundle extras = new Bundle();
                extras.putString(Consts.EXTRA_DATA_IMAGE_URI, uri);
                extras.putBoolean(Consts.EXTRA_DATA_FIRST_TIME_BOOLEAN, false);
                extras.putInt(Consts.EXTRA_DATA_IMAGE_ORIENTATION, 0);
                extras.putString(Consts.EXTRA_DATA_SEARCH_SOURCE, SearchSource.CAMERA.toString());
                navigateToActivity(CropImageActivity.class, extras, true, -1);
            }
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        layoutOrientation = newConfig.orientation;
    }

    public String saveToCameraRoll(final byte[] bytes) {
        Location location = SFApi.getInstance().getLocationService().getCurrentLocation();
        String picId = "stylephoto_" + (new Date()).getTime();
        String imagePath = Environment.getExternalStorageDirectory() + "/Style";
        File dir = new File(imagePath);
        if (!dir.exists()) {
            if (!dir.mkdir()) {
                return null;
            }
        }
        String filePath = dir + File.separator + picId + ".jpg";
        Uri uri = saveMediaEntry(filePath, bytes, "style photo", "style photo", System.currentTimeMillis(), orientation, location);
        return uri.toString();
    }

    private Uri saveMediaEntry(String imagePath, byte[] bytes, String title,String description,long dateTaken,int orientation,Location loc) {
        ContentValues v = new ContentValues();
        v.put(MediaStore.Images.Media.TITLE, title);
        v.put(MediaStore.Images.Media.DISPLAY_NAME, title);
        v.put(MediaStore.Images.Media.DESCRIPTION, description);
        v.put(MediaStore.Images.Media.DATE_ADDED, dateTaken);
        v.put(MediaStore.Images.Media.DATE_TAKEN, dateTaken);
        v.put(MediaStore.Images.Media.DATE_MODIFIED, dateTaken) ;
        v.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        v.put(MediaStore.Images.Media.ORIENTATION, orientation);

        File f = new File(imagePath) ;
        try {
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes);
            fo.close();
        } catch (IOException ex){}
        File parent = f.getParentFile() ;
        String path = parent.toString().toLowerCase() ;
        String name = parent.getName().toLowerCase() ;
        v.put(MediaStore.Images.ImageColumns.BUCKET_ID, path.hashCode());
        v.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, name);
        v.put(MediaStore.Images.Media.SIZE,f.length()) ;
        f = null ;
        if( loc != null ) {
            v.put(MediaStore.Images.Media.LATITUDE, loc.getLatitude());
            v.put(MediaStore.Images.Media.LONGITUDE, loc.getLongitude());
        }
        v.put("_data",imagePath) ;
        ContentResolver c = getContentResolver() ;
        return c.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, v);
    }

    private void applyFlashSettings() {
        Camera camera = mPreview.getCamera();
        if (camera == null) {
            return;
        }
        Camera.Parameters cp = camera.getParameters();
        if (mCameraFlashOn) {
            cp.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            changeFlashButton.setImageResource(R.drawable.btnflash);
        } else {
            cp.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            changeFlashButton.setImageResource(R.drawable.btnflashoff);
        }
        try {
            camera.setParameters(cp);
        } catch (Exception ex) {
            Log.e(TAG, "flash is not supported");

        }
    }

    private void switchFlash() {
        mCameraFlashOn = !mCameraFlashOn;
        applyFlashSettings();
    }
}
