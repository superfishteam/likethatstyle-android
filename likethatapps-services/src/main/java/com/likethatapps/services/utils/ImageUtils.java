package com.likethatapps.services.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.Rect;
import android.net.Uri;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.DisplayMetrics;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by eladyarkoni on 5/12/14.
 */
public class ImageUtils {

    private static final int LANDSCAPE_THUMBNAIL_WIDTH = 600;
    private static final int PORTAIT_THUMBNAIL_WIDHT = 600;
    private static final int QUALITY = 100;

    public static byte[] decodeAndRotateImageData(byte[] data, int calculatedOrientation, boolean scaleBitmap) throws IOException {
        Bitmap bmp = getBitmap(data);
        if (scaleBitmap) {
            bmp = scaleBitmap(bmp);
        }
        if (calculatedOrientation != 0) {
            Matrix mtx = new Matrix();
            mtx.postRotate(calculatedOrientation);
            bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mtx, false);
        }
        return getImageByteArray(bmp, QUALITY);
    }

    public static Bitmap blur(Context ctx, Bitmap image) {
        int width = Math.round(image.getWidth());
        int height = Math.round(image.getHeight());

        Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript rs = RenderScript.create(ctx);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(20.5f);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);

        return outputBitmap;
    }

    public static byte[] decodeAndRotateImageData(byte[] data, int calculatedOrientation, int scaleToWidth, int scaleToHeight, int x, int y, int width, int height) throws IOException {
        Bitmap bmp = getBitmap(data);
        Bitmap scaledBitmap = scaleBitmap(bmp, scaleToWidth, scaleToHeight);
        if (calculatedOrientation != 0) {
            Matrix mtx = new Matrix();
            mtx.postRotate(calculatedOrientation);
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, x, y, width, height, mtx, false);
        }
        return getImageByteArray(scaledBitmap, QUALITY);
    }

    public static Bitmap compressBitmap(Bitmap bitmap, int quality) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream);
        return getBitmap(stream.toByteArray());
    }

    public static byte[] decodeImageData(byte[] data) throws IOException{
        Bitmap bmp = getBitmap(data);
        if (bmp == null) {
            return null;
        }
        Bitmap scaledBitmap = scaleBitmap(bmp);
        return getImageByteArray(scaledBitmap, QUALITY);
    }

    public static Bitmap scaleBitmap(Bitmap bitmap) {
        float h = bitmap.getHeight();
        float w = bitmap.getWidth();
        float ratio = 1;
        if (h > w) {
            if (w <= PORTAIT_THUMBNAIL_WIDHT) {
                return bitmap;
            }
            ratio = (PORTAIT_THUMBNAIL_WIDHT / w);
            return Bitmap.createScaledBitmap(bitmap, PORTAIT_THUMBNAIL_WIDHT, Math.round(ratio * h), true);
        } else {
            if (w <= LANDSCAPE_THUMBNAIL_WIDTH) {
                return bitmap;
            }
            ratio = (LANDSCAPE_THUMBNAIL_WIDTH / h);
            return Bitmap.createScaledBitmap(bitmap,Math.round(ratio * w),LANDSCAPE_THUMBNAIL_WIDTH, true);
        }
    }

    public static Bitmap scaleMaxBitmap(Bitmap bitmap, int maxDimension) {
        float h = bitmap.getHeight();
        float w = bitmap.getWidth();
        float ratio = 1;
        if (h > w) {
            if (h <= maxDimension) {
                return bitmap;
            }
            ratio = (maxDimension / h);
            return Bitmap.createScaledBitmap(bitmap, Math.round(w * ratio), maxDimension, true);
        } else {
            if (w <= maxDimension) {
                return bitmap;
            }
            ratio = (maxDimension / w);
            return Bitmap.createScaledBitmap(bitmap,maxDimension,Math.round(h * ratio), true);
        }
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int width, int height) {
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    public static Bitmap getBitmap(Context context, Integer resourceId) {
        BitmapFactory.Options o=new BitmapFactory.Options();
        o.inDither=false;
        o.inPurgeable=true;
        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(),
                resourceId, o);
        return Bitmap.createScaledBitmap(bmp, PORTAIT_THUMBNAIL_WIDHT, PORTAIT_THUMBNAIL_WIDHT, true);
    }

    public static Bitmap getResourceBitmap(Context context, Integer resourceId) {
        BitmapFactory.Options o=new BitmapFactory.Options();
        o.inDither=false;
        o.inPurgeable=true;
        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(),
                resourceId, o);
        return bmp;
    }

    public static Bitmap getBitmap(byte[] imageArray) {
        Bitmap bmp = BitmapFactory.decodeByteArray(imageArray, 0, imageArray.length);
        return bmp;
    }

    public static byte[] getImageByteArray(Context ctx, Uri uri) throws IOException {
        InputStream iStream =   ctx.getContentResolver().openInputStream(uri);
        return getBytes(iStream);
    }

    public static byte[] getImageByteArray(Bitmap data, int quality) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        data.compress(Bitmap.CompressFormat.JPEG, quality, stream);
        return stream.toByteArray();
    }

    private static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static Bitmap getRoundedShape(Activity activity, int widthDp, int heightDp, Bitmap scaleBitmapImage) {
        int targetWidth = Math.round(convertDpToPixel(widthDp, activity));
        int targetHeight = Math.round(convertDpToPixel(heightDp,activity));
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                targetHeight,Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);

        canvas.clipPath(path);
        Bitmap sourceBitmap = scaleBitmapImage;
        canvas.drawBitmap(sourceBitmap,
                new Rect(0, 0, sourceBitmap.getWidth(),
                        sourceBitmap.getHeight()),
                new Rect(0, 0, targetWidth,
                        targetHeight), null);
        return targetBitmap;
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }
}
