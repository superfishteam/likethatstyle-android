package com.likethatapps.services.api;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.likethatapps.services.DecorABTestServiceImpl;
import com.likethatapps.services.api.http.Ajax;
import com.likethatapps.services.api.types.SearchServer;

/**
 * Created by eladyarkoni on 5/14/14.
 */
public class SFApi {

    private static SFApi _instance = new SFApi();

    /* customs */
    private String customAppName = null;

    /* services */
    private ISearchService searchService = null;
    private ILocationService locationService = null;
    private IReportService reportService = null;
    private IGalleryService galleryService = null;
    private IABTestService abTestService = null;

    protected SFApi() {}

    public static SFApi getInstance() {
        return _instance;
    }

    public void init(Context context, SearchServer searchServer, String appName) {
        this.customAppName = appName;
        Ajax.init(context);
        switch (searchServer) {
            case MARLIN:
                searchService = new MarlinSearchServiceImpl(context, getOSVersion(context), getAppName(context), getAppVersion(context), getUserId(context), getDeviceModel(context));
                reportService = new MarlinReportServiceImpl(context, getOSVersion(context), getUserId(context), getAppName(context), getAppVersion(context), getDeviceModel(context));
            break;
            case GINN:
                abTestService = new DecorABTestServiceImpl(context, getUserId(context));
                searchService = new GinnSearchServiceImpl(context, getOSVersion(context), getAppName(context), getAppVersion(context), getUserId(context), getDeviceModel(context));
                reportService = new DecorReportServiceImpl(context, getOSVersion(context), getUserId(context), getAppName(context), getAppVersion(context), getDeviceModel(context));
            break;
            case GARDENING:
                searchService = new GardeningSearchServiceImpl(context, getOSVersion(context), getAppName(context), getAppVersion(context), getUserId(context), getDeviceModel(context));
                reportService = new GardeningReportServiceImpl(context, getOSVersion(context), getUserId(context), getAppName(context), getAppVersion(context), getDeviceModel(context));
            break;
            case TATTOO:
                searchService = new TattooSearchServiceImpl(context, getOSVersion(context), getAppName(context), getAppVersion(context), getUserId(context), getDeviceModel(context));
                reportService = new TattooReportServiceImpl(context, getOSVersion(context), getUserId(context), getAppName(context), getAppVersion(context), getDeviceModel(context));
                break;
            case STYLE:
                searchService = new StyleSearchServiceImpl(context, getOSVersion(context), getAppName(context), getAppVersion(context), getUserId(context), getDeviceModel(context));
                reportService = new StyleReportServiceImpl(context, getOSVersion(context), getUserId(context), getAppName(context), getAppVersion(context), getDeviceModel(context));
                break;
        }
        locationService = new LocationServiceImpl(context);
        galleryService = new GalleryServiceImpl(context);
        if (abTestService != null) {
            abTestService.updateUserData();
        }
    }

    public IGalleryService getGalleryService() {return galleryService;}

    public ISearchService getSearchService() {
        return searchService;
    }

    public ILocationService getLocationService() {
        return locationService;
    }

    public IReportService getReportService() {
        return reportService;
    }

    public IABTestService getAbTestService() {return abTestService;}

    public String getUserId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getAppName(Context context) {
        if (this.customAppName != null) {
            return this.customAppName;
        }
        try {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo appInfo = pm.getApplicationInfo(context.getPackageName(), 0);
            String appName =  pm.getApplicationLabel(appInfo).toString();
            return appName.replaceAll(" ", "");
        } catch (PackageManager.NameNotFoundException ex) {
            return "";
        }
    }

    public String getAppVersion(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException ex) {
            return "";
        }
    }

    public String getNetworkType(Context context) {
        TelephonyManager teleMan = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo.isConnected()) {
            return "WIFI";
        }
        int networkType = teleMan.getNetworkType();
        switch (networkType)
        {
            case TelephonyManager.NETWORK_TYPE_1xRTT: return "1xRTT";
            case TelephonyManager.NETWORK_TYPE_CDMA: return "CDMA";
            case TelephonyManager.NETWORK_TYPE_EDGE: return "EDGE";
            case TelephonyManager.NETWORK_TYPE_EHRPD: return "eHRPD";
            case TelephonyManager.NETWORK_TYPE_EVDO_0: return "EVDO rev. 0";
            case TelephonyManager.NETWORK_TYPE_EVDO_A: return "EVDO rev. A";
            case TelephonyManager.NETWORK_TYPE_EVDO_B: return "EVDO rev. B";
            case TelephonyManager.NETWORK_TYPE_GPRS: return "GPRS";
            case TelephonyManager.NETWORK_TYPE_HSDPA: return "HSDPA";
            case TelephonyManager.NETWORK_TYPE_HSPA: return "HSPA";
            case TelephonyManager.NETWORK_TYPE_HSPAP: return "HSPA+";
            case TelephonyManager.NETWORK_TYPE_HSUPA: return "HSUPA";
            case TelephonyManager.NETWORK_TYPE_IDEN: return "iDen";
            case TelephonyManager.NETWORK_TYPE_LTE: return "LTE";
            case TelephonyManager.NETWORK_TYPE_UMTS: return "UMTS";
            case TelephonyManager.NETWORK_TYPE_UNKNOWN: return "Unknown";
        }
        return "Unknown";
    }

    public boolean isRelease(Context context) {
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            return ((pi.applicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) == 0);
        } catch (Exception e) {}
        return false;
    }

    public String getDeviceModel(Context context) {
        return Build.MODEL != null ? Build.MODEL.replaceAll(" ", "_") : "Unknown";
    }

    public String getOSVersion(Context context) {
        return Build.VERSION.RELEASE;
    }
}
