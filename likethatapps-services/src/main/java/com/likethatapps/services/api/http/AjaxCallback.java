package com.likethatapps.services.api.http;

/**
 * Created by eladyarkoni on 5/13/14.
 */
public abstract class AjaxCallback {

    public void success(String result) {}
    public void error(String error, int code) {}
    public void timeout(int retry) {}
    public void retry(int retry) {}
}
