package com.tapreason.view.pages;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.tapreason.R;
import com.tapreason.sdk.TapReason;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventResult;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventSubResult;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventTypes;
import com.tapreason.sdk.TapReasonConfiguration.TapReasonPageConf;
import com.tapreason.sdk.TapReasonNetworkImageView;
import com.tapreason.view.base.TapReasonActivity;
import com.tapreason.view.util.TapReasonUtils;

public abstract class TapReasonRulePageBase
{
	private Bundle params;
	private TapReasonEventTypes eventType;
	private byte styleTypeId;
	
	protected TapReasonRulePageBase( Bundle params, TapReasonEventTypes eventType, byte defaultStyleTypeId )
	{
		this.params = params;
		this.eventType = eventType;
		this.styleTypeId = defaultStyleTypeId;
		
		// Loading the style from the page conf
		TapReasonPageConf pageConf = TapReason.getConf().getUiConfByEventType( getEventType() );
		
		if ( pageConf != null && pageConf.getUITemplateId() != TapReasonPageConf.UI_TEMPLATE_NOT_SPECIFIED )
		{
			this.styleTypeId = pageConf.getUITemplateId();
		}		
	}
	
	public static TapReasonRulePageBase createPageByEventType( TapReasonEventTypes eventType, Bundle params )
	{
		TapReasonRulePageBase page = null;
		switch ( eventType )
		{
			case FEEDBACK_EVENT:
			{
				page = new TapReasonFeedbackPage( params,  eventType );
				break;
			}
			case REQUEST_FOR_RATE_EVENT:
			{
				page = new TapReasonRatingPage( params, eventType );
				break;
			}
			case SHARE_APP_TO_LINE_EVENT:				
			case SHARE_APP_TO_KIK_EVENT:			
			case SHARE_APP_TO_TWITTER_EVENT:
			case SHARE_APP_TO_FACEBOOK_EVENT:
			case SHARE_APP_TO_WHATSAPP_EVENT:
			case SHARE_APP_TO_WECHAT_EVENT:
			case SHARE_APP_TO_KAKAO_EVENT:
			case SHARE_APP_TO_VK_EVENT:
			case SHARE_APP_TO_VIBER_EVENT:
			case SHARE_APP_TO_SMS_EVENT:
			case SHARE_APP_TO_GOOGLE_HANGOUTS:					
			{
				page = new TapReasonShareAppPage( params, eventType );
				break;
			}			
			default:
				break;
		}
		
		return page;
	}
	
	protected void notifyEventResult( TapReasonEventTypes eventType, TapReasonEventResult eventResult )
	{
		try
		{
			if ( TapReason.getConf().getOnEventResultListener() != null )
			{
				TapReason.getConf().getOnEventResultListener().onTapReasonEventResult( eventType, eventResult );
			}
		}
		catch ( Throwable tr )
		{
			Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "Error notifying about event result", tr );
		}		
	}
	
	protected Bundle getParams()
	{
		return params;
	}
	
	protected TapReasonEventTypes getEventType()
	{
		return eventType;
	}
	
	public void onCreate( TapReasonActivity activity, OnClickListener onClickListener )
	{
		TapReason.reportEventResult( getEventType(), TapReasonEventResult.DISPLAYED);
		registerListenersForPageStyle( activity, onClickListener );
	}
	
	public abstract void onClick( TapReasonActivity activity, View view );
	
	public void onResume(TapReasonActivity activity)
	{
		// Configuring the screen
		configurePage( activity );
	}

	@SuppressLint("ResourceAsColor")
	protected abstract void configurePage( TapReasonActivity activity );
	
	public void onActivityResult(TapReasonActivity activity, int requestCode,
			int resultCode, Intent data)
	{
		
	}
	
	public void onKeyDown(TapReasonActivity activity, int keyCode, KeyEvent event)
	{
		if ( keyCode == KeyEvent.KEYCODE_BACK )
		{
			TapReason.reportEventResult( eventType, TapReasonEventResult.DISMISSED_BY_BACK_BUTTON );
			notifyEventResult( eventType, TapReasonEventResult.DISMISSED_BY_BACK_BUTTON );			
		}
	}	
	
	/**
	 * Parses the params bundle and checks if all the params are valid
	 * @return if the bundle is valid
	 */
	public abstract boolean parseBundle();
	
	public byte getStyleTypeId()
	{
		return styleTypeId;
	}
	
	protected void setStyleTypeId(byte styleTypeId)
	{
		this.styleTypeId = styleTypeId;
	}
	
	protected void onLeaveFeedbackClick( TapReasonActivity activity )
	{
		notifyEventResult( getEventType(), TapReasonEventResult.FEEDBACK );

		TapReason.reportEventResult( getEventType(), TapReasonEventResult.FEEDBACK);
		
		try
		{
			boolean userOverrideDefaultAction = false;
			
			if ( TapReason.getConf().getOnEventResultListener() != null  )
			{
				userOverrideDefaultAction = TapReason.getConf().getOnEventResultListener().onTapReasonUserFeedback( getEventType() );
			}
			
			if ( !userOverrideDefaultAction )
			{
				String title = activity.getString( R.string.tap_reason_feedback_send_email_general_feedback, TapReason.getConf().getAppName() );
				
				String defaultEmailPackageName = TapReasonUtils.getDefaultEmailPackage( activity.getPackageManager() );
				
				if ( defaultEmailPackageName == null )
				{
					TapReason.reportEventResult( getEventType(), TapReasonEventResult.ERROR, TapReasonEventSubResult.ERROR_APP_PACKAGE_DOESNT_EXIST );
					
					// Closing the dialog
					activity.finish();
				}
				else
				{
					Intent emailIntent = TapReasonUtils.generateFeedbackEmailIntent( activity.getPackageManager(), defaultEmailPackageName, title );
					
					if ( emailIntent == null )
					{
						TapReason.reportEventResult( getEventType(), TapReasonEventResult.ERROR, TapReasonEventSubResult.ERROR_APP_PACKAGE_DOESNT_EXIST );
						
						// Closing the dialog
						activity.finish();
					}
					else
					{						
						activity.startActivity(emailIntent);
					}
				}

			}
		}
		catch( Throwable tr )
		{
			Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "Error while trying to get feedback from user", tr );
		}
	}	
	
	protected void onRemindMeLaterClick(TapReasonActivity activity)
	{
		// Marking the result
		TapReason.reportEventResult( getEventType(), TapReasonEventResult.POSTPONED );
		
		notifyEventResult( getEventType(), TapReasonEventResult.POSTPONED );	
	}	
	
	protected void registerListenersForPageStyle( TapReasonActivity activity, OnClickListener onClickListener )
	{
		switch ( getStyleTypeId() )
		{
			case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_2:
			{
				activity.setContentView( R.layout.tap_reason_three_btn_style2_dialog );
				
				activity.findViewById( R.id.tapReasonOption1Btn ).setOnClickListener( onClickListener );
				activity.findViewById( R.id.tapReasonOption2Btn ).setOnClickListener( onClickListener );
				activity.findViewById( R.id.tapReasonOption3Btn ).setOnClickListener( onClickListener );	
				break;
			}
			// This is the default case
			case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_1:
			{		
				activity.setContentView( R.layout.tap_reason_three_btn_style1_dialog );				
		
				activity.findViewById( R.id.tapReasonOption1Btn ).setOnClickListener( onClickListener );
				activity.findViewById( R.id.tapReasonOption2Btn ).setOnClickListener( onClickListener );
				activity.findViewById( R.id.tapReasonOption3Btn ).setOnClickListener( onClickListener );	
				break;
			}
			case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_1:
			{
				activity.setContentView( R.layout.tap_reason_two_btn_style1_dialog );				
				
				// Button listeners registering
				activity.findViewById( R.id.tapReasonOption1BtnLayout ).setOnClickListener( onClickListener );
				activity.findViewById( R.id.tapReasonOption2BtnLayout ).setOnClickListener( onClickListener );					
				break;
			}	
			case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_2:
			{
				activity.setContentView( R.layout.tap_reason_two_btn_style2_dialog );

				// Button listeners registering
				activity.findViewById( R.id.tapReasonOption1BtnLayout ).setOnClickListener( onClickListener );
				activity.findViewById( R.id.tapReasonOption2BtnLayout ).setOnClickListener( onClickListener );					
				break;
			}				
		}
		
		// Adding the listener for the power by watermark click
		activity.findViewById( R.id.tapReasonPoweredByWatermarkImg ).setOnClickListener( onClickListener );
	}
	
	@SuppressLint({ "ResourceAsColor", "DefaultLocale", "CutPasteId" })
	protected void configurePage( TapReasonActivity activity, TapReasonStyledPageConfiguration pageConf )
	{
		try
		{	
			// Handling the powered by wat ermark
			View poweredByView = activity.findViewById( R.id.tap_reason_powered_by_layout );
			if ( pageConf.shouldShowPoweredByWatermark() )
			{
				poweredByView.setVisibility( View.VISIBLE );
			}
			else
			{
				poweredByView.setVisibility( View.GONE );
			}
			
			// Handling the ui configuration according to the template type
			switch ( getStyleTypeId() )
			{
				case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_1:
				{					
					// Top icon
					TapReasonNetworkImageView img = (TapReasonNetworkImageView)activity.findViewById( R.id.tapReasonTopImg );
					if ( pageConf.isShowTopIcon() )
					{
						img.setVisibility( View.VISIBLE );						
						img.loadUrl( pageConf.getTopIconUrl(), pageConf.getTopIconResourceId() );
					}
					else
					{
						img.setVisibility( View.GONE );
					}				

					// Main title
					TextView mainTitle = (TextView)activity.findViewById( R.id.tapReasonMainHeaderTxt);
					mainTitle.setText( pageConf.getMainTitle() );
					mainTitle.setTextColor( pageConf.getMainTitleTextColor() );
					
					// Secondary msg
					TextView secondaryTitle = (TextView)activity.findViewById( R.id.tapReasonSecondaryHeaderTxt);
					secondaryTitle.setText( pageConf.getSecondaryTitle() );
					secondaryTitle.setTextColor( pageConf.getSecondaryTitleTextColor() );
					
					// Main Msg layout background
					TapReasonUtils.setBackgroundColor( activity.findViewById( R.id.tapReasonMainMsgLayout ), 
														pageConf.getHeaderBGColor(), 0 );					
					
					// Option1 btn				
					TextView option1BtnText = (TextView)activity.findViewById( R.id.tapReasonOption1BtnTxt );
					option1BtnText.setText( pageConf.getOption1BtnText() );					
					TapReasonUtils.setTextViewStyle( option1BtnText, pageConf.isOption1BtnTextIsBold() );				
					
					// Option1 Background color
					TapReasonUtils.setBackgroundColor( activity.findViewById( R.id.tapReasonOption1Btn ), 
														pageConf.getOption1BtnBGColor(),
														pageConf.getSelectionBGColor(),
														0 );

					// Text Color
					option1BtnText.setTextColor( pageConf.getOption1BtnTextColor() );
					
					TapReasonNetworkImageView option1Img = (TapReasonNetworkImageView)activity.findViewById( R.id.tapReasonOption1BtnIcn );
					
					if ( pageConf.isOption1BtnTextShowIcon() )
					{
						option1Img.loadUrl( pageConf.getOption1BtnUrl(), pageConf.getOption1BtnResourceId() );
						option1Img.setVisibility( View.VISIBLE );
						
						LayoutParams layoutParams = new LayoutParams( option1Img.getLayoutParams());
						layoutParams.addRule( RelativeLayout.CENTER_IN_PARENT );
						
						if ( TapReason.getConf().isRtl() )
						{
							layoutParams.addRule( RelativeLayout.RIGHT_OF, R.id.tapReasonOption1BtnTxt );
						}
						else
						{
							layoutParams.addRule( RelativeLayout.LEFT_OF, R.id.tapReasonOption1BtnTxt );						
						}
						
						option1Img.setLayoutParams( layoutParams );
					}
					else
					{
						option1Img.setVisibility( View.GONE );
					}
			
					
					// Option 2 Btn
					View option2BtnLayout = activity.findViewById( R.id.tapReasonOption2Btn );
					if ( pageConf.isShowOption2Btn() )
					{
						option2BtnLayout.setVisibility( View.VISIBLE );
						
						TextView text = (TextView)activity.findViewById( R.id.tapReasonOption2BtnTxt);
						text.setText( pageConf.getOption2BtnText() );					
						TapReasonUtils.setTextViewStyle( text, pageConf.isOption2BtnTextIsBold() );				
						
						// Option2 Background color
						TapReasonUtils.setBackgroundColor( option2BtnLayout, 
															pageConf.getOption2BtnBGColor(),
															pageConf.getSelectionBGColor(),
															R.color.tap_reason_3_btn_style2_border_color );

						// Text Color
						text.setTextColor( pageConf.getOption2BtnTextColor() );
						
					}
					else
					{
						option2BtnLayout.setVisibility( View.GONE );								
					}
					
					
					// Option 3 Btn
					View option3BtnLayout = activity.findViewById( R.id.tapReasonOption3Btn );
					if ( pageConf.isShowOption3Btn() )
					{
						option3BtnLayout.setVisibility( View.VISIBLE );
						
						TextView text = (TextView)activity.findViewById( R.id.tapReasonOption3BtnTxt);

						text.setText( pageConf.getOption3BtnText() );					
						TapReasonUtils.setTextViewStyle( text, pageConf.isOption3BtnTextIsBold() );				
						
						//Background color
						TapReasonUtils.setBackgroundColor( option3BtnLayout, 
															pageConf.getOption3BtnBGColor(),
															pageConf.getSelectionBGColor(),
															R.color.tap_reason_3_btn_style2_border_color );

						// Text Color
						text.setTextColor( pageConf.getOption3BtnTextColor() );						
					}
					else
					{
						option3BtnLayout.setVisibility( View.GONE );
					}						
					break;
				}
				case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_2:
				{					
					// Top icon
					TapReasonNetworkImageView img = (TapReasonNetworkImageView)activity.findViewById( R.id.tapReasonTopImg );
					img.setVisibility( View.VISIBLE );
					img.loadUrl( pageConf.getTopIconUrl(), pageConf.getTopIconResourceId() );	
					
					TapReasonUtils.createRoundView( activity.findViewById( R.id.tapReasonTopImgWrapper ), pageConf.getHeaderBGColor(), 0 );
					
					// Body layout
					TapReasonUtils.setBackgroundColor( activity.findViewById( R.id.tapReasonBodyLayout ), pageConf.getBodyBGColor() );											
					
					// Main title
					TextView mainTitle = (TextView)activity.findViewById( R.id.tapReasonMainHeaderTxt);
					mainTitle.setText( pageConf.getMainTitle() );
					mainTitle.setTextColor( pageConf.getMainTitleTextColor() );
					
					// Secondary msg
					TextView secondaryTitle = (TextView)activity.findViewById( R.id.tapReasonSecondaryHeaderTxt);
					secondaryTitle.setText( pageConf.getSecondaryTitle() );
					secondaryTitle.setTextColor( pageConf.getSecondaryTitleTextColor() );

					// Option1 btn				
					TextView option1BtnText = (TextView)activity.findViewById( R.id.tapReasonOption1BtnTxt );
					option1BtnText.setText( pageConf.getOption1BtnText() );					
					TapReasonUtils.setTextViewStyle( option1BtnText, pageConf.isOption1BtnTextIsBold() );				

					// Option1 Color view BG
					TapReasonUtils.setBackgroundColor( activity.findViewById( R.id.tapReasonOption1ColorView ), 
							pageConf.getOption1BtnBGColor(), 
							pageConf.getSelectionBGColor() );
					
					// Option1 Background color
					TapReasonUtils.setBackgroundColor( activity.findViewById( R.id.tapReasonOption1Btn ), 
														Color.TRANSPARENT,
														pageConf.getSelectionBGColor(),
														R.color.tap_reason_3_btn_style2_border_color );

					// Text Color
					option1BtnText.setTextColor( pageConf.getOption1BtnTextColor() );
					
					TapReasonNetworkImageView option1Img = (TapReasonNetworkImageView)activity.findViewById( R.id.tapReasonOption1BtnIcn );
					
					if ( pageConf.isOption1BtnTextShowIcon() )
					{
						option1Img.loadUrl( pageConf.getOption1BtnUrl(), pageConf.getOption1BtnResourceId() );						
						option1Img.setVisibility( View.VISIBLE );
						
						LayoutParams layoutParams = new LayoutParams( option1Img.getLayoutParams());
						layoutParams.addRule( RelativeLayout.CENTER_IN_PARENT );
						
						if ( TapReason.getConf().isRtl() )
						{
							layoutParams.addRule( RelativeLayout.RIGHT_OF, R.id.tapReasonOption1BtnTxt );
						}
						else
						{
							layoutParams.addRule( RelativeLayout.LEFT_OF, R.id.tapReasonOption1BtnTxt );						
						}
						
						option1Img.setLayoutParams( layoutParams );
					}
					else
					{
						option1Img.setVisibility( View.GONE );
					}
					
					// Option 2 Btn
					View option2BtnLayout = activity.findViewById( R.id.tapReasonOption2Btn );
					if ( pageConf.isShowOption2Btn() )
					{
						option2BtnLayout.setVisibility( View.VISIBLE );
						
						TextView text = (TextView)activity.findViewById( R.id.tapReasonOption2BtnTxt);
						text.setText( pageConf.getOption2BtnText() );					
						TapReasonUtils.setTextViewStyle( text, pageConf.isOption2BtnTextIsBold() );				

						// Option2 Color view BG
						TapReasonUtils.setBackgroundColor( activity.findViewById( R.id.tapReasonOption2ColorView ), 
								pageConf.getOption2BtnBGColor(), 
								pageConf.getSelectionBGColor() );
						
						// Option2 Background color
						TapReasonUtils.setBackgroundColor( option2BtnLayout, 
															Color.TRANSPARENT,
															pageConf.getSelectionBGColor(),
															R.color.tap_reason_3_btn_style2_border_color );

						// Text Color
						text.setTextColor( pageConf.getOption2BtnTextColor() );
						
					}
					else
					{
						option2BtnLayout.setVisibility( View.GONE );								
					}
					
					
					// Option 3 Btn
					View option3BtnLayout = activity.findViewById( R.id.tapReasonOption3Btn );
					if ( pageConf.isShowOption3Btn() )
					{
						option3BtnLayout.setVisibility( View.VISIBLE );
						
						TextView text = (TextView)activity.findViewById( R.id.tapReasonOption3BtnTxt);

						text.setText( pageConf.getOption3BtnText() );					
						TapReasonUtils.setTextViewStyle( text, pageConf.isOption3BtnTextIsBold() );				

						//Color view BG
						TapReasonUtils.setBackgroundColor( activity.findViewById( R.id.tapReasonOption3ColorView ), 
								pageConf.getOption3BtnBGColor(), 
								pageConf.getSelectionBGColor() );
						
						//Background color
						TapReasonUtils.setBackgroundColor( option3BtnLayout, 
															Color.TRANSPARENT,
															pageConf.getSelectionBGColor(),
															R.color.tap_reason_3_btn_style2_border_color );

						// Text Color
						text.setTextColor( pageConf.getOption3BtnTextColor() );						
					}
					else
					{
						option3BtnLayout.setVisibility( View.GONE );
					}				
					break;
				}				
				case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_1:
				{
					// Top icon
					TapReasonNetworkImageView img = (TapReasonNetworkImageView)activity.findViewById( R.id.tapReasonTopImg );
					if ( pageConf.isShowTopIcon() )
					{
						img.setVisibility( View.VISIBLE );
						img.loadUrl( pageConf.getTopIconUrl(), pageConf.getTopIconResourceId() );		
					}
					else
					{
						img.setVisibility( View.GONE );
					}
										
					// Main Title		
					TextView mainTitle = (TextView)activity.findViewById( R.id.tapReasonMainHeaderTxt );
					mainTitle.setText( pageConf.getMainTitle() );
					mainTitle.setTextColor( pageConf.getMainTitleTextColor() );

					// Secondary title
					TextView secondaryTitle = (TextView)activity.findViewById( R.id.tapReasonSecondaryHeaderTxt );
					secondaryTitle.setText( pageConf.getSecondaryTitle()  );
					secondaryTitle.setTextColor( pageConf.getSecondaryTitleTextColor() );
					
					// Header BG
					View mainMsgLayout = activity.findViewById( R.id.tapReasonMainHeaderLayout );	
					ImageView zigZagsImg = (ImageView)activity.findViewById( R.id.tapReasonHeaderSeparaterImg );
					TapReasonUtils.setBackgroundColor( mainMsgLayout, pageConf.getHeaderBGColor() );
						
					// Header separator zig zags, adjusting their color
					TapReasonUtils.changeDrawableColor( zigZagsImg.getDrawable(), pageConf.getHeaderBGColor() );																		
					
					// Body BG 
					View bodyLayout = activity.findViewById( R.id.tapReasonBodyLayout );				
					TapReasonUtils.setBackgroundColor( bodyLayout, pageConf.getBodyBGColor() );									
									
					// Option1 + Icon 
					TextView option1Text = (TextView)activity.findViewById( R.id.tapReasonOption1BtnTxt );
					TapReasonNetworkImageView option1Img = (TapReasonNetworkImageView)activity.findViewById( R.id.tapReasonOption1BtnIcn );
					View option1Layout = activity.findViewById( R.id.tapReasonOption1BtnLayout );	
					option1Text.setText( pageConf.getOption1BtnText() );						
					TapReasonUtils.setTextViewStyle( option1Text, pageConf.isOption1BtnTextIsBold() );				
						
					// Option1 background color
					TapReasonUtils.setBackgroundColor( option1Layout, 
														pageConf.getOption1BtnBGColor(), 
														pageConf.getSelectionBGColor(),
														R.color.tap_reason_style2_border_color );
					option1Text.setTextColor( pageConf.getOption1BtnTextColor() );
					
					option1Img.loadUrl( pageConf.getOption1BtnUrl(), pageConf.getOption1BtnResourceId() );				
						
					
					// Option2 Btn + Icon
					TextView option2Text = (TextView)activity.findViewById( R.id.tapReasonOption2BtnTxt );
					TapReasonNetworkImageView option2Img = (TapReasonNetworkImageView)activity.findViewById( R.id.tapReasonOption2BtnIcn );								

					// Option2 BG
					TapReasonUtils.setBackgroundColor( activity.findViewById( R.id.tapReasonOption2BtnLayout ),
							android.R.color.transparent,
							pageConf.getSelectionBGColor(),
							R.color.tap_reason_style2_border_color );	
					option2Text.setText( pageConf.getOption2BtnText() );
					
					TapReasonUtils.setTextViewStyle( option2Text, pageConf.isOption2BtnTextIsBold() );				
					
					option2Text.setTextColor( pageConf.getOption2BtnTextColor() );
					option2Img.loadUrl( pageConf.getOption2BtnUrl(), pageConf.getOption2BtnResourceId() );
					
					// Options Separator Layout					
					TapReasonUtils.createRoundView( activity.findViewById( R.id.tapReasonOptionsSeparatorLayout ), 
													pageConf.getHeaderBGColor(), pageConf.getBodyBGColor() );
					
					// Options separator text
					TextView optionsSeparatorTxt = ( TextView )activity.findViewById( R.id.tapReasonOptionsSeparatorTxt );
					optionsSeparatorTxt.setText( pageConf.getOptionsSeparatorText() );
					optionsSeparatorTxt.setTextColor( pageConf.getBodyBGColor() );				
					break;
				}
				case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_2:
				{
					// Top icon
					TapReasonNetworkImageView img = (TapReasonNetworkImageView)activity.findViewById( R.id.tapReasonTopImg );
					img.loadUrl( pageConf.getTopIconUrl(), pageConf.getTopIconResourceId() );

					if ( pageConf.getHeaderBGColor() != 0 )
					{
						// Round view behind the top icon
						TapReasonUtils.createRoundView( activity.findViewById( R.id.tapReasonTopImgWrapper ), pageConf.getHeaderBGColor(), 0 );							
					}				
					
					// Main Title		
					TextView mainTitle = (TextView)activity.findViewById( R.id.tapReasonMainHeaderTxt );
					mainTitle.setText( pageConf.getMainTitle() );
					mainTitle.setTextColor( pageConf.getMainTitleTextColor() );

					// Secondary title
					TextView secondaryTitle = (TextView)activity.findViewById( R.id.tapReasonSecondaryHeaderTxt );
					secondaryTitle.setText( pageConf.getSecondaryTitle()  );
					secondaryTitle.setTextColor( pageConf.getSecondaryTitleTextColor() );																	
					
					// Body BG 
					View bodyLayout = activity.findViewById( R.id.tapReasonBodyLayout );				
					TapReasonUtils.setBackgroundColor( bodyLayout, pageConf.getBodyBGColor() );									
									
					// Option1 + Icon 
					TextView option1Text = (TextView)activity.findViewById( R.id.tapReasonOption1BtnTxt );
					TapReasonNetworkImageView option1Img = (TapReasonNetworkImageView)activity.findViewById( R.id.tapReasonOption1BtnIcn );
					View option1Layout = activity.findViewById( R.id.tapReasonOption1BtnLayout );	
					option1Text.setText( pageConf.getOption1BtnText() );						
					TapReasonUtils.setTextViewStyle( option1Text, pageConf.isOption1BtnTextIsBold() );				
					
					TapReasonUtils.setBackgroundColor( option1Layout, 
														Color.TRANSPARENT, 
														pageConf.getSelectionBGColor() );
					option1Text.setTextColor( pageConf.getOption1BtnTextColor() );
					
					option1Img.loadUrl( pageConf.getOption1BtnUrl(), pageConf.getOption1BtnResourceId() );				
						
					
					// Option2 Btn + Icon
					TextView option2Text = (TextView)activity.findViewById( R.id.tapReasonOption2BtnTxt );
					TapReasonNetworkImageView option2Img = (TapReasonNetworkImageView)activity.findViewById( R.id.tapReasonOption2BtnIcn );								

					TapReasonUtils.setBackgroundColor( activity.findViewById( R.id.tapReasonOption2BtnLayout ),
														Color.TRANSPARENT,
														pageConf.getSelectionBGColor() );	
					option2Text.setText( pageConf.getOption2BtnText() );
					
					TapReasonUtils.setTextViewStyle( option2Text, pageConf.isOption2BtnTextIsBold() );				
					
					option2Text.setTextColor( pageConf.getOption2BtnTextColor() );
					option2Img.loadUrl( pageConf.getOption2BtnUrl(), pageConf.getOption2BtnResourceId() );				
					break;
				}				
			}		
		}
		catch ( Exception exception )
		{
			Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "Error configuring page", exception );
		}
	}	
}
