package com.likethatapps.services.api;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

/**
 * Created by eladyarkoni on 5/13/14.
 */
public class LocationServiceImpl implements OnLocationUpdatedListener, ILocationService {

    private Location mLocation = null;
    private Context context;
    private static final String TAG = "location-service";
    private Geocoder mGeocoder;

    protected LocationServiceImpl(Context context) {
        this.context = context;
        mGeocoder = new Geocoder(context);
        requestLocation();
    }

    public void requestLocation() {
        SmartLocation.with(this.context).location().oneFix().start(this);
    }

    public Location getCurrentLocation() {
        return mLocation;
    }

    public Address getCurrentAddress() {
        List<Address> addresses;
        try {
            LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            addresses = mGeocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                return addresses.get(0);
            }
        } catch (IOException e) {
            // Do nothing, can't parse address
        }
        return null;

    }

    public Address getLocationAddress(double lat, double lng) {
        List<Address> addresses;
        try {
            LatLng latLng = new LatLng(lat, lng);
            addresses = mGeocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                return addresses.get(0);
            }
        } catch (IOException e) {
            // Do nothing, can't parse address
        }
        return null;
    }

    @Override
    public void onLocationUpdated(Location location) {
        if (location != null) {
            mLocation = location;
            Log.d(TAG, "location: " + location.getAltitude() + "," + location.getLongitude());
        }
    }
}
