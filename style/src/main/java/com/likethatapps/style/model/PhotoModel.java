package com.likethatapps.style.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by eladyarkoni on 9/8/14.
 */
public class PhotoModel implements Parcelable, Serializable {

    private String id;
    private String croppedOriginalId;
    private String url;
    private String thumbUrl;
    private String title;
    private String source;
    private String merchant;
    private String description;
    private String searchHost;
    private String pageUrl;
    private Double distance;
    private Double price;
    private double latitude;
    private double longitude;
    private String[] clusterKeywords;
    private List<String> generics;
    private List<String> specifics;
    private List<String> descriptors;
    private int width;
    private int height;

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(croppedOriginalId);
        parcel.writeString(url);
        parcel.writeString(thumbUrl);
        parcel.writeString(title);
        parcel.writeString(source);
        parcel.writeString(description);
        parcel.writeString(searchHost);
        parcel.writeString(pageUrl);
        parcel.writeDouble(price);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeInt(width);
        parcel.writeInt(height);
        parcel.writeInt(clusterKeywords == null ? 0 : clusterKeywords.length);
        for( int index=0; clusterKeywords != null && index < clusterKeywords.length; index++ ){
            parcel.writeString(clusterKeywords[index]);
        }
    }

    public static final Creator<PhotoModel> CREATOR = new Creator<PhotoModel>() {
        @Override
        public PhotoModel createFromParcel(Parcel parcel) {
            PhotoModel photoModel = new PhotoModel();
            photoModel.id = parcel.readString();
            photoModel.croppedOriginalId = parcel.readString();
            photoModel.url = parcel.readString();
            photoModel.thumbUrl = parcel.readString();
            photoModel.title = parcel.readString();
            photoModel.source = parcel.readString();
            photoModel.description = parcel.readString();
            photoModel.searchHost = parcel.readString();
            photoModel.pageUrl = parcel.readString();
            photoModel.price = parcel.readDouble();
            photoModel.latitude = parcel.readDouble();
            photoModel.longitude = parcel.readDouble();
            photoModel.width = parcel.readInt();
            photoModel.height = parcel.readInt();
            int size = parcel.readInt();
            photoModel.clusterKeywords = new String[size];
            for( int i=0; i<size; i++ ){
                photoModel.clusterKeywords[i] = parcel.readString();
            }
            return photoModel;
        }

        @Override
        public PhotoModel[] newArray(int i) {
            return new PhotoModel[i];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getSearchHost() {
        return searchHost;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public Double getPrice() {
        if (price == null) {
            return new Double(0);
        }
        return Math.floor(price + 0.5d);
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String[] getClusterKeywords() {
        return clusterKeywords;
    }

    public List<String> getGenerics() {
        return generics;
    }

    public List<String> getSpecifics() {
        return specifics;
    }

    public List<String> getDescriptors() {
        return descriptors;
    }

    public Double getDistance() {
        return distance;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSearchHost(String searchHost) {
        this.searchHost = searchHost;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setClusterKeywords(String[] clusterKeywords) {
        this.clusterKeywords = clusterKeywords;
    }

    public void setGenerics(List<String> generics) {
        this.generics = generics;
    }

    public void setSpecifics(List<String> specifics) {
        this.specifics = specifics;
    }

    public void setDescriptors(List<String> descriptors) {
        this.descriptors = descriptors;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public static Creator<PhotoModel> getCreator() {
        return CREATOR;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getMerchantOrSource() {
        if (this.merchant != null && !this.merchant.isEmpty()) {
            return this.merchant;
        }
        return this.source;
    }

    public String getCroppedOriginalId() {
        return this.croppedOriginalId;
    }

    public String getStatsImageId() {
        if (this.croppedOriginalId != null && this.croppedOriginalId.length() > 0) {
            return this.croppedOriginalId;
        }
        return this.id;
    }
}
