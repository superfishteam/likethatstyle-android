package com.likethatapps.services.api;

import android.graphics.Point;
import android.location.Location;

import com.likethatapps.services.api.http.AjaxCallback;
import com.likethatapps.services.api.types.SearchSource;

import java.util.List;

/**
 * Created by eladyarkoni on 5/12/14.
 */
public interface ISearchService {

    public void startSearch(byte[] data, SearchSource source, int numOfResults, Location location, int cropX, int cropY, int width, int height, final AjaxCallback callback);
    public void startSearchWithCropPath(byte[] data, List<Point> cropPathArray, SearchSource source, int numOfResults, Location location, int cropX, int cropY, int width, int height, final AjaxCallback callback);
    public void startSearch(byte[] data, SearchSource source, int numOfResults, Location location, final AjaxCallback callback);
    public void startSearch(String imageUrl, SearchSource source, String imageTitle, String categoryName, String clusterKeyword, String textSearch, int numOfResults, Location location, final AjaxCallback callback);
    public void startSearch(String imageId, int numOfResults, final AjaxCallback callback);
    public void setScope(String scope);
    public String getScope();
    public void setServerDomain(String serverDomain);
    public String getServerDomain();
}
