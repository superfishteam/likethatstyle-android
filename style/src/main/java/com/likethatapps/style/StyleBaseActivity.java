package com.likethatapps.style;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.likethatapps.services.api.SFApi;
import com.likethatapps.services.api.types.SearchSource;
import com.likethatapps.services.utils.ImageUtils;
import com.likethatapps.services.utils.Utils;
import com.likethatapps.style.service.ReportService;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;
import com.tapreason.sdk.TapReason;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by eladyarkoni on 7/2/15.
 */
public class StyleBaseActivity extends FragmentActivity {

    protected static final int PICK_IMAGE_REQUEST = 500;

    private Activity mActivity;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private View mSearchButton;
    private static Map<String, Boolean> mClassLoaded = new HashMap<>();

    public void onCreateBaseActivity(Activity activity, Bundle savedInstanceState, int layout) {
        super.onCreate(savedInstanceState);
        mActivity = activity;
        setContentView(R.layout.style_base_activity);
        ViewGroup mainLayout = (ViewGroup)getLayoutInflater().inflate(layout,null);
        ((RelativeLayout)findViewById(R.id.content_frame)).addView(mainLayout);
        if (showBar()) {
            renderBar();
        } else {
            findViewById(R.id.title_bar).setVisibility(View.GONE);
        }
        createSideMenu();
        if (hasCameraSearchBtn()) {
            mSearchButton = findViewById(R.id.btn_camera);
            mSearchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ReportService.getInstance().report(3015, "searchbyphoto_clicked");
                    navigateToActivity(CameraActivity.class, null, true, Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }
            });
            if (!mClassLoaded.containsKey(mActivity.getLocalClassName())) {
                final AnimationSet rollingIn = new AnimationSet(true);
                Animation moving = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 5, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
                moving.setDuration(1000);
                final Animation rotating = new RotateAnimation(720, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotating.setDuration(1000);
                rollingIn.addAnimation(rotating);
                rollingIn.addAnimation(moving);
                mSearchButton.startAnimation(rollingIn);
                mClassLoaded.put(mActivity.getLocalClassName(), true);
            }

        } else {
            findViewById(R.id.btn_camera).setVisibility(View.GONE);
        }
    }

    protected View getCameraSearchButton() {
        return mSearchButton;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this, getString(R.string.facebook_id));
    }

    @Override
    protected void onStart() {
        super.onStart();
        TapReason.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        TapReason.unRegister(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this, getString(R.string.facebook_id));
    }

    public void createSideMenu() {
        final String appVersion = SFApi.getInstance().getAppVersion(this);
        findViewById(R.id.action_bar_btn_hamburger).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPressingHamburgerButton();
            }
        });
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return 9;
            }

            @Override
            public int getViewTypeCount() {
                return 2;
            }

            @Override
            public int getItemViewType(int position) {
                if (position > 4) {
                    return 1;
                }
                return 0;
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View convertView, ViewGroup viewGroup) {
                View view = convertView;
                int type = getItemViewType(i);
                if (view == null) {
                    if (type == 0) {
                        view = getLayoutInflater().inflate(R.layout.sidemenu_list_item, null);
                    } else if (type == 1) {
                        view = getLayoutInflater().inflate(R.layout.sidemenu_promo_app_item, null);
                    }
                }
                ImageView image = (ImageView) view.findViewById(R.id.image);
                TextView title = ((TextView) view.findViewById(R.id.title));
                TextView subtitle = ((TextView) view.findViewById(R.id.subtitle));
                subtitle.setVisibility(View.GONE);
                image.setVisibility(View.GONE);
                view.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                title.setTextSize(16);
                title.setTypeface(null, Typeface.NORMAL);
                title.setTextColor(Color.parseColor("#ffffff"));
                switch (i) {
                    case 0:
                        title.setText(R.string.sidemenu_home);
                        break;
                    case 1:
                        title.setText(R.string.sidemenu_share);
                        break;
                    case 2:
                        title.setText(R.string.sidemenu_about);
                        subtitle.setVisibility(View.VISIBLE);
                        subtitle.setText(appVersion);
                        break;
                    case 3:
                        title.setText(R.string.sidemenu_feedback);
                        break;
                    case 4:
                        title.setText(R.string.sidemenu_history);
                        break;
                    case 5:
                        view.setBackgroundColor(Color.parseColor("#ffffff"));
                        title.setText(R.string.xpromo_title);
                        title.setTextColor(Color.parseColor("#000000"));
                        title.setTextSize(14);
                        title.setTypeface(null, Typeface.BOLD);
                        break;
                    case 6:
                        title.setText("LikeThat Décor");
                        subtitle.setVisibility(View.VISIBLE);
                        subtitle.setText(R.string.xpromo_ltd);
                        image.setVisibility(View.VISIBLE);
                        image.setImageResource(R.drawable.ico_decor);
                        break;
                    case 7:
                        title.setText("LikeThat Pets");
                        subtitle.setVisibility(View.VISIBLE);
                        subtitle.setText(R.string.xpromo_ltp);
                        image.setVisibility(View.VISIBLE);
                        image.setImageResource(R.drawable.ico_pets);
                        break;
                    case 8:
                        title.setText("LikeThat Garden");
                        subtitle.setVisibility(View.VISIBLE);
                        subtitle.setText(R.string.xpromo_ltg);
                        image.setVisibility(View.VISIBLE);
                        image.setImageResource(R.drawable.ico_garden);
                        break;
                }
                return view;
            }
        });
        mDrawerList.setOnItemClickListener(new SideMenuListener());
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
//                ReportService.getInstance().report(27, "hamburger_menu_opened", null, null, null, null);
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void onPressingHamburgerButton() {
        if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            mDrawerLayout.openDrawer(mDrawerList);
            ReportService.getInstance().report(3043, "hamburger_menu_opened");
        }
    }

    public class SideMenuListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Map<String, String> extraData = new HashMap<String, String>();
            switch (i){
                case 0:
                    // go to home
                    if (!(mActivity instanceof HomeActivity)) {
                        navigateToActivity(HomeActivity.class, null, true, Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    }
                    break;
                case 1:
                    StyleApplication.sendShareMail(mActivity);
                    break;
                case 2:
                    // go to about
                    if (!(mActivity instanceof AboutActivity)) {
                        ReportService.getInstance().report(3044, "hamburger_about_opened");
                        navigateToActivity(AboutActivity.class, null, false, -1);
                    }
                    break;
                case 3:
                    StyleApplication.sendFeedback(mActivity);
                    break;
                case 4:
                    // go to history
                    if (!(mActivity instanceof HistoryActivity)) {
                        ReportService.getInstance().report(3054, "hamburger_history_opened");
                        navigateToActivity(HistoryActivity.class, null, false, -1);
                    }
                    break;
                case 6:
                    extraData.put("AppName", "LikeThatDecor");
//                    ReportService.getInstance().report(1037, "xPromo_app_clicked", null, null, null, extraData);
                    Utils.openAppByPackageName(mActivity, "com.likethatapps.decormatch");
                    break;
                case 7:
                    extraData.put("AppName", "LikeThatPets");
//                    ReportService.getInstance().report(1037, "xPromo_app_clicked", null, null, null, extraData);
                    Utils.openAppByPackageName(mActivity, "com.likethatapps.petmatchapp");
                    break;
                case 8:
                    extraData.put("AppName", "LikeThatGarden");
//                    ReportService.getInstance().report(1037, "xPromo_app_clicked", null, null, null, extraData);
                    Utils.openAppByPackageName(mActivity, "com.likethatapps.garden");
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot() && !(mActivity instanceof HomeActivity)) {
            navigateToActivity(HomeActivity.class, null, true, -1);
        } else {
            super.onBackPressed();
        }
    }

    protected boolean showBar() {
        return true;
    }

    protected void navigateToActivity(Class activityClass, Bundle intentExtras, boolean finish, int flags) {
        Intent intent = new Intent(mActivity, activityClass);
        if (intentExtras != null) {
            intent.putExtras(intentExtras);
        }
        if (flags != -1) {
            intent.setFlags(flags);
        }
        startActivity(intent);
        if (finish) {
            finish();
        }
    }

    protected int barColor() {
        return getResources().getColor(R.color.color1);
    }

    private void renderBar() {
        findViewById(R.id.title_bar).setBackgroundColor(barColor());
        updateBarTitle(getBarTitle());
        if (mActivity instanceof WatchListActivity) {
            findViewById(R.id.action_bar_btn_eye).setSelected(true);
        } else {
            findViewById(R.id.action_bar_btn_eye).setSelected(false);
            findViewById(R.id.action_bar_btn_eye).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToActivity(WatchListActivity.class, null, false, -1);
                }
            });
        }
    }

    protected String getBarTitle() {
        return null;
    }

    protected void updateBarTitle(String title) {
        if (title != null) {
            ((TextView) findViewById(R.id.topbar_title)).setText(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return false;
    }

    public void selectPhotoFromGallery() {
        if (StyleApplication.getCounter("cameraroll_select_photo") == 0) {
            ReportService.getInstance().report(3060, "FTUE_cameraroll_education_opened");
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.cameraroll_help_tip, null);
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.setView(dialogView, 0,0,0,0);
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogView.findViewById(R.id.tip_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ReportService.getInstance().report(3062, "FTUE_cameraroll_education_searched");
                    Bundle bundle = new Bundle();
                    bundle.putString(Consts.EXTRA_DATA_SEARCH_SOURCE, SearchSource.FTUE.toString());
                    bundle.putInt(Consts.EXTRA_DATA_IMAGE_RESOURCE_ID, R.drawable.tipcamerarollimage);
                    navigateToActivity(CropImageActivity.class, bundle, true, -1);
                }
            });
            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    ReportService.getInstance().report(3061, "FTUE_cameraroll_education_skipped");
                    startCameraRollIntent();
                }
            });
            dialogView.findViewById(R.id.skip_text).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ReportService.getInstance().report(3061, "FTUE_cameraroll_education_skipped");
                    alertDialog.dismiss();
                    startCameraRollIntent();
                }
            });
            StyleApplication.increaseCounter("cameraroll_select_photo");
        } else {
            startCameraRollIntent();
        }
    }

    public void startCameraRollIntent() {
        ReportService.getInstance().report(3017, "cameraroll_screen_opened");
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {
            Uri _uri = data.getData();
            String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION};
            Cursor cursor = getContentResolver().query(_uri, columns, null, null, null);
            cursor.moveToFirst();
            int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            int orientationColumn = -1;
            try {
                orientationColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.ORIENTATION);
            } catch (IllegalArgumentException ex) {
                orientationColumn = -1;
            }

            final String imageFilePath = cursor.getString(dataColumn);
            final int orientation = orientationColumn != -1 ? cursor.getInt(orientationColumn) : -1;
            cursor.close();
            try {
                photoFromGallerySelected(_uri.toString(), orientation);
            } catch (Exception ex) {}
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void photoFromGallerySelected(String uri, int orientation) {
        Uri imageUri = Uri.parse(uri);
        try {
            ReportService.getInstance().report(3019, "photo_uploaded");
            Bundle extras = new Bundle();
            extras.putString(Consts.EXTRA_DATA_SEARCH_SOURCE, SearchSource.ALBUM.toString());
            extras.putString(Consts.EXTRA_DATA_IMAGE_URI, imageUri.toString());
            extras.putBoolean(Consts.EXTRA_DATA_FIRST_TIME_BOOLEAN, false);
            extras.putInt(Consts.EXTRA_DATA_IMAGE_ORIENTATION, orientation);
            navigateToActivity(CropImageActivity.class, extras, false, -1);
        } catch (Exception ex) {}
    }

    public class FirstGalleryImageAsyncTask extends AsyncTask<Void, Void, Bitmap> {

        ImageView image;
        Context context;

        public FirstGalleryImageAsyncTask(Context context, ImageView image) {
            this.image = image;
            this.context = context;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            String[] projection = new String[]{
                    MediaStore.Images.ImageColumns._ID,
                    MediaStore.Images.ImageColumns.DATA,
                    MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                    MediaStore.Images.ImageColumns.DATE_TAKEN,
                    MediaStore.Images.ImageColumns.MIME_TYPE
            };
            final Cursor cursor = getContentResolver()
                    .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null,
                            null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");
            if (cursor.moveToFirst()) {
                String imageLocation = cursor.getString(1);
                File imageFile = new File(imageLocation);
                if (!imageFile.exists()) {return null;}
                try {
                    byte[] imageData = ImageUtils.getImageByteArray(context, Uri.fromFile(imageFile));
                    byte[] rotatedImage = ImageUtils.decodeImageData(imageData);
                    if (rotatedImage == null) {
                        return null;
                    }
                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                    return ImageUtils.scaleBitmap(ImageUtils.getBitmap(rotatedImage), Math.round(45 * metrics.density), Math.round(45 * metrics.density));
                } catch (IOException ex) {
                    return null;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap imageBitmap) {
            if (imageBitmap != null) {
                image.setImageBitmap(imageBitmap);
            }
        }
    }

    protected boolean hasCameraSearchBtn() {
        return true;
    }

    protected ToolTipView showTooltip(int layout, View view, boolean autoremove) {
        ToolTip toolTip = new ToolTip()
                .withContentView(View.inflate(this, layout, null))
                .withColor(getResources().getColor(R.color.color1))
                .withAnimationType(ToolTip.AnimationType.FROM_TOP);
        final ToolTipView toolTipView = ((ToolTipRelativeLayout) findViewById(R.id.tooltips_relative_view)).showToolTipForView(toolTip, view);
        if (autoremove) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    toolTipView.remove();
                }
            },2000);
        }
        return toolTipView;
    }
}
