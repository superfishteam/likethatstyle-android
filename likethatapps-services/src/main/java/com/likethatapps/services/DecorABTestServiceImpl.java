package com.likethatapps.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.likethatapps.services.api.IABTestService;
import com.likethatapps.services.api.http.Ajax;
import com.likethatapps.services.api.http.AjaxCallback;
import com.likethatapps.services.api.model.abtest.UserDataModel;

/**
 * Created by eladyarkoni on 11/12/14.
 */
public class DecorABTestServiceImpl implements IABTestService {

    private static final String SERVICE_URL = "http://www.likethatapps.com/decor/service/abtest/userdata/";
    private static final String ABTEST_PREFERENCES_KEY = "ABTestData";
    private static final String ABTEST_DATA_KEY = "ABTestData";
    private static final String TAG = "abtest-service";

    private UserDataModel userDataModel;
    private Context context;
    private String userId;
    private SharedPreferences preferences;

    public DecorABTestServiceImpl(Context context, String userId) {
        this.context = context;
        this.userId = userId;
        this.preferences = this.context.getSharedPreferences(ABTEST_PREFERENCES_KEY, 0);
        String lastAbDataStr = this.preferences.getString(ABTEST_DATA_KEY, null);
        if (lastAbDataStr != null) {
            try {
                this.userDataModel = new Gson().fromJson(lastAbDataStr, UserDataModel.class);
            } catch (Exception ex) {
                Log.d(TAG, "last abtest data is corrupted");
            }
        }
    }

    @Override
    public void updateUserData() {
        Ajax.get(SERVICE_URL + this.userId, new AjaxCallback() {
            @Override
            public void success(String result) {
                preferences.edit().putString(ABTEST_DATA_KEY, result).commit();
                userDataModel = new Gson().fromJson(result, UserDataModel.class);
            }
        });
    }

    @Override
    public UserDataModel getUserData() {
        return userDataModel;
    }
}
