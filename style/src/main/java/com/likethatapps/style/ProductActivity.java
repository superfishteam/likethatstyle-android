package com.likethatapps.style;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.etsy.android.grid.StaggeredGridView;
import com.google.gson.Gson;
import com.likethatapps.services.api.SFApi;
import com.likethatapps.services.api.http.AjaxCallback;
import com.likethatapps.style.model.PhotoModel;
import com.likethatapps.style.model.ResultModel;
import com.likethatapps.style.service.ReportService;
import com.likethatapps.style.service.WatchListService;
import com.tapreason.sdk.TapReason;
import com.tapreason.sdk.TapReasonCustomEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by eladyarkoni on 7/5/15.
 */
public class ProductActivity extends StyleBaseActivity implements AdapterView.OnItemClickListener, ResultImageAdapter.ResultImageAdapterActivity{

    private ResultImageAdapter mAdapter;
    private RelativeLayout mShareImageLayout;
    private String mSearchId;
    private long mStartMilliseconds = 0;
    private long mServerResponseMilliseconds = 0;
    private PhotoModel mPhotoModel;
    private Spinner mSortSpinner;
    private Spinner mSizesSpinner;
    private int mNumOfSearches;
    private StaggeredGridView mGridView;
    private TextView mProductCountTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        onCreateBaseActivity(this, savedInstanceState, R.layout.activity_product);
        mNumOfSearches = 0;
        mStartMilliseconds = System.currentTimeMillis();
        mPhotoModel = (PhotoModel)getIntent().getSerializableExtra(Consts.EXTRA_DATA_PHOTO_MODEL_SERIALIZABLE);
        mSearchId = getIntent().getStringExtra(Consts.EXTRA_DATA_SEARCH_ID_STRING);
        mGridView = (StaggeredGridView)findViewById(R.id.grid_view);
        View headerLayout = getHeaderLayout(mGridView, mPhotoModel);
        mGridView.addHeaderView(headerLayout, null, false);
        mGridView.setOnItemClickListener(this);
        List<PhotoModel> initialList = new ArrayList<PhotoModel>(){};
        initialList.add(new PhotoModel());
        mAdapter = new ResultImageAdapter(ProductActivity.this, initialList, true, false, false);
        mGridView.setAdapter(mAdapter);
        mNumOfSearches++;
        SFApi.getInstance().getSearchService().startSearch(mPhotoModel.getId(), 80, new AjaxCallback() {
            @Override
            public void success(String result) {
                mServerResponseMilliseconds = System.currentTimeMillis();
                final ResultModel resultModel = new Gson().fromJson(result, ResultModel.class);
                if (resultModel == null) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.update(resultModel.getPhotos());
                        updateProductCount(resultModel.getPhotos().size());
                    }
                });
            }

            @Override
            public void timeout(int retry) {
                super.timeout(retry);
                ReportService.getInstance().report(3037, "connection_timeout_event");
            }

            @Override
            public void retry(int retry) {
                super.retry(retry);
                ReportService.getInstance().report(3038, "connection_retry_event");
            }
        });
        renderShareImage(mPhotoModel);
    }

    private void updateProductCount(int size) {
        String productsCountString = "";
        if (size == 1) {
            productsCountString = getString(R.string.product_count, size+"");
        } else {
            productsCountString = getString(R.string.products_count, size+"");
        }
        ArrayAdapter spinnerAdapter = new ArrayAdapter<String>(this, R.layout.sort_by_spinner_item, Arrays.asList(
                getString(R.string.sort_by_similarity) + " " + productsCountString,
                getString(R.string.sort_by_price_low_to_high) + " " + productsCountString,
                getString(R.string.sort_by_price_high_to_low) + " " + productsCountString
        ));
        spinnerAdapter.setDropDownViewResource(R.layout.sort_by_spinner_dropdown_item);
        if (mSortSpinner != null) {
            mSortSpinner.setAdapter(spinnerAdapter);
        }
    }

    protected void searchAgain(String newScope) {
        mNumOfSearches++;
        SFApi.getInstance().getSearchService().setScope(newScope);
        SFApi.getInstance().getSearchService().startSearch(mPhotoModel.getId(), 80, new AjaxCallback() {
            @Override
            public void success(String result) {
                final ResultModel resultModel = new Gson().fromJson(result, ResultModel.class);
                if (resultModel == null) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.update(resultModel.getPhotos());
                        mSortSpinner.setSelection(0);
                        updateProductCount(resultModel.getPhotos().size());
                    }
                });
            }

            @Override
            public void timeout(int retry) {
                super.timeout(retry);
                ReportService.getInstance().report(3037, "connection_timeout_event");
            }

            @Override
            public void retry(int retry) {
                super.retry(retry);
                ReportService.getInstance().report(3038, "connection_retry_event");
            }

            @Override
            public void error(String error, int code) {
                super.error(error, code);
            }
        });
    }

    @Override
    public void onFirstImageLoaded() {
        if (mStartMilliseconds != 0 && mServerResponseMilliseconds != 0 && mNumOfSearches == 1) {
            Map<String, String> extraData = new HashMap<>();
            extraData.put("ServerResponseTime", (mServerResponseMilliseconds - mStartMilliseconds) + "");
            extraData.put("PageLoadingTime", (System.currentTimeMillis() - mStartMilliseconds)+"");
            extraData.put("NetworkType", SFApi.getInstance().getNetworkType(this));
            ReportService.getInstance().report(3036, "pdp_loading_time_event", mSearchId, mPhotoModel.getStatsImageId(), extraData);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TapReason.reportCustomEvent(TapReasonCustomEvent.TapReasonCustomEventType.STORE_ENTERED, this);
        TapReason.reportCustomEvent(TapReasonCustomEvent.TapReasonCustomEventType.STORE_ITEM_VIEW,this);
        ReportService.getInstance().report(3024, "pdp_screen_opened");
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onWatchListItemInserted(PhotoModel photo) {
        showTooltip(R.layout.watchlist_eye_tooltip, findViewById(R.id.action_bar_btn_eye), true);
    }

    @Override
    public void onWatchListItemRemoved(PhotoModel photo) {
        showTooltip(R.layout.watchlist_eye_tooltip_removed, findViewById(R.id.action_bar_btn_eye), true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PhotoModel photo = (PhotoModel)mAdapter.getItem(position);
        if (photo != null) {
            Map<String, String> extraData = new HashMap<String, String>();
            extraData.put("TapPosition", position+"");
            extraData.put("DisplayLocation", "(1," + position +")");
            extraData.put("ImageURL", photo.getUrl());
            ReportService.getInstance().report(3021, "pdp_photo_clicked", mSearchId, photo.getStatsImageId(), extraData);
            Bundle intentBundle = new Bundle();
            intentBundle.putSerializable(Consts.EXTRA_DATA_PHOTO_MODEL_SERIALIZABLE, photo);
            intentBundle.putString(Consts.EXTRA_DATA_SEARCH_ID_STRING, mSearchId);
            navigateToActivity(ProductActivity.class, intentBundle, false, -1);
        }
    }

    protected View getHeaderLayout(StaggeredGridView gridView, final PhotoModel photoModel) {
        View convertView = getLayoutInflater().inflate(R.layout.pdp_header, gridView, false);
        ((TextView)convertView.findViewById(R.id.price)).setText("$"+photoModel.getPrice().intValue());
        ((TextView)convertView.findViewById(R.id.title)).setText(photoModel.getTitle());
        ((TextView)convertView.findViewById(R.id.subtitle)).setText(photoModel.getMerchantOrSource());
        RelativeLayout pdpImageContainer = (RelativeLayout)convertView.findViewById(R.id.pdp_image_container);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        LinearLayout.LayoutParams pdpImageLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Math.round(size.y * 0.5f));
        pdpImageLp.setMargins(0,20,0,20);
        pdpImageContainer.setLayoutParams(pdpImageLp);
        StyleApplication.imageLoader.DisplayImage(photoModel.getThumbUrl(), imageView);
        convertView.findViewById(R.id.share_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share(photoModel);
            }
        });
        convertView.findViewById(R.id.website_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToWebsite(photoModel);
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReportService.getInstance().report(3055, "pdp_image_clicked", mSearchId, photoModel.getStatsImageId(), null);
                goToWebsite(photoModel);
            }
        });
        final ImageButton watchItemBtn = (ImageButton)convertView.findViewById(R.id.watch_btn);
        if (WatchListService.getInstance().isInWatchList(photoModel)) {
            watchItemBtn.setSelected(true);
        } else {
            watchItemBtn.setSelected(false);
        }
        watchItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (WatchListService.getInstance().isInWatchList(photoModel)) {
                    WatchListService.getInstance().remove(photoModel);
                    watchItemBtn.setSelected(false);
                    showTooltip(R.layout.watchlist_eye_tooltip_removed, findViewById(R.id.action_bar_btn_eye), true);
                } else {
                    WatchListService.getInstance().add(photoModel);
                    watchItemBtn.setSelected(true);
                    showTooltip(R.layout.watchlist_eye_tooltip, findViewById(R.id.action_bar_btn_eye), true);
                }
            }
        });
        // render sort spinner
        mSortSpinner = (Spinner)convertView.findViewById(R.id.sort_spinner);
        ArrayAdapter spinnerAdapter = new ArrayAdapter<String>(this, R.layout.sort_by_spinner_item, Arrays.asList(
                getString(R.string.sort_by_similarity),
                getString(R.string.sort_by_price_low_to_high),
                getString(R.string.sort_by_price_high_to_low)
        ));
        spinnerAdapter.setDropDownViewResource(R.layout.sort_by_spinner_dropdown_item);
        mSortSpinner.setAdapter(spinnerAdapter);
        mSortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mAdapter.sortBy(ResultImageAdapter.SORT_BY_SIMILARITY);
                } else if (position == 1) {
                    mAdapter.sortBy(ResultImageAdapter.SORT_BY_PRICE_LOW_TO_HIGH);
                } else if (position == 2) {
                    mAdapter.sortBy(ResultImageAdapter.SORT_BY_PRICE_HIGH_TO_LOW);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // render sizes spinner
        mSizesSpinner = (Spinner)convertView.findViewById(R.id.sizes_spinner);
        ArrayAdapter sizesAdapter = new ArrayAdapter<String>(this, R.layout.sizes_spinner_item, Arrays.asList(
                getString(R.string.sizes_standard),
                getString(R.string.sizes_plus),
                getString(R.string.sizes_maternity)
        ));
        sizesAdapter.setDropDownViewResource(R.layout.sort_by_spinner_dropdown_item);
        mSizesSpinner.setAdapter(sizesAdapter);
        String currentScope = SFApi.getInstance().getSearchService().getScope();
        if (currentScope.equals("style")) {
            mSizesSpinner.setSelection(0);
        } else if (currentScope.equals("xxl")) {
            mSizesSpinner.setSelection(1);
        } else if (currentScope.equals("maternity")) {
            mSizesSpinner.setSelection(2);
        }
        mSizesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String currentScope = SFApi.getInstance().getSearchService().getScope();
                if (position == 0) {
                    if (!currentScope.equals("style")) {
                        ReportService.getInstance().report(3074, "size_standard_tapped");
                        searchAgain("style");
                    }
                } else if (position == 1) {
                    if (!currentScope.equals("xxl")) {
                        ReportService.getInstance().report(3075, "size_plus_tapped");
                        searchAgain("xxl");
                    }
                } else if (position == 2) {
                    if (!currentScope.equals("maternity")) {
                        ReportService.getInstance().report(3076, "size_maternity_tapped");
                        searchAgain("maternity");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return convertView;
    }

    public void renderShareImage(PhotoModel model) {
        mShareImageLayout = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.share_image_layout, null);
        ((TextView) mShareImageLayout.findViewById(R.id.share_image_title)).setText(model.getTitle());
        ((TextView) mShareImageLayout.findViewById(R.id.share_image_source)).setText(model.getMerchantOrSource());
        StyleApplication.imageLoader.DisplayImage(model.getThumbUrl(), ((ImageView) mShareImageLayout.findViewById(R.id.share_image_image)));
    }

    private void share(final PhotoModel photoModel) {
        ReportService.getInstance().report(3026, "share_photo_clicked", mSearchId, photoModel.getStatsImageId(), null);
        final View loadingView = findViewById(R.id.loading_view);
        loadingView.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShareImageLayout.setDrawingCacheEnabled(true);
                mShareImageLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                mShareImageLayout.layout(0, 0, mShareImageLayout.getMeasuredWidth(), mShareImageLayout.getMeasuredHeight());
                mShareImageLayout.buildDrawingCache(true);
                Bitmap shareImageBitmap = Bitmap.createBitmap(mShareImageLayout.getDrawingCache());
                mShareImageLayout.setDrawingCacheEnabled(false);
                String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), shareImageBitmap,"image", null);
                Uri bmpUri = Uri.parse(pathofBmp);
                String shareString = getString(R.string.share_message);
                // create the intent
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                intent.setType("image/png");
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_message_title));
                intent.putExtra(Intent.EXTRA_TEXT, shareString + "\n" + photoModel.getPageUrl());
                startActivity(Intent.createChooser(intent, getString(R.string.how_to_share_message)));
                loadingView.setVisibility(View.GONE);
            }
        },100);
    }

    private void goToWebsite(PhotoModel photoModel) {
        ReportService.getInstance().report(3022, "visit_site_clicked", mSearchId, photoModel.getStatsImageId(), null);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Consts.EXTRA_DATA_PHOTO_MODEL_SERIALIZABLE, photoModel);
        navigateToActivity(WebsiteActivity.class, bundle, false, -1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        TapReason.reportCustomEvent(TapReasonCustomEvent.TapReasonCustomEventType.STORE_EXIT, this);
    }

    @Override
    public void onReachedBottomList() {
        ReportService.getInstance().report(3073, "bottom_PDP_viewed", mSearchId, null, null);
    }

    @Override
    protected String getBarTitle() {
        return getString(R.string.title_product_activity);
    }

    @Override
    public void onAskExpertButtonClicked() {

    }
}
