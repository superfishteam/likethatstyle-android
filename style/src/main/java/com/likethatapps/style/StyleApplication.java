package com.likethatapps.style;

import android.app.Activity;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.likethatapps.services.api.SFApi;
import com.likethatapps.services.api.types.SearchServer;
import com.likethatapps.services.loader.ImageLoader;
import com.likethatapps.style.service.ReportService;
import com.likethatapps.style.service.UserHistoryService;
import com.likethatapps.style.service.WatchListService;
import com.tapreason.sdk.TapReason;
import com.tapreason.sdk.TapReasonCustomEvent;

import java.lang.ref.WeakReference;

import io.fabric.sdk.android.Fabric;

/**
 * Created by eladyarkoni on 6/28/15.
 */
public class StyleApplication extends Application {

    private static Context context;
    private static SharedPreferences settings;
    public static ImageLoader imageLoader;
    private static final long MAX_COUNTER_VALUE = 500;

    public static final String COUNTER_START_APP = "start_app_counter3.0";
    public static final String COUNTER_SEARCH_BY_CAMERA = "counter_search_by_camera";
    private static final String APP_NAME = "LikeThatStyle";

    @Override
    public void onCreate(){
        super.onCreate();
        context = getApplicationContext();
        TapReason.init("FE46DAB0C1D450EDCC21DC42D411288B", "kxrkgnepbsbztrks", new WeakReference<Context>(context), "LikeThat Style");
        TapReason.getConf().setUseHttps(true);
        TapReason.getConf().enableLogging(true);
        TapReason.getConf().setSupportEmail("feedback@likethatapps.com");
        settings = getSharedPreferences("Preferences", 0);
        imageLoader = new ImageLoader(this);
        WatchListService.getInstance().init(context);
        UserHistoryService.getInstance().init(context);
        SFApi.getInstance().init(this, SearchServer.STYLE, APP_NAME);
        ReportService.getInstance().init(context);
        ReportService.getInstance().report(3001, "application_launch");
        FacebookSdk.sdkInitialize(context);
        if (SFApi.getInstance().isRelease(this)) {
            Fabric.with(this, new Crashlytics());
        }
    }

    public static void increaseCounter(String feature) {
        long current = getCounter(feature);
        if (current >= MAX_COUNTER_VALUE) {
            return;
        }
        settings.edit().putLong(feature + "_counter", ++current).commit();
    }

    public static long getCounter(String feature) {
        return settings.getLong(feature + "_counter", 0);
    }

    public static Context getAppContext() {
        return context;
    }

    public static boolean isUserRated() {
        return settings.getBoolean("user_rated", false);
    }

    public static void userRated() {
        settings.edit().putBoolean("user_rated", true).commit();
    }

    public static boolean userDoesntLikeThisApp() {
        return settings.getBoolean("user_doesnt_like_this_app", false);
    }

    public static void setUserDoesntLikeThisApp() {
        settings.edit().putBoolean("user_doesnt_like_this_app", true).commit();
    }

    public static void startRate(Activity activity) {
        StyleApplication.userRated();
        TapReason.reportCustomEvent(TapReasonCustomEvent.TapReasonCustomEventType.REPORT_INTERNAL_RATE_EVENT, activity);
        Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            activity.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
        }
    }

    public static void sendFeedback(Context context) {
        StyleApplication.sendFeedback(context, false);
    }

    public static void sendFeedback(Context context, boolean userDoesntLikeThisApp) {
        if (userDoesntLikeThisApp) {
            StyleApplication.setUserDoesntLikeThisApp();
        }
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "feedback@likethatapps.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.feedback_title));
        context.startActivity(Intent.createChooser(emailIntent, "Send feedback..."));
    }

    public static void sendShareMail(Context context) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.setType("text/html");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.share_subject));
        emailIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.share_body));
        context.startActivity(Intent.createChooser(emailIntent, "Share..."));
    }

    public static void askExpertMail(Context context, String searchId, String searchImageUrl) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "feedback@likethatapps.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.ask_expert_mail_subject));
        emailIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.ask_expert_mail_body, searchImageUrl, searchId));
        context.startActivity(Intent.createChooser(emailIntent, context.getString(R.string.ask_expert_btn)));
    }
}
