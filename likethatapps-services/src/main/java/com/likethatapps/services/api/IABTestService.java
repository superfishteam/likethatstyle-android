package com.likethatapps.services.api;

import com.likethatapps.services.api.model.abtest.UserDataModel;

/**
 * Created by eladyarkoni on 11/12/14.
 */
public interface IABTestService {

    public void updateUserData();
    public UserDataModel getUserData();
}
