package com.tapreason.view.pages;

import java.io.File;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.tapreason.R;
import com.tapreason.sdk.TapReason;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventResult;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventSubResult;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventTypes;
import com.tapreason.sdk.TapReasonConfiguration.TapReasonPageConf;
import com.tapreason.sdk.TapReasonGeneralCons;
import com.tapreason.sdk.TapReasonGeneralCons.TapReasonAppType;
import com.tapreason.view.base.TapReasonActivity;
import com.tapreason.view.util.TapReasonUtils;

final class TapReasonShareAppPage extends TapReasonRulePageBase
{
	private TapReasonAppType appType;
	private String appPackageToUse;
	private String textToShare;
	private String imgPath;
	
	private static final int SHARE_APP_REQUEST_CODE = 1;	
	
	public TapReasonShareAppPage( Bundle params, TapReasonEventTypes eventType )
	{
		super( params, eventType, TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_1 );
	}
	
	@Override
	public boolean parseBundle()
	{
		boolean validData = true;
		
		Bundle params = getParams();
		
		if ( params == null || params.isEmpty() )
		{
			validData = false;			
			Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "ShareAppEvent - Please make sure that params bundle exists in the intent" );				
		}
		else
		{
			// App Package
			if ( params.containsKey( TapReasonGeneralCons.INTENT_APP_PACKAGE_NAME_PARAM ) )
			{
				appPackageToUse = params.getString( TapReasonGeneralCons.INTENT_APP_PACKAGE_NAME_PARAM );
			}
			
			if ( TextUtils.isEmpty( appPackageToUse ) )
			{
				validData = false;
				
				// Now package to trigger
				Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "ShareAppEvent - no app package was provided" );
			}
			
			// App Type
			if ( params.containsKey( TapReasonGeneralCons.INTENT_APP_TYPE_PARAM ) )
			{
				int appTypeId = params.getInt( TapReasonGeneralCons.INTENT_APP_TYPE_PARAM );
				appType = TapReasonAppType.getById( appTypeId );
				
				if ( appType == null )
				{
					// Wrong app type
					Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "ShareAppEvent - Unknown app type with id - " + appType );
					validData = false;
				}			
			}
			else
			{
				validData = false;
			}
			
			// Text to share
			if ( params.containsKey( TapReasonGeneralCons.INTENT_TEXT_TO_SHARE_PARAM ) )
			{
				textToShare = params.getString( TapReasonGeneralCons.INTENT_TEXT_TO_SHARE_PARAM );
			}
			
			// Img path to share
			if ( params.containsKey( TapReasonGeneralCons.INTENT_IMG_TO_SHARE_PARAM ) )
			{
				imgPath = params.getString( TapReasonGeneralCons.INTENT_IMG_TO_SHARE_PARAM );
			}
		}
		
		return validData;
	}

	@Override
	@SuppressLint({ "ResourceAsColor", "CutPasteId" })
	protected void configurePage( TapReasonActivity activity )
	{
		TapReasonPageConf pageConf = TapReason.getConf().getUiConfByEventType( getEventType() );		
		TapReasonStyledPageConfiguration conf = new TapReasonStyledPageConfiguration();
		
		// Default initializations
		// Top Icon						
		conf.setTopIconDrawable( pageConf, getDefaultTopIconByAppType(), activity );
		
		// Default selection color
		conf.setSelectionBGColor( pageConf, R.color.tap_reason_default_selected_color, activity );
								
		// Main title
		conf.setMainTitle( pageConf, getDefaultMainTitle(), activity, true );
		
		// Secondary title
		conf.setSecondaryTitle( pageConf, activity, getDefaultSecondaryTitle() );
		
		// Option 1 Btn		
		conf.setOption1BtnText( pageConf, activity, R.string.tap_reason_share_app_option_btn );
		conf.setOption1BtnTextIsBold( pageConf.isOption1BtnTextIsBold() );
		conf.setOption1BtnDrawable( pageConf, getDefaultTopIconByAppType(), activity );		
		conf.setOption1BtnTextShowIcon( pageConf.isOption1BtnTextShowIcon() );		
		
		// Header BG
		conf.setHeaderBGColor( pageConf, getDefaultHeaderColor(), activity );
		
		// Body BG color
		conf.setBodyBGColor( pageConf, getDefaultBackgroundColor(), activity );		
		
		
		switch ( getStyleTypeId() )
		{
			case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_1:
			{
				// Top Icon
				conf.setShowTopIcon( pageConf.isShowTopIcon() );
				
				// Main title text color
				conf.setMainTitleTextColor( pageConf, R.color.tap_reason_white, activity );						

				// Secondary title text color
				conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_white, activity );
				
				// Option 1 Btn				
				conf.setOption1BtnBGColor( pageConf, R.color.tap_reason_white, activity );
				conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_black, activity );					

				// Option 2 Btn
				conf.setOption2BtnBGColor( pageConf, R.color.tap_reason_white, activity );
				conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_black, activity );
				conf.setOption2BtnText( pageConf, activity, R.string.tap_reason_share_app_leave_feedback );
				conf.setOption2BtnTextIsBold( pageConf.isOption2BtnTextIsBold() );
				conf.setShowOption2Btn( pageConf.isShowOption2Btn() );						
				
				// Option 3 Btn
				conf.setOption3BtnBGColor( pageConf, R.color.tap_reason_white, activity );
				conf.setOption3BtnTextColor( pageConf, R.color.tap_reason_black, activity );	
				conf.setOption3BtnText( pageConf, activity, R.string.tap_reason_share_app_remind_later_option_btn );
				conf.setOption3BtnTextIsBold( pageConf.isOption3BtnTextIsBold() );
				conf.setShowOption3Btn( pageConf.isShowOption3Btn() );																				
				break;
			}
			case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_2:
			{	
				// Header BG
				conf.setHeaderBGColor( pageConf, android.R.color.transparent, activity );
				
				// Top Icon
				conf.setShowTopIcon( true );
				
				// Main title text color
				conf.setMainTitleTextColor( pageConf, R.color.tap_reason_pomegranate_red, activity );					

				// Secondary title text color
				conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_concrete_grey, activity );
				
				// Option 1 Btn				
				conf.setOption1BtnBGColor( pageConf, R.color.tap_reason_nephritis_green, activity );
				conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_3_btn_style2_btn_text_color, activity );				

				// Option 2 Btn
				conf.setOption2BtnBGColor( pageConf, R.color.tap_reason_orange_orange, activity );
				conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_3_btn_style2_btn_text_color, activity );
				conf.setOption2BtnText( pageConf, activity, R.string.tap_reason_share_app_leave_feedback );
				conf.setOption2BtnTextIsBold( pageConf.isOption2BtnTextIsBold() );
				conf.setShowOption2Btn( pageConf.isShowOption2Btn() );					
				
				// Option 3 Btn
				conf.setOption3BtnTextIsBold( pageConf.isOption3BtnTextIsBold() );
				conf.setOption3BtnTextColor( pageConf, R.color.tap_reason_3_btn_style2_btn_text_color, activity );	
				conf.setOption3BtnText( pageConf, activity, R.string.tap_reason_share_app_remind_later_option_btn );
				conf.setOption3BtnBGColor( pageConf, R.color.tap_reason_midnight_blue_grey, activity );
				conf.setShowOption3Btn( pageConf.isShowOption3Btn() );					
				break;
			}			
			case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_1:
			{
				// Top Icon
				conf.setShowTopIcon( pageConf.isShowTopIcon() );
				
				// Main title text color
				conf.setMainTitleTextColor( pageConf, R.color.tap_reason_white, activity );						

				// Secondary title text color
				conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_black, activity );				
		
				// Option 1 Btn				
				conf.setOption1BtnBGColor( pageConf, getDefaultHeaderColor(), activity );
				conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_white, activity );
				conf.setOption1BtnText( pageConf, activity, getDefaultOption1Text() );
				conf.setOption1BtnDrawable( pageConf, R.drawable.tap_reason_23, activity );	

				// Option 2 Btn
				conf.setOption2BtnBGColor( pageConf, android.R.color.transparent, activity );
				conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_black, activity );
				conf.setOption2BtnText( pageConf, activity, getDefaultOption2Text() );
				conf.setOption2BtnTextIsBold( pageConf.isOption2BtnTextIsBold() );	
				conf.setOption2BtnDrawable( pageConf, R.drawable.tap_reason_29, activity );
		
				conf.setOptionsSeparatorText( pageConf, activity, R.string.tap_reason_options_separator_text );
				break;
			}	
			case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_2:
			{
				conf.setTopIconDrawable( pageConf, R.drawable.tap_reason_pink_heart, activity );
				
				conf.setMainTitle( pageConf, getDefaultMainTitle(), activity, false );
				
				// Main title text color
				conf.setMainTitleTextColor( pageConf, R.color.tap_reason_black, activity );								

				// Secondary title text color
				conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_black, activity );				

				// Header BG
				conf.setHeaderBGColor( pageConf, android.R.color.transparent, activity );
				
				// Body BG color
				conf.setBodyBGColor( pageConf, R.color.tap_reason_white, activity );						
				
				// Option 1 Btn				
				conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_midnight_blue_grey, activity );			

				// Option 2 Btn
				conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_midnight_blue_grey, activity );	
				conf.setOption2BtnDrawable( pageConf, R.drawable.tap_reason_29, activity );
				conf.setOption2BtnText( pageConf, activity, R.string.tap_reason_rate_dialog_remind_later_option );					
				break;
			}
		}
		
		configurePage( activity, conf );	
	}

	private int getDefaultBackgroundColor()
	{
		int color = R.color.tap_reason_white;		
		
		switch ( appType )
		{
			case TWITTER:
			{
				color = R.color.tap_reason_twitter_bg;
				break;
			}
			case FACEBOOK:
			{
				color = R.color.tap_reason_facebook_bg;
				break;
			}
			case WHATSAPP:
			{
				color = R.color.tap_reason_whatsapp_bg;
				break;
			}	
			case KAKAO:
			{
				color = R.color.tap_reason_kakao_bg;
				break;
			}
			case VK:
			{
				color = R.color.tap_reason_vk_bg;
				break;
			}
			case WECHAT:
			{
				color = R.color.tap_reason_wechat_bg;
				break;
			}	
			case KIK:
			{
				color = R.color.tap_reason_kik_bg;
				break;
			}		
			case LINE:
			{
				color = R.color.tap_reason_line_bg;
				break;
			}	
			case VIBER:
			{
				color = R.color.tap_reason_viber_bg;
				break;
			}	
			case SMS:
			{
				color = R.color.tap_reason_sms_bg;
				break;
			}	
			case HANGOUTS:
			{
				color = R.color.tap_reason_google_hangouts_bg;
				break;
			}			
			default:
				break;
		}
		
		return color;
	}
	
	private int getDefaultHeaderColor()
	{
		int color = R.color.tap_reason_white;
		
		switch ( appType )
		{
			case TWITTER:
			{
				color = R.color.tap_reason_twitter_title;
				break;
			}
			case FACEBOOK:
			{
				color = R.color.tap_reason_facebook_title;
				break;
			}
			case WHATSAPP:
			{
				color = R.color.tap_reason_whatsapp_title;
				break;
			}	
			case KAKAO:
			{
				color = R.color.tap_reason_kakao_title;
				break;
			}
			case VK:
			{
				color = R.color.tap_reason_vk_title;
				break;
			}
			case WECHAT:
			{
				color = R.color.tap_reason_wechat_title;
				break;
			}
			case KIK:
			{
				color = R.color.tap_reason_kik_title;
				break;
			}
			case LINE:
			{
				color = R.color.tap_reason_line_title;
				break;
			}		
			case VIBER:
			{
				color = R.color.tap_reason_viber_title;
				break;
			}	
			case SMS:
			{
				color = R.color.tap_reason_sms_title;
				break;
			}		
			case HANGOUTS:
			{
				color = R.color.tap_reason_google_hangouts_title;
				break;
			}				
			default:
				break;
		}
		
		return color;
	}	
	
	private int getDefaultMainTitle()
	{
		int defaultMainTitle = R.string.tap_reason_share_app_main_title_text;
		
		return defaultMainTitle;
	}
	
	private int getDefaultSecondaryTitle()
	{
		int text = R.string.tap_reason_share_app_secondary_title_text;
		
		return text;
	}	
	
	private int getDefaultOption1Text()
	{
		int text = R.string.tap_reason_share_app_option_btn;
		
		return text;
	}
	
	private int getDefaultOption2Text()
	{
		int text = R.string.tap_reason_share_app_remind_later_option_btn;
		
		return text;
	}	

	private int getDefaultTopIconByAppType()
	{
		int defaultTopIconResourceId = R.drawable.tap_reason_10;
		
		switch ( appType )
		{
			case FACEBOOK:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_facebook_6;
				break;
			}
			case TWITTER:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_19;				
				break;
			}
			case WHATSAPP:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_whatsapp_3;	
				break;
			}
			case KAKAO:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_kakao_2;	
				break;
			}
			case VK:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_vk_3;	
				break;
			}
			case WECHAT:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_wechat_2;	
				break;
			}	
			case KIK:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_kik_1;	
				break;
			}	
			case LINE:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_line_2;	
				break;
			}
			case VIBER:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_viber_1;	
				break;
			}	
			case SMS:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_sms_1;	
				break;
			}
			case HANGOUTS:
			{
				defaultTopIconResourceId = R.drawable.tap_reason_google_hangouts_1;	
				break;
			}				
			default:
				break;
		}
		return defaultTopIconResourceId;
	}
	
	@Override
	public void onClick(TapReasonActivity activity, View v )
	{
		int id = v.getId();
		switch ( getStyleTypeId() )
		{
			// This is the default case
			case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_1:
			case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_2:				
			{
				if ( id == R.id.tapReasonOption1Btn )
				{
					onShareAppClick( activity );			
				}
				if ( id == R.id.tapReasonOption2Btn )
				{
					onLeaveFeedbackClick( activity );			
				}				
				else if ( id == R.id.tapReasonOption3Btn )
				{
					onRemindMeLaterClick( activity );	
					activity.finish();
				}
				break;
			}
			case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_1:
			case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_2:				
			default:
			{
				if ( id == R.id.tapReasonOption1BtnLayout )
				{
					onShareAppClick( activity );			
				}
				else if ( id == R.id.tapReasonOption2BtnLayout )
				{
					onRemindMeLaterClick( activity );	
					activity.finish();					
				}					
				break;
			}
		}
	}

	private void onShareAppClick(TapReasonActivity activity)
	{
		Intent intent = TapReasonUtils.findIntentForPackage( activity.getPackageManager(), appPackageToUse );
		
		if ( intent == null )
		{
			TapReason.reportEventResult( getEventType(), TapReasonEventResult.ERROR, TapReasonEventSubResult.ERROR_APP_PACKAGE_DOESNT_EXIST );
			
			// Closing the dialog
			activity.finish();
		}
		else
		{				
			intent.putExtra( Intent.EXTRA_TEXT, textToShare );
			
			try
			{
				if ( !TextUtils.isEmpty( imgPath ) && TapReasonUtils.isExternalStorageReadable(activity.getPackageManager(), activity.getPackageName() ) )
				{
					File f = new File(imgPath);
					
					if ( f.exists() )
					{
						intent.putExtra( Intent.EXTRA_STREAM, Uri.parse( imgPath ) );
						
						// Overriding the intent type
						intent.setType("*/*");
					}
				}
			}
			catch ( Exception exception ){}
			
			// Marking the result
			TapReason.reportEventResult( getEventType(), TapReasonEventResult.POSITIVE );
			
			notifyEventResult( getEventType(), TapReasonEventResult.POSITIVE );	
			
			activity.startActivityForResult( intent, SHARE_APP_REQUEST_CODE );
		}
	}

	@Override
	public void onActivityResult(TapReasonActivity activity, int requestCode,int resultCode, Intent data)
	{
		switch ( requestCode )
		{
			case SHARE_APP_REQUEST_CODE:
			{
				if ( resultCode == Activity.RESULT_OK )
				{
					TapReason.reportEventResult( getEventType(), TapReasonEventResult.POSITIVE, TapReasonEventSubResult.TRIGGERED_ACTION_RESULT_OK );
				}
				break;
			}
			default:
				break;
		}
	}
}
