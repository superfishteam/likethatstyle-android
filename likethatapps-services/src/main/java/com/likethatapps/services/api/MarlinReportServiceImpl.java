package com.likethatapps.services.api;

import android.content.Context;
import android.util.Log;

import com.likethatapps.services.api.http.Ajax;

import org.json.JSONObject;

/**
 * Created by eladyarkoni on 5/21/14.
 */
public class MarlinReportServiceImpl extends BaseService implements IReportService {

    private static final String REPORT_ACTION_URL = "http://feedback.marlin.venn.me/petmatch/action";
    private static final String TAG = "report-service";

    public MarlinReportServiceImpl(Context context, String osVersion, String userId, String appName, String appVersion, String deviceModel) {
        super(context, osVersion, appName, appVersion, userId, deviceModel);
    }

    @Override
    public void reportActionEvent(int id, String action, String sessionId, String searchId, String imageId, String category, String extraData) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("action", action);
            if (sessionId != null) {
                obj.put("sessionId", sessionId);
            }
            if (searchId != null) {
                obj.put("searchId", searchId);
            }
            if (imageId != null) {
                obj.put("imageId", imageId);
            }
            if (extraData != null) {
                obj.put("extraData", extraData);
            }
            buildObjectParams(obj);
            Ajax.post(REPORT_ACTION_URL, obj);
            Log.d(TAG, "report action: " + action + " with params: " + obj.toString());
        } catch (Exception ex) {}
    }
}
