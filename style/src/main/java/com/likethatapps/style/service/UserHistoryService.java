package com.likethatapps.style.service;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.likethatapps.style.model.HistoryModel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by eladyarkoni on 9/11/14.
 */
public class UserHistoryService {

    private static final String HISTORY_FILE = "user_style_history";
    private static final String TAG = "user-history-service";
    private static final int MAX_HISTORY_SIZE = 50;

    private static UserHistoryService _instance = new UserHistoryService();

    private List<HistoryModel> history;
    private Context context;

    protected UserHistoryService(){}

    public static UserHistoryService getInstance() {
        return _instance;
    }

    public void init(Context context) {
        history = new ArrayList<HistoryModel>();
        this.context = context;
        load();
    }

    public void add(String imageUrl, String searchId, String results) {
        String searchFileName = "history_search_" + searchId;
        if (saveFile(results, searchFileName)) {
            history.add(0, new HistoryModel(imageUrl, searchId, searchFileName));
        } else {
            return;
        }
        if (history.size() >= MAX_HISTORY_SIZE) {
            history.remove(history.size() - 1);
        }
        save();
    }

    public void update(String searchId, String results) {
        if (history != null && history.size() > 0) {
            for (HistoryModel h : history) {
                if (h.searchId.equals(searchId)) {
                    String searchFileName = "history_search_" + searchId;
                    h.searchResultsFile = searchFileName;
                    saveFile(results, searchFileName);
                    save();
                    return;
                }
            }
        }
    }

    public void remove(String searchId) {
        for (HistoryModel hModel : history) {
            if (searchId.equals(hModel.searchId)) {
                history.remove(hModel);
                save();
                return;
            }
        }
    }

    public String getHistorySearchFile(String searchId) {
        String searchFileName = "history_search_" + searchId;
        ObjectInputStream objectIn = null;
        Object object = null;
        try {
            FileInputStream fileIn = context.getApplicationContext().openFileInput(searchFileName);
            objectIn = new ObjectInputStream(fileIn);
            object = objectIn.readObject();
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            Log.e(TAG, "cant save data " + e.toString());
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "cant save data " + e.toString());
            e.printStackTrace();
            return null;
        } finally {
            if (objectIn != null) {
                try {
                    objectIn.close();
                } catch (IOException e) {
                    Log.e(TAG, "cant save data " + e.toString());
                }
            }
        }
        return (String)object;
    }

    public List<HistoryModel> getHistory() {
        return history;
    }

    private void save() {
        ObjectOutputStream objectOut = null;
        try {
            FileOutputStream fileOut = context.openFileOutput(HISTORY_FILE, Activity.MODE_PRIVATE);
            objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(history);
            fileOut.getFD().sync();
        } catch (IOException e) {
            Log.e(TAG, "cant save data " + e.toString());
            e.printStackTrace();
        } finally {
            if (objectOut != null) {
                try {
                    objectOut.close();
                } catch (IOException e) {
                    Log.e(TAG, "cant save data " + e.toString());
                }
            }
        }
    }

    private boolean saveFile(String data, String filename) {
        ObjectOutputStream objectOut = null;
        try {
            FileOutputStream fileOut = context.openFileOutput(filename, Activity.MODE_PRIVATE);
            objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(data);
            fileOut.getFD().sync();
            return true;
        } catch (IOException e) {
            Log.e(TAG, "cant save data " + e.toString());
            e.printStackTrace();
            return false;
        } finally {
            if (objectOut != null) {
                try {
                    objectOut.close();
                } catch (IOException e) {
                    Log.e(TAG, "cant save data " + e.toString());
                }

            }
        }
    }

    private void load() {
        ObjectInputStream objectIn = null;
        Object object = null;
        try {
            FileInputStream fileIn = context.getApplicationContext().openFileInput(HISTORY_FILE);
            objectIn = new ObjectInputStream(fileIn);
            object = objectIn.readObject();
        } catch (FileNotFoundException e) {
            // do nothing
        } catch (IOException e) {
            Log.e(TAG, "cant save data " + e.toString());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "cant save data " + e.toString());
            e.printStackTrace();
        } finally {
            if (objectIn != null) {
                try {
                    objectIn.close();
                } catch (IOException e) {
                    Log.e(TAG, "cant save data " + e.toString());
                }
            }
        }
        history = (ArrayList<HistoryModel>)object;
        if (history == null) {
            history = new ArrayList<HistoryModel>();
        }
    }

    public void clearHistoryResults() {
        if (history != null && history.size() > 0) {
            for (HistoryModel h : history) {
                removeFile(h.searchResultsFile);
                Log.d(TAG, "history searchAgain file is removed: " + h.searchResultsFile);
            }

        }
    }

    private void removeFile(String filename) {
        if (filename != null) {
            context.deleteFile(filename);
        }
    }

}
