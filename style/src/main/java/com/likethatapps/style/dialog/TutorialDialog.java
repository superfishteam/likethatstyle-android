package com.likethatapps.style.dialog;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.likethatapps.style.R;

/**
 * Created by eladyarkoni on 5/14/14.
 */
public class TutorialDialog extends DialogFragment {

    private static TutorialDialog dialog;
    private TutorialViewPager pager;
    private ImageAdapter imagesAdapter;
    private int[] layoutResources;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tutorial_dialog, container);
        this.imagesAdapter = new ImageAdapter(this.getChildFragmentManager());
        layoutResources = getArguments().getIntArray("layouts");
        this.imagesAdapter.setLayouts(layoutResources);
        pager = (TutorialViewPager)view.findViewById(R.id.pager);
        pager.setAdapter(this.imagesAdapter);
        pager.setOffscreenPageLimit(layoutResources.length);
        pager.setCurrentItem(0);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return view;
    }

    public void onNextBtnClicked(int position) {
        if (position < layoutResources.length - 1) {
            pager.setCurrentItem(position + 1);
        } else {
            ((TutorialDialogCallback)getActivity()).accept(dialog);
        }
    }

    public void onPrevBtnClicked(int position) {
        if (position > 0) {
            pager.setCurrentItem(position - 1);
        }
    }

    public void onSkipBtnClicked() {
        ((TutorialDialogCallback)getActivity()).skip(dialog);
    }

    public static void show(FragmentActivity activity, int[] layoutResource) {
        dialog = new TutorialDialog();
        dialog.setStyle(R.style.DialogTheme, R.style.DialogTheme);
        Bundle args = new Bundle();
        args.putSerializable("layouts", layoutResource);
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), "");
    }

    private class ImageAdapter extends FragmentStatePagerAdapter {

        private ScreenLayoutFragment[] fragments;
        private int currentScreen = -1;

        public ImageAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setLayouts(int[] layoutResources) {
            fragments = new ScreenLayoutFragment[layoutResources.length];
            for (int i = 0; i < layoutResources.length; i++) {
                fragments[i] = new ScreenLayoutFragment();
                fragments[i].setResource(layoutResources[i], i);
            }
        }

        @Override
        public Fragment getItem(int i) {
            return fragments[i];
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            if (currentScreen != position) {
                currentScreen = position;
                ((TutorialDialogCallback)getActivity()).screenView(position, container);
            }
        }

        @Override
        public int getCount() {
            return this.fragments.length;
        }
    }

    public static class ScreenLayoutFragment extends Fragment {

        private int resourceId;
        private int position;

        public void setResource(int resourceId, int position) {
            this.resourceId = resourceId;
            this.position = position;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(resourceId, container, false);
            Button prevButton = (Button)rootView.findViewById(R.id.prev_btn);
            Button nextButton = (Button)rootView.findViewById(R.id.next_btn);
            Button skipButton = (Button)rootView.findViewById(R.id.skip_btn);
            if (nextButton != null) {
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.onNextBtnClicked(position);
                    }
                });
            }
            if (prevButton != null) {
                prevButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.onPrevBtnClicked(position);
                    }
                });
            }
            if (skipButton != null) {
                skipButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.onSkipBtnClicked();
                    }
                });
            }
            return rootView;
        }
    }

    public interface TutorialDialogCallback {
        public void skip(TutorialDialog dialog);
        public void accept(TutorialDialog dialog);
        public void screenView(int screenIndex, ViewGroup contentView);
    }
}
