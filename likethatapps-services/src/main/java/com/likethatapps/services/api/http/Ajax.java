package com.likethatapps.services.api.http;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by eladyarkoni on 5/21/14.
 */
public class Ajax {

    private static final String TAG = "superfish-ajax";
    private static final int TIMEOUT = 5000;
    private static final int RETRY_DELAY = 1000;
    private static final int MAX_RETRIES = 3;
    private static final int ERROR_TIMEOUT = -999;
    private static AQuery aq = null;

    public static void init(Context context) {
        aq = new AQuery(context);
    }

    public static void postImage(String url, String filename, byte[] filedata, final AjaxCallback callback) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(filename, filedata);
        aq.ajax(url, params, String.class, new MyAjaxCallback(callback));
    }

    public static void postFormData(String url, Map<String,Object> params, final AjaxCallback callback) {
        aq.ajax(url, params, String.class, new MyAjaxCallback(callback));
    }

    public static void post(final String urlString, final JSONObject json) {
        aq.post(urlString, json, String.class, new com.androidquery.callback.AjaxCallback<String>());
    }

    public static void get(final String urlString, final AjaxCallback callback) {
        aq.ajax(urlString, String.class, new MyAjaxCallback(callback));
    }

    private static class MyAjaxCallback extends com.androidquery.callback.AjaxCallback<String> {

        private AjaxCallback appCallback;

        public MyAjaxCallback(AjaxCallback appCallback) {
            this.appCallback = appCallback;
            this.retry(MAX_RETRIES);
            this.timeout(TIMEOUT);
        }

        @Override
        public void callback(String url, String object, AjaxStatus status) {
            if (status.getCode() == 200 || status.getCode() == 304) {
                appCallback.success(object);
            } else {
                appCallback.error(status.getError(), status.getCode());
            }
        }
    }
}
