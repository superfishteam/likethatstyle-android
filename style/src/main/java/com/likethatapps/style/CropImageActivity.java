package com.likethatapps.style;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.edmodo.cropper.CropImageView;
import com.google.gson.Gson;
import com.likethatapps.services.api.SFApi;
import com.likethatapps.services.api.http.AjaxCallback;
import com.likethatapps.services.api.types.SearchSource;
import com.likethatapps.services.utils.ImageUtils;
import com.likethatapps.style.dialog.TutorialDialog;
import com.likethatapps.style.model.ResultModel;
import com.likethatapps.style.service.ReportService;
import com.likethatapps.style.service.UserHistoryService;
import com.tapreason.sdk.TapReasonAnnotations;

import java.io.IOException;
import java.util.List;

@TapReasonAnnotations.TapReasonDontInvokeEventsForListener
public class CropImageActivity extends StyleBaseActivity implements TutorialDialog.TutorialDialogCallback{

    private static final String TAG = CropImageActivity.class.getName();
    private Long mSearchStartTime = null;
    private Long mServerResponseTime = null;
    private static final int CROPPED_IMAGE_QUALITY = 90;
    private Bitmap mSearchImageBitmap;
    private List<Point> mCropPathPoints;
    private boolean mAllowSearch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreateBaseActivity(this, savedInstanceState, R.layout.activity_crop_image);
        findViewById(R.id.action_bar_btn_eye).setVisibility(View.GONE);
        findViewById(R.id.action_bar_btn_hamburger).setVisibility(View.GONE);
        final boolean isFirstTime = getIntent().getBooleanExtra(Consts.EXTRA_DATA_FIRST_TIME_BOOLEAN, false);
        final View loadingView = findViewById(R.id.loading_view);
        loadingView.setVisibility(View.VISIBLE);
        new AsyncTask<Void, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(Void... params) {
                return retrieveImageBitmapFromIntent(getIntent());
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                mSearchImageBitmap = bitmap;
                loadingView.setVisibility(View.GONE);
                if (mSearchImageBitmap == null) {
                    finish();
                }
                if (!isFirstTime) {
                    startCropping(isFirstTime, mSearchImageBitmap);
                }
            }
        }.execute();
        if (isFirstTime) {
            findViewById(R.id.try_it_title).setVisibility(View.VISIBLE);
            ReportService.getInstance().report(3011, "FTUE_crop_opened");
            TutorialDialog.show(CropImageActivity.this, new int[]{R.layout.crop_ftue_screen1, R.layout.crop_ftue_screen2});
        }
    }

    @Override
    public void screenView(int screenIndex, ViewGroup contentView) {
        if (screenIndex == 1) {
            ImageView animImageView = (ImageView)contentView.findViewById(R.id.crop_animation_image);
            ((AnimationDrawable) animImageView.getBackground()).start();
        }
    }

    @Override
    public void skip(TutorialDialog dialog) {

    }

    @Override
    public void accept(TutorialDialog dialog) {
        dialog.dismiss();
        startCropping(true, mSearchImageBitmap);
    }

    protected void startCropping(final boolean isFirstTime, final Bitmap image) {
        final Button searchBtn = (Button)findViewById(R.id.start_search_btn);
        final Button undoBtn = (Button)findViewById(R.id.undo_btn);
        final CropImageView cropImageView = (CropImageView)findViewById(R.id.crop_image_view);
        cropImageView.setImageBitmap(image);
        cropImageView.setGuidelines(0);
        searchBtn.setAlpha(0.5f);
        mAllowSearch = false;
        final View cropHint = findViewById(R.id.crop_please_hint);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mAllowSearch) {
                    cropHint.setVisibility(View.VISIBLE);
                    return;
                }
                if (isFirstTime) {
                    ReportService.getInstance().report(3012, "FTUE_crop_clicked");
                }
                cropImageView.getCroppedImage(new CropImageView.GetCroppedImageCallback() {
                    @Override
                    public void onCroppedImage(Bitmap croppedBitmap, int croppedX, int croppedY, int croppedWidth, int croppedHeight) {
                        Bitmap realUserCropImage = Bitmap.createBitmap(croppedBitmap, croppedX, croppedY, croppedWidth, croppedHeight);
                        if (isFirstTime) {
                            Bitmap searchBitmap = ImageUtils.getResourceBitmap(CropImageActivity.this, R.drawable.sample_photo_server);
                            doSearch(searchBitmap, realUserCropImage, 0, 0, searchBitmap.getWidth(), searchBitmap.getHeight());
                        } else {
                            doSearch(croppedBitmap, realUserCropImage, croppedX, croppedY, croppedWidth, croppedHeight);
                        }
                        showScanDialog(true, realUserCropImage);
                    }
                });
            }
        });


        // Loading the path crop canvas
        final ImageView cropImageViewInside = cropImageView.getImageView();
        cropImageViewInside.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                cropImageViewInside.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                undoBtn.setVisibility(View.GONE);
                final ImagePathCropView pathCanvasView = (ImagePathCropView) findViewById(R.id.path_crop_view);
                final Rect bitmapRect = cropImageView.getBitmapRect();
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(bitmapRect.width(), bitmapRect.height());
                lp.setMargins(bitmapRect.left, bitmapRect.top, 0, 0);
                pathCanvasView.setLayoutParams(lp);
                pathCanvasView.setImage(image, bitmapRect.width(), bitmapRect.height());
                pathCanvasView.setImagePathReadyListener(new ImagePathCropView.ImagePathReadyListener() {
                    @Override
                    public void onImagePathReady(int x1, int y1, int x2, int y2, List<Point> path) {
                        cropHint.setVisibility(View.GONE);
                        mCropPathPoints = path;
                        pathCanvasView.setVisibility(View.GONE);
                        cropImageView.setVisibility(View.VISIBLE);
                        cropImageView.setCropWindowPosition(x1, y1, x2, y2);
                        undoBtn.setVisibility(View.VISIBLE);
                        searchBtn.setAlpha(1);
                        mAllowSearch = true;
                    }
                });
                undoBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pathCanvasView.setVisibility(View.VISIBLE);
                        cropImageView.setVisibility(View.GONE);
                        undoBtn.setVisibility(View.GONE);
                        searchBtn.setAlpha(0.5f);
                        mAllowSearch = false;
                    }
                });
                cropImageView.setVisibility(View.GONE);
            }
        });
    }

    protected void showScanDialog(boolean show, Bitmap image) {
        final LinearLayout screen = (LinearLayout)findViewById(R.id.scan_screen);
        if (show) {
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            View scanImage = findViewById(R.id.tutorial_screen_image_scanner);
            screen.setVisibility(View.VISIBLE);
            ImageView imageView = ((ImageView)findViewById(R.id.tutorial_screen_image));
            RelativeLayout imageContainer = ((RelativeLayout)findViewById(R.id.tutorial_screen_image_container));
            float width = image.getWidth();
            float height = image.getHeight();
            float newheight = 0;
            float newwidth = 0;
            if (height > width) {
                newheight = 287;
            } else {
                newheight = 253;
            }
            newwidth = width / (height/newheight);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Math.round(newwidth * metrics.density), Math.round(newheight * metrics.density));
            params.setMargins(0,0,0,35);
            imageContainer.setLayoutParams(params);
            imageView.setImageBitmap(image);
            // activate animation
            TranslateAnimation anim = new TranslateAnimation(Animation.RELATIVE_TO_PARENT,1, Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_PARENT, 0);
            anim.setRepeatCount(10);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setDuration(500);
            scanImage.setAnimation(anim);
            anim.start();
        } else {
            Animation anim = AnimationUtils.loadAnimation(this, R.anim.fast_fade_out_anim);
            screen.startAnimation(anim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    screen.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    protected Bitmap retrieveImageBitmapFromIntent(Intent intent) {
        byte[] imageData = getIntent().getByteArrayExtra(Consts.EXTRA_DATA_IMAGE_BYTE_ARRAY);
        if (imageData != null) {
            return ImageUtils.getBitmap(imageData);
        } else {
            String imageUriStr = getIntent().getStringExtra(Consts.EXTRA_DATA_IMAGE_URI);
            if (imageUriStr != null) {
                int orientation = getIntent().getIntExtra(Consts.EXTRA_DATA_IMAGE_ORIENTATION, 0);
                Uri imageUri = Uri.parse(imageUriStr);
                try {
                    byte[] imageByteArray = ImageUtils.getImageByteArray(this, imageUri);
                    imageByteArray = ImageUtils.decodeAndRotateImageData(imageByteArray, orientation, false);
                    return ImageUtils.getBitmap(imageByteArray);
                } catch (IOException ex) {}
            } else {
                int resourceId = getIntent().getIntExtra(Consts.EXTRA_DATA_IMAGE_RESOURCE_ID, -1);
                if (resourceId != -1) {
                    return ImageUtils.getResourceBitmap(this, resourceId);
                }
            }
        }
        return null;
    }

    @Override
    protected String getBarTitle() {
        return getString(R.string.crop_page_title);
    }

    @Override
    protected boolean hasCameraSearchBtn() {
        return false;
    }

    @Override
    protected boolean showBar() {
        boolean isFirstTime = getIntent().getBooleanExtra(Consts.EXTRA_DATA_FIRST_TIME_BOOLEAN, false);
        return !isFirstTime;
    }

    protected void doSearch(final Bitmap croppedImage, final Bitmap realUserCroppedImage, int croppedX, int croppedY, int croppedWidth, int croppedHeight) {
        mSearchStartTime = System.currentTimeMillis();
        String sourceStr = getIntent().getStringExtra(Consts.EXTRA_DATA_SEARCH_SOURCE);
        SearchSource source = SearchSource.parse(sourceStr);
        // scale searchAgain image
        Bitmap searchImage = ImageUtils.scaleMaxBitmap(croppedImage, 600);
        float scaleRatio = (float)searchImage.getWidth() / (float)croppedImage.getWidth();
        croppedX = Math.round(croppedX * scaleRatio);
        croppedY = Math.round(croppedY * scaleRatio);
        croppedWidth =  Math.round(croppedWidth * scaleRatio);
        croppedHeight =  Math.round(croppedHeight * scaleRatio);
        final SearchSource searchSource = source != null ? source : SearchSource.CAMERA;
        try {
            SFApi.getInstance().getSearchService().startSearchWithCropPath(
                    ImageUtils.getImageByteArray(searchImage, CROPPED_IMAGE_QUALITY),
                    mCropPathPoints,
                    searchSource,
                    100,
                    null,
                    croppedX,
                    croppedY,
                    croppedWidth,
                    croppedHeight,
                    new AjaxCallback() {
                        @Override
                        public void success(String result) {
                            if (result == null) {
                                onSearchError();
                            } else {
                                onSearchSuccess(result, searchSource, realUserCroppedImage);
                            }
                        }

                        @Override
                        public void timeout(int retry) {
                            super.timeout(retry);
                            ReportService.getInstance().report(3037, "connection_timeout_event");
                            onSearchError();
                        }

                        @Override
                        public void retry(int retry) {
                            super.retry(retry);
                            ReportService.getInstance().report(3038, "connection_retry_event");
                            onSearchError();
                        }

                        @Override
                        public void error(String error, int code) {
                            onSearchError();
                        }
                    }
            );
        } catch (IOException ex) {
            finish();
        }
    }

    protected void onSearchSuccess(final String result, final SearchSource searchSource, final Bitmap realUserCroppedImage) {
        mServerResponseTime = System.currentTimeMillis();
        saveSearchToHistory(result);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Bundle bundle = new Bundle();
                byte[] imageByteArray = null;
                try {
                    Bitmap scaledBitmap = ImageUtils.scaleMaxBitmap(realUserCroppedImage, 400);
                    imageByteArray = ImageUtils.getImageByteArray(scaledBitmap, 60);
                    bundle.putByteArray(Consts.EXTRA_DATA_IMAGE_BYTE_ARRAY,imageByteArray);
                } catch (IOException ex) {

                } finally {
                    bundle.putString(Consts.EXTRA_DATA_SEARCH_SOURCE, searchSource.toString());
                    bundle.putString(Consts.EXTRA_DATA_RESULTS_JSON, result);
                    bundle.putBoolean(Consts.EXTRA_DATA_FIRST_TIME_BOOLEAN, getIntent().getBooleanExtra(Consts.EXTRA_DATA_FIRST_TIME_BOOLEAN, false));
                    bundle.putLong(Consts.EXTRA_DATA_SERVER_RESPONSE_TIME_LONG, mServerResponseTime);
                    bundle.putLong(Consts.EXTRA_DATA_SERVER_START_TIME_LONG, mSearchStartTime);
                    navigateToActivity(StyleResultsActivity.class, bundle, true, -1);
                }
            }
        });
    }

    private void saveSearchToHistory(final String result) {
        ResultModel resultModel = new Gson().fromJson(result, ResultModel.class);
        UserHistoryService.getInstance().add(resultModel.getCroppedImageUrl(), resultModel.getSearchId(), result);
    }

    protected void onSearchError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
