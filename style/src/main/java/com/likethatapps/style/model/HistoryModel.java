package com.likethatapps.style.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;

/**
* Created by eladyarkoni on 4/29/15.
*/
public class HistoryModel implements Serializable, Parcelable {

    static final long serialVersionUID = 2L;

    public String imageUrl;
    public Date date;
    public String searchId;
    public String searchResultsFile;

    public HistoryModel() {}

    public HistoryModel(String imageUrl, String searchId, String searchResultsFile) {
        this.imageUrl = imageUrl;
        this.date = new Date();
        this.searchId = searchId;
        this.searchResultsFile = searchResultsFile;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(imageUrl);
        parcel.writeSerializable(date);
        parcel.writeSerializable(searchId);
        parcel.writeSerializable(searchResultsFile);
    }

    public static final Creator<HistoryModel> CREATOR = new Creator<HistoryModel>() {
        @Override
        public HistoryModel createFromParcel(Parcel parcel) {
            HistoryModel historyModel = new HistoryModel();
            historyModel.imageUrl = parcel.readString();
            historyModel.date = (Date)parcel.readSerializable();
            historyModel.searchId = parcel.readString();
            historyModel.searchResultsFile = parcel.readString();
            return historyModel;
        }

        @Override
        public HistoryModel[] newArray(int i) {
            return new HistoryModel[i];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (((HistoryModel)o).imageUrl != null && ((HistoryModel)o).imageUrl.equals(this.imageUrl)) {
            return true;
        }
        return false;
    }

    public static Creator<HistoryModel> getCreator() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
