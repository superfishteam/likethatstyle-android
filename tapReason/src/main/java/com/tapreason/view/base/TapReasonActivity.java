package com.tapreason.view.base;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;

import com.tapreason.R;
import com.tapreason.sdk.TapReason;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventTypes;
import com.tapreason.sdk.TapReasonAnnotations.TapReasonDontCollectAnalyticsOnListener;
import com.tapreason.sdk.TapReasonAnnotations.TapReasonDontInvokeEventsForListener;
import com.tapreason.sdk.TapReasonGeneralCons;
import com.tapreason.view.pages.TapReasonRulePageBase;
import com.tapreason.view.util.TapReasonUtils;

@TapReasonDontInvokeEventsForListener
@TapReasonDontCollectAnalyticsOnListener
public final class TapReasonActivity extends Activity implements OnTouchListener, OnClickListener
{
	private static final String TAP_REASON_ACTIVITY_PLEASE_MAKE_SURE_THAT_PARAMS_BUNDLE_EXISTS_IN_THE_INTENT = "TapReasonActivity - Please make sure that params bundle exists in the intent";

	private boolean validData = false;
	private TapReasonRulePageBase rulePage = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate( savedInstanceState );	
		
		if ( getIntent().hasExtra( TapReasonGeneralCons.INTENT_BUNDLE_PARAM ) )
		{
			Bundle params = getIntent().getBundleExtra( TapReasonGeneralCons.INTENT_BUNDLE_PARAM );
			
			if ( params == null || params.isEmpty() )
			{		
				Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, TAP_REASON_ACTIVITY_PLEASE_MAKE_SURE_THAT_PARAMS_BUNDLE_EXISTS_IN_THE_INTENT );				
			}
			else
			{
				// Event type
				if ( params.containsKey( TapReasonGeneralCons.INTENT_EVENT_TYPE_PARAM ) )
				{
					int eventTypeId = params.getInt( TapReasonGeneralCons.INTENT_EVENT_TYPE_PARAM );
					
					// Parsing the event type to the enum value
					TapReasonEventTypes eventType = TapReasonEventTypes.getById( eventTypeId );					
					rulePage = TapReasonRulePageBase.createPageByEventType( eventType, params );
					
					if ( rulePage == null )
					{
						Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "Can't find page for event type " + eventType.name() + " , unknown event type. Please make sure that you are using the latest version of the SDK" );						
					}
					else
					{
						validData = rulePage.parseBundle();
						if ( validData )
						{
							// We have valid data only in case that the page was created and parsed the bundle parameteres
							rulePage.onCreate( this, this );
						}
						else
						{
							Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "Can't open Activity for event type, the bundle parameters are incomplete. Check the logs for error message." );
						}
					}
				}
			}
		}
		else
		{
			Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, TAP_REASON_ACTIVITY_PLEASE_MAKE_SURE_THAT_PARAMS_BUNDLE_EXISTS_IN_THE_INTENT );
		}
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();		
		TapReason.register( this );		
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		TapReason.unRegister( this );
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		TapReason.clearDrawablesCache();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		if ( !validData )
		{
			return;
		}
		
		rulePage.onResume( this );
	}
	
	@Override
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void setContentView(int layoutResID)
	{
		// Removing the title from the dialog
		// This one must be called before setting the content
		getWindow().requestFeature( Window.FEATURE_NO_TITLE );
		
		super.setContentView( layoutResID );	
		
		// Setting this to remove the first overdraw level from all the views
		// This one is important for the powered by tapreason section, since it has transparent background.
		// In addition, we have ui elements like top icons that need transparent background also
		getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );		
		
		// Setting that the screen can't be closed when pressing outside of the boundaries
		// of the dialog
		if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB )
		{
			this.setFinishOnTouchOutside( false );
		}
		else
		{
			getWindow().getDecorView().setOnTouchListener( this );
		}				
	}	
	
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		if ( !validData )
		{
			return false;
		}
		
        if (event.getAction() == MotionEvent.ACTION_DOWN && TapReasonUtils.isOutOfBounds(event, this, getWindow())) 
        {
            return true;
        }
        
        return false;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ( !validData )
		{
			return super.onKeyDown( keyCode, event );
		}
		
		rulePage.onKeyDown( this, keyCode, event );
		return super.onKeyDown( keyCode, event );
	}

	@Override
	public void onClick(View v)
	{
		if ( !validData )
		{
			finish();
			return;
		}
		
		if ( v.getId() == R.id.tapReasonPoweredByWatermarkImg )
		{
			// In case that the powered by water mark img was clicked, we need to open a browser
			// with the link to tapreason website
			String url = generateUrlForClickOnPoweredByWaterMark();
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);			
		}
		else
		{
			rulePage.onClick( this, v );
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if ( !validData )
		{
			finish();
			return;
		}
		
		finish();
		
		super.onActivityResult( requestCode, resultCode, data );
		
		rulePage.onActivityResult( this, requestCode, resultCode, data );
	}
	
	private String generateUrlForClickOnPoweredByWaterMark()
	{
		String url = String.format( TapReasonGeneralCons.POWERED_BY_WATERMARK_ACTION_URL_FORMAT, "popup", rulePage.getStyleTypeId(), TapReason.getConf().getAppName() );		
		return url;
	}
}
