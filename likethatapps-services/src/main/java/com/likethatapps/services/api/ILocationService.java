package com.likethatapps.services.api;

import android.location.Address;
import android.location.Location;

/**
 * Created by eladyarkoni on 5/14/14.
 */
public interface ILocationService {
    public Location getCurrentLocation();
    public void requestLocation();
    public Address getCurrentAddress();
    public Address getLocationAddress(double lat, double lng);
}
