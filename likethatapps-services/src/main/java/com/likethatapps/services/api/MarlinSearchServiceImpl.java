package com.likethatapps.services.api;

import android.content.Context;
import android.graphics.Point;
import android.location.Location;
import android.util.Log;

import com.likethatapps.services.api.http.Ajax;
import com.likethatapps.services.api.http.AjaxCallback;
import com.likethatapps.services.api.types.SearchSource;

import java.util.List;
import java.util.UUID;

/**
 * Created by eladyarkoni on 5/12/14.
 */
public class MarlinSearchServiceImpl extends BaseService implements ISearchService {

    private String serverDomain = "http://marlin.superfish.com";
    private String byImageData = "/search";
    private String byImageUrl = "/search/by-url/";
    private static final String TAG = "search-service";
    private static final int N_RESULTS = 20;
    private String scope = "pet-finder";

    protected MarlinSearchServiceImpl(Context context, String osVersion, String appName, String appVersion, String userId, String deviceModel) {
        super(context, osVersion, appName, appVersion, userId, deviceModel);
    }

    @Override
    public void startSearchWithCropPath(byte[] data, List<Point> cropPathArray, SearchSource source, int numOfResults, Location location, int cropX, int cropY, int width, int height, final AjaxCallback callback) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void startSearch(String imageId, int numOfResults, AjaxCallback callback) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void startSearch(byte[] data, SearchSource source, int numOfResults, Location location, int cropX, int cropY, int width, int height, AjaxCallback callback) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void startSearch(final byte[] data, final SearchSource source, int numOfResults, final Location location, final AjaxCallback callback) {
        final StringBuilder url = new StringBuilder(buildUrlParams(serverDomain + byImageData));
        url.append("&scope=").append(scope);
        url.append("&n-results=").append(N_RESULTS);
        url.append("&image-origin=").append(source.toString());
        if (location != null) {
            url.append("&latitude=").append(location.getLatitude());
            url.append("&longitude=").append(location.getLongitude());
            url.append("&radius=").append("500");
        }
        Log.d(TAG, "start search with url: " + url);
        Ajax.postImage(url.toString(), "pic.jpg", data, callback);
    }

    @Override
    public void setScope(String scope) {
        this.scope = scope;
    }

    @Override
    public String getScope() {
        return scope;
    }

    @Override
    public void setServerDomain(String serverDomain) {
        this.serverDomain = serverDomain;
    }

    @Override
    public String getServerDomain() {
        return serverDomain;
    }

    public void startSearch(String imageUrl, SearchSource source, String imageTitle, String categoryName, String clusterKeyword, String textSearch, int numOfResults, Location location, final AjaxCallback callback) {
        final StringBuilder url = new StringBuilder(buildUrlParams(serverDomain + byImageUrl));
        url.append("&url=").append(imageUrl);
        url.append("&scope=").append(scope);
        url.append("&n-results=").append(N_RESULTS);
        url.append("&image-origin=").append(source.toString());
        if (location != null) {
            url.append("&latitude=").append(location.getLatitude());
            url.append("&longitude=").append(location.getLongitude());
            url.append("&radius=").append("500");
        }
        Log.d(TAG, "start search with url: " + url);
        Ajax.get(url.toString(), callback);
    }

    private String getSearchId() {
        return userId + "_" + UUID.randomUUID();
    }
}
