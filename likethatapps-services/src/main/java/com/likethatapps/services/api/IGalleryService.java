package com.likethatapps.services.api;

import com.likethatapps.services.api.http.AjaxCallback;

/**
 * Created by eladyarkoni on 7/15/14.
 */
public interface IGalleryService {
    public void getPetMatchGallery(final AjaxCallback callback);
    public void getDecorGallery(final AjaxCallback callback);
}
