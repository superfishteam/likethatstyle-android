package com.likethatapps.style.dialog;

/**
 * Created by eladyarkoni on 5/14/14.
 */

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

public class TutorialViewPager extends ViewPager {

    public TutorialViewPager(Context context) {
        super(context);
    }

    public TutorialViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
