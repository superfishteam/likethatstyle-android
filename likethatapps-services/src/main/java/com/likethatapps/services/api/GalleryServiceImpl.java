package com.likethatapps.services.api;

import android.content.Context;

import com.likethatapps.services.api.http.Ajax;
import com.likethatapps.services.api.http.AjaxCallback;

import java.util.HashMap;

/**
 * Created by eladyarkoni on 7/15/14.
 */
public class GalleryServiceImpl implements IGalleryService {

    private Context context;
    HashMap<String, String> mCache;

    protected GalleryServiceImpl(Context context) {
        this.context = context;
        mCache = new HashMap<String, String>();
    }



    @Override
    public void getDecorGallery(final AjaxCallback callback) {
        if (mCache.get("decor") != null) {
            callback.success(mCache.get("decor"));
        } else {
            Ajax.get("http://www.superfish.com/apps/galleries/prod/likethat_decor_ios/gallery.json", new AjaxCallback() {
                @Override
                public void success(String result) {
                    mCache.put("decor", result);
                    callback.success(mCache.get("decor"));
                }

                @Override
                public void error(String error, int code) {
                    callback.error(error, code);
                }
            });
        }
    }

    @Override
    public void getPetMatchGallery(final AjaxCallback callback) {
        if (mCache.get("petmatch") != null) {
            callback.success(mCache.get("petmatch"));
        } else {
            Ajax.get("http://www.superfish.com/apps/galleries/prod/petmatch/gallery.json", new AjaxCallback() {
                @Override
                public void success(String result) {
                    mCache.put("petmatch", result);
                    callback.success(mCache.get("petmatch"));
                }

                @Override
                public void error(String error, int code) {
                    callback.error(error, code);
                }
            });
        }
    }
}
