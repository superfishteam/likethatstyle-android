package com.likethatapps.services.api.model.abtest;

/**
 * Created by eladyarkoni on 11/12/14.
 */
public class TierModel {

    private int previousGroup;
    private int group;
    private int nextGroup;
    private String bucket;

    public int getPreviousGroup() {
        return previousGroup;
    }

    public int getGroup() {
        return group;
    }

    public int getNextGroup() {
        return nextGroup;
    }

    public String getBucket() {
        return bucket;
    }
}
