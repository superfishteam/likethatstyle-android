package com.likethatapps.style;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.applinks.AppLinkData;
import com.likethatapps.services.api.SFApi;
import com.likethatapps.services.api.types.SearchSource;
import com.likethatapps.style.service.ReportService;
import com.tapreason.sdk.TapReasonAnnotations;


@TapReasonAnnotations.TapReasonDontInvokeEventsForListener
public class StartActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (StyleApplication.getCounter(StyleApplication.COUNTER_START_APP) == 0) {
            startCropFtue();
        } else {
            startHomeActivity(2000);
        }
        StyleApplication.increaseCounter(StyleApplication.COUNTER_START_APP);
        ReportService.getInstance().report(3004, "splash_screen_opened");

        // Facebook Ads Link
        AppLinkData.fetchDeferredAppLinkData(this,
            new AppLinkData.CompletionHandler() {
                @Override
                public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                    if (appLinkData == null || appLinkData.getArgumentBundle() == null) {
                        return;
                    }
                    Uri uri = appLinkData.getTargetUri();
                    String searchScope = null;
                    if (uri != null) {
                        searchScope = uri.getQueryParameter("SearchScope");
                    }
                    Log.d("facebook-ads-link", "SearchScope: " + searchScope);
                    if (searchScope == null || searchScope.length() <= 1) {
                        return;
                    }
                    switch (searchScope) {
                        case "style":
                            SFApi.getInstance().getSearchService().setScope("style");
                            break;
                        case "xxl":
                            SFApi.getInstance().getSearchService().setScope("xxl");
                            break;
                        case "maternity":
                            SFApi.getInstance().getSearchService().setScope("maternity");
                            break;
                    }
                }
            }
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this, getString(R.string.facebook_id));
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this, getString(R.string.facebook_id));
    }

    protected void startHomeActivity(long milli) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(StartActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }, milli);
    }

    protected void startCropFtue() {
        Intent intent = new Intent(StartActivity.this, CropImageActivity.class);
        intent.putExtra(Consts.EXTRA_DATA_SEARCH_SOURCE, SearchSource.FTUE.toString());
        intent.putExtra(Consts.EXTRA_DATA_FIRST_TIME_BOOLEAN, true);
        intent.putExtra(Consts.EXTRA_DATA_IMAGE_RESOURCE_ID, R.drawable.sample_photo);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
