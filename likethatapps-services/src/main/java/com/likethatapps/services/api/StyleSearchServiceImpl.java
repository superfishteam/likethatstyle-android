package com.likethatapps.services.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.likethatapps.services.api.http.Ajax;
import com.likethatapps.services.api.http.AjaxCallback;
import com.likethatapps.services.api.model.abtest.UserDataModel;
import com.likethatapps.services.api.types.SearchSource;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by eladyarkoni on 5/12/14.
 */
public class StyleSearchServiceImpl extends BaseService implements ISearchService {

    private String serverDomain = "http://style.likethatapps.com";
    private String byImageData = "/search";
    private String byImageUrl = "/search/by-url/";
    private String byImageId = "/search/by-id/";
    private String TAG = "search-service";
    private String scope = "style";
    private static SharedPreferences settings;

    protected StyleSearchServiceImpl(Context context, String osVersion, String appName, String appVersion, String userId, String deviceModel) {
        super(context, osVersion, appName, appVersion, userId, deviceModel);
        settings = context.getSharedPreferences("SearchPreferences", 0);
        String savedScope = settings.getString("scope", null);
        if (savedScope != null) {
            this.scope = savedScope;
        }
    }

    @Override
    public void startSearchWithCropPath(byte[] data, List<Point> cropPathArray, SearchSource source, int numOfResults, Location location, int cropX, int cropY, int width, int height, final AjaxCallback callback) {
        final StringBuilder url = new StringBuilder(buildUrlParams(serverDomain + byImageData));
        url.append("&scope=").append(scope);
        url.append("&n-results=").append(numOfResults);
        url.append("&image-origin=").append(source.toString());
        url.append("&crop-x=").append(cropX);
        url.append("&crop-y=").append(cropY);
        url.append("&crop-width=").append(width);
        url.append("&crop-height=").append(height);
        if (location != null) {
            url.append("&latitude=").append(location.getLatitude());
            url.append("&longitude=").append(location.getLongitude());
            url.append("&radius=").append("500");
        }
        if (SFApi.getInstance().getAbTestService() != null) {
            UserDataModel userData = SFApi.getInstance().getAbTestService().getUserData();
            if (userData != null) {
                try {
                    url.append("&tier1_bucket=").append(userData.getTier1().getBucket());
                    url.append("&tier2_bucket=").append(userData.getTier2().getBucket());
                    url.append("&tier1_curr_group=").append(userData.getTier1().getGroup());
                    url.append("&tier2_curr_group=").append(userData.getTier2().getGroup());
                    url.append("&tier1_prev_group=").append(userData.getTier1().getPreviousGroup());
                    url.append("&tier2_prev_group=").append(userData.getTier2().getPreviousGroup());
                    url.append("&tier1_next_group=").append(userData.getTier1().getNextGroup());
                    url.append("&tier2_next_group=").append(userData.getTier2().getNextGroup());
                } catch (NullPointerException ex){
                    Log.e(TAG, "abtest data is corrupted");
                }
            }
        }
        Log.d(TAG, "start search with url: " + url);
        String cropPathArrayString = new Gson().toJson(cropPathArray);
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("pic.jpg", data);
        params.put("facedetection.json", "");
        params.put("croppath.json", cropPathArrayString);
        Ajax.postFormData(url.toString(), params, callback);
    }

    @Override
    public void startSearch(String imageId, int numOfResults, AjaxCallback callback) {
        final StringBuilder url = new StringBuilder(buildUrlParams(serverDomain + byImageId));
        url.append("&id=").append(imageId);
        url.append("&scope=").append(scope);
        url.append("&n-results=").append(numOfResults);
        url.append("&image-origin=").append(SearchSource.FEED.toString());
        if (SFApi.getInstance().getAbTestService() != null) {
            UserDataModel userData = SFApi.getInstance().getAbTestService().getUserData();
            if (userData != null) {
                try {
                    url.append("&tier1_bucket=").append(userData.getTier1().getBucket());
                    url.append("&tier2_bucket=").append(userData.getTier2().getBucket());
                    url.append("&tier1_curr_group=").append(userData.getTier1().getGroup());
                    url.append("&tier2_curr_group=").append(userData.getTier2().getGroup());
                    url.append("&tier1_prev_group=").append(userData.getTier1().getPreviousGroup());
                    url.append("&tier2_prev_group=").append(userData.getTier2().getPreviousGroup());
                    url.append("&tier1_next_group=").append(userData.getTier1().getNextGroup());
                    url.append("&tier2_next_group=").append(userData.getTier2().getNextGroup());
                } catch (NullPointerException ex){
                    Log.e(TAG, "abtest data is corrupted");
                }
            }
        }
        Log.d(TAG, "start search by url: " + url.toString());
        Ajax.get(url.toString(), callback);
    }

    @Override
    public void startSearch(byte[] data, SearchSource source, int numOfResults, Location location, int cropX, int cropY, int width, int height, AjaxCallback callback) {
        final StringBuilder url = new StringBuilder(buildUrlParams(serverDomain + byImageData));
        url.append("&scope=").append(scope);
        url.append("&n-results=").append(numOfResults);
        url.append("&image-origin=").append(source.toString());
        url.append("&crop-x=").append(cropX);
        url.append("&crop-y=").append(cropY);
        url.append("&crop-width=").append(width);
        url.append("&crop-height=").append(height);
        if (location != null) {
            url.append("&latitude=").append(location.getLatitude());
            url.append("&longitude=").append(location.getLongitude());
            url.append("&radius=").append("500");
        }
        if (SFApi.getInstance().getAbTestService() != null) {
            UserDataModel userData = SFApi.getInstance().getAbTestService().getUserData();
            if (userData != null) {
                try {
                    url.append("&tier1_bucket=").append(userData.getTier1().getBucket());
                    url.append("&tier2_bucket=").append(userData.getTier2().getBucket());
                    url.append("&tier1_curr_group=").append(userData.getTier1().getGroup());
                    url.append("&tier2_curr_group=").append(userData.getTier2().getGroup());
                    url.append("&tier1_prev_group=").append(userData.getTier1().getPreviousGroup());
                    url.append("&tier2_prev_group=").append(userData.getTier2().getPreviousGroup());
                    url.append("&tier1_next_group=").append(userData.getTier1().getNextGroup());
                    url.append("&tier2_next_group=").append(userData.getTier2().getNextGroup());
                } catch (NullPointerException ex){
                    Log.e(TAG, "abtest data is corrupted");
                }
            }
        }
        Log.d(TAG, "start search with url: " + url);
        Ajax.postImage(url.toString(), "pic.jpg", data, callback);
    }

    @Override
    public void startSearch(final byte[] data, final SearchSource source, int numOfResults, final Location location, final AjaxCallback callback) {
        final StringBuilder url = new StringBuilder(buildUrlParams(serverDomain + byImageData));
        url.append("&scope=").append(scope);
        url.append("&n-results=").append(numOfResults);
        url.append("&image-origin=").append(source.toString());
        if (location != null) {
            url.append("&latitude=").append(location.getLatitude());
            url.append("&longitude=").append(location.getLongitude());
            url.append("&radius=").append("500");
        }
        if (SFApi.getInstance().getAbTestService() != null) {
            UserDataModel userData = SFApi.getInstance().getAbTestService().getUserData();
            if (userData != null) {
                try {
                    url.append("&tier1_bucket=").append(userData.getTier1().getBucket());
                    url.append("&tier2_bucket=").append(userData.getTier2().getBucket());
                    url.append("&tier1_curr_group=").append(userData.getTier1().getGroup());
                    url.append("&tier2_curr_group=").append(userData.getTier2().getGroup());
                    url.append("&tier1_prev_group=").append(userData.getTier1().getPreviousGroup());
                    url.append("&tier2_prev_group=").append(userData.getTier2().getPreviousGroup());
                    url.append("&tier1_next_group=").append(userData.getTier1().getNextGroup());
                    url.append("&tier2_next_group=").append(userData.getTier2().getNextGroup());
                } catch (NullPointerException ex){
                    Log.e(TAG, "abtest data is corrupted");
                }
            }
        }
        Log.d(TAG, "start search with url: " + url);
        Ajax.postImage(url.toString(), "pic.jpg", data, callback);
    }

    @Override
    public void setScope(String scope) {
        this.scope = scope;
        this.settings.edit().putString("scope", scope).commit();
    }

    @Override
    public String getScope() {
        return scope;
    }

    @Override
    public void setServerDomain(String serverDomain) {
        this.serverDomain = serverDomain;
    }

    @Override
    public String getServerDomain() {
        return serverDomain;
    }

    public void startSearch(String imageUrl, SearchSource source, String imageTitle, String categoryName, String clusterKeyword, String textSearch, int numOfResults, Location location, final AjaxCallback callback) {
        final StringBuilder url = new StringBuilder(buildUrlParams(serverDomain + byImageUrl));
        Uri.Builder uriBuilder = Uri.parse(url.toString()).buildUpon();
        uriBuilder.appendQueryParameter("url", imageUrl);
        uriBuilder.appendQueryParameter("scope", scope);
        uriBuilder.appendQueryParameter("n-results", numOfResults+"");
        uriBuilder.appendQueryParameter("image-origin", source.toString());
        if (imageTitle != null) {
            uriBuilder.appendQueryParameter("image-title", imageTitle);
        }
        if (textSearch != null) {
            uriBuilder.appendQueryParameter("text-search-query", textSearch);
        }
        if (categoryName != null) {
            uriBuilder.appendQueryParameter("category-name", categoryName);
        }
        if (clusterKeyword != null) {
            uriBuilder.appendQueryParameter("cluster-keyword", clusterKeyword);
        }
        if (location != null) {
            uriBuilder.appendQueryParameter("latitude", location.getLatitude() + "");
            uriBuilder.appendQueryParameter("longitude", location.getLongitude() + "");
            uriBuilder.appendQueryParameter("radius", "500");
        }
        if (SFApi.getInstance().getAbTestService() != null) {
            UserDataModel userData = SFApi.getInstance().getAbTestService().getUserData();
            if (userData != null) {
                try {
                    uriBuilder.appendQueryParameter("tier1_bucket", userData.getTier1().getBucket());
                    uriBuilder.appendQueryParameter("tier2_bucket", userData.getTier2().getBucket());
                    uriBuilder.appendQueryParameter("tier1_curr_group", userData.getTier1().getGroup() + "");
                    uriBuilder.appendQueryParameter("tier2_curr_group", userData.getTier2().getGroup() + "");
                    uriBuilder.appendQueryParameter("tier1_prev_group", userData.getTier1().getPreviousGroup() + "");
                    uriBuilder.appendQueryParameter("tier2_prev_group", userData.getTier2().getPreviousGroup() + "");
                    uriBuilder.appendQueryParameter("tier1_next_group", userData.getTier1().getNextGroup() + "");
                    uriBuilder.appendQueryParameter("tier2_next_group", userData.getTier2().getNextGroup() + "");
                } catch (NullPointerException ex){
                    Log.e(TAG, "abtest data is corrupted");
                }
            }
        }
        Log.d(TAG, "start search with url: " + uriBuilder.toString());
        Ajax.get(uriBuilder.toString(), callback);
    }

    private String getSearchId() {
        return userId + "_" + UUID.randomUUID();
    }
}
