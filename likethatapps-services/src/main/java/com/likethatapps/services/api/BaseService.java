package com.likethatapps.services.api;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by eladyarkoni on 6/3/14.
 */
public class BaseService {

    protected Context context;
    protected String osVersion;
    protected String appName;
    protected String appVersion;
    protected String userId;
    protected String deviceModel;

    public BaseService(Context context, String osVersion, String appName, String appVersion, String userId, String deviceModel) {
        this.context = context;
        this.appName = appName;
        this.appVersion = appVersion;
        this.userId = userId;
        this.osVersion = osVersion;
        this.deviceModel = deviceModel;
    }

    protected String buildUrlParams(String url) {
        StringBuilder ub = new StringBuilder(url);
        ub.append("?user-id=").append(userId);
        ub.append("&app-name=").append(appName);
        ub.append("&app-version=").append(appVersion);
        ub.append("&device-model=").append(deviceModel);
        ub.append("&os=").append("Android").append("_").append(osVersion);
        return ub.toString();
    }

    protected void buildObjectParams(JSONObject obj) {
        try {
            obj.put("userId", userId);
            obj.put("appName", appName);
            obj.put("appVersion", appVersion);
            obj.put("deviceModel", deviceModel);
            obj.put("os", "Android_" + osVersion);
        } catch (Exception ex) {}
    }
}
