package com.likethatapps.style.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.likethatapps.services.api.SFApi;
import com.likethatapps.style.StyleApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.UUID;

/**
 * Created by eladyarkoni on 9/14/14.
 */
public class ReportService {

    private static ReportService ourInstance = new ReportService();
    private String deviceModel;
    private String appName;
    private String osVersion;
    private String userId;
    private String appVersion;
    private static final int MAX_SESSION_MILLISECONDS = 1800000;
    private static final String TAG = "report-service";
    private static SharedPreferences settings;

    public static ReportService getInstance() {
        return ourInstance;
    }

    private ReportService() {

    }

    public void report(int id, String action) {
        report(id, action, null, null, null);
    }

    public void report(final int id, final String action, final String searchId, final String imageId, final Map extraData) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                long lastReportTimestamp = settings.getLong("last_report_timestamp", 0);
                long currentTimestamp = System.currentTimeMillis();
                if ((currentTimestamp - lastReportTimestamp) > MAX_SESSION_MILLISECONDS) {
                    settings.edit().putString("session_id",generateSessionID()).commit();
                }
                settings.edit().putLong("last_report_timestamp",currentTimestamp).commit();
                String currentSessionId = settings.getString("session_id","none");
                // report to appstats db
                JSONObject extraDataJson = null;
                if (extraData != null) {
                    extraDataJson = new JSONObject(extraData);
                } else {
                    extraDataJson = new JSONObject();
                }
                try {
                    extraDataJson.put("DeviceModel", deviceModel);
                } catch (JSONException ex){}
                Log.d(TAG, extraDataJson.toString());
                SFApi.getInstance().getReportService().reportActionEvent(id, action, currentSessionId, searchId, imageId, null, extraDataJson.toString());
                return null;
            }
        }.execute();
    }

    public void init(Context context) {
        settings = StyleApplication.getAppContext().getSharedPreferences("Preferences", 0);
        appName = SFApi.getInstance().getAppName(context);
        appVersion = SFApi.getInstance().getAppVersion(context);
        osVersion = SFApi.getInstance().getOSVersion(context);
        userId = SFApi.getInstance().getUserId(context);
        deviceModel = SFApi.getInstance().getDeviceModel(context);
    }

    private String generateSessionID() {
        return UUID.randomUUID().toString();
    }
}
