package com.likethatapps.style.model;

import java.util.List;

/**
 * Created by eladyarkoni on 9/8/14.
 */
public class ClusterModel {

    private int index;
    private String score;
    private List<String> keywords;
    private List<PhotoModel> photos;

    public int getIndex() {
        return index;
    }

    public String getScore() {
        return score;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public List<PhotoModel> getPhotos() {
        return photos;
    }
}
