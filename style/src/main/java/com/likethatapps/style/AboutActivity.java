package com.likethatapps.style;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.likethatapps.services.utils.ImageUtils;

/**
 * Created by eladyarkoni on 7/7/15.
 */
public class AboutActivity extends StyleBaseActivity{
    private static final float IMAGE_WIDTH = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreateBaseActivity(this, savedInstanceState, R.layout.activity_about);
        RelativeLayout container = (RelativeLayout)findViewById(R.id.image_container);
        Bitmap aboutBitmap = ImageUtils.getResourceBitmap(this, R.drawable.about);
        // scaling down image
        new AsyncTask<Bitmap, Bitmap, Bitmap>(){
            @Override
            protected Bitmap doInBackground(Bitmap... bitmaps) {
                float height = bitmaps[0].getHeight() * (IMAGE_WIDTH/bitmaps[0].getWidth());
                return ImageUtils.scaleBitmap(bitmaps[0], Math.round(IMAGE_WIDTH), Math.round(height));
            }

            @Override
            protected void onPostExecute(Bitmap image) {
                ((ImageView)findViewById(R.id.image_view)).setImageBitmap(image);
            }
        }.execute(aboutBitmap);
    }

    @Override
    protected boolean hasCameraSearchBtn() {
        return false;
    }

    @Override
    protected String getBarTitle() {
        return getString(R.string.sidemenu_about);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
