package com.likethatapps.style.service;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.likethatapps.style.R;
import com.likethatapps.style.model.PhotoModel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eladyarkoni on 9/11/14.
 */
public class WatchListService {

    private static final String FAVORITE_FILE = "user_watchlist";
    private static final String TAG = "user-watchlist-service";

    private static WatchListService _instance = new WatchListService();

    private List<PhotoModel> watchlist;
    private Context context;

    protected WatchListService(){}

    public static WatchListService getInstance() {
        return _instance;
    }

    public void init(Context context) {
        watchlist = new ArrayList<PhotoModel>();
        this.context = context;
        load();
    }

    public void add(PhotoModel photoModel) {
        watchlist.add(photoModel);
        ReportService.getInstance().report(3041, "watchlist_item_added");
        save();
    }

    public void remove(PhotoModel photoModel) {
        for (PhotoModel pModel : watchlist) {
            if (pModel.getId().equals(photoModel.getId())) {
                watchlist.remove(pModel);
                ReportService.getInstance().report(3042, "watchlist_item_removed");
                save();
                return;
            }
        }
    }

    public boolean isInWatchList(PhotoModel photoModel) {
        for (PhotoModel pModel : watchlist) {
            if (pModel.getId().equals(photoModel.getId())) {
                return true;
            }
        }
        return false;
    }

    public List<PhotoModel> getWatchList() {
        return watchlist;
    }

    private void save() {
        ObjectOutputStream objectOut = null;
        try {
            FileOutputStream fileOut = context.openFileOutput(FAVORITE_FILE, Activity.MODE_PRIVATE);
            objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(watchlist);
            fileOut.getFD().sync();

        } catch (IOException e) {
            Log.e(TAG, "cant save data " + e.toString());
            e.printStackTrace();
        } finally {
            if (objectOut != null) {
                try {
                    objectOut.close();
                } catch (IOException e) {
                    Log.e(TAG, "cant save data " + e.toString());
                }

            }
        }
    }

    private void load() {
        ObjectInputStream objectIn = null;
        Object object = null;
        try {
            FileInputStream fileIn = context.getApplicationContext().openFileInput(FAVORITE_FILE);
            objectIn = new ObjectInputStream(fileIn);
            object = objectIn.readObject();
        } catch (FileNotFoundException e) {
            // do nothing
        } catch (IOException e) {
            Log.e(TAG, "cant save data " + e.toString());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "cant save data " + e.toString());
            e.printStackTrace();
        } finally {
            if (objectIn != null) {
                try {
                    objectIn.close();
                } catch (IOException e) {
                    Log.e(TAG, "cant save data " + e.toString());
                }
            }
        }

        watchlist = (ArrayList<PhotoModel>)object;
        if (watchlist == null) {
            watchlist = new ArrayList<PhotoModel>();
        }
    }
}
