package com.tapreason.view.pages;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;

import com.tapreason.sdk.TapReason;
import com.tapreason.sdk.TapReasonConfiguration.TapReasonPageConf;

final class TapReasonStyledPageConfiguration
{
	// Initializing to defaults
	private boolean showTopIcon = true;
	private int topIconResourceId;
	private String topIconUrl;	
	private String mainTitle;
	private int mainTitleTextColor = 0;
	private String secondaryTitle;		
	private int secondaryTitleTextColor = 0;
	private int headerBGColor = 0;
	private int selectionBGColor = 0;
	private int bodyBGColor = 0;
	private String optionsSeparatorText;
	private int defaultTextColor = 0;
	
	private String option1BtnText;
	private boolean option1BtnTextIsBold = true;
	private boolean option1BtnTextShowIcon = true;
	private int option1BtnTextColor = 0;
	private int option1BtnBGColor = 0;
	private int option1BtnResourceId;
	private String option1BtnUrl;	
	
	private String option2BtnText;
	private boolean option2BtnTextIsBold = false;
	private boolean showOption2Btn = true;
	private int option2BtnTextColor = 0;
	private int option2BtnBGColor = 0;	
	private int option2BtnResourceId;	
	private String option2BtnUrl;	
	
	private String option3BtnText;
	private boolean option3BtnTextIsBold = false;
	private boolean showOption3Btn = true;
	private int option3BtnTextColor = 0;
	private int option3BtnBGColor = 0;
	
	boolean isShowTopIcon()
	{
		return showTopIcon;
	}	
	void setShowTopIcon(boolean showTopIcon)
	{
		this.showTopIcon = showTopIcon;
	}

	int getTopIconResourceId()
	{
		return topIconResourceId;
	}
	
	String getTopIconUrl()
	{
		return topIconUrl;
	}
	
	void setTopIconDrawable( TapReasonPageConf pageConf, int defaultDrawableResource, Context context )
	{
		if ( !TextUtils.isEmpty( pageConf.getTopIconDrawableUrl() ) )
		{
			this.topIconUrl =  pageConf.getTopIconDrawableUrl();
		}
		else
		{
			this.topIconUrl = null;
		}
		
		this.topIconResourceId = defaultDrawableResource;
	}
	
	String getMainTitle()
	{
		return mainTitle;
	}
	
	@SuppressLint("DefaultLocale")
	void setMainTitle(TapReasonPageConf pageConf, int defaultResourceId, Context context, boolean isUpperCase )
	{
		if ( TextUtils.isEmpty( pageConf.getMainTitle() ) )
		{
			this.mainTitle = context.getString( defaultResourceId, TapReason.getConf().getAppName() );
		}
		else
		{
			this.mainTitle = pageConf.getMainTitle();
		}
		
		if ( isUpperCase )
		{
			this.mainTitle = this.mainTitle.toUpperCase();
		}
	}
	
	int getMainTitleTextColor()
	{
		return mainTitleTextColor;
	}
	
	void setMainTitleTextColor( TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getMainTitleTextColor() == 0 )
		{
			this.mainTitleTextColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.mainTitleTextColor = pageConf.getMainTitleTextColor();
		}
	}
	
	String getSecondaryTitle()
	{
		return secondaryTitle;
	}
	
	void setSecondaryTitle( TapReasonPageConf pageConf, Context context, int defaultStringResourceId )
	{
		if ( TextUtils.isEmpty( pageConf.getSecondaryTitle() ) )
		{
			this.secondaryTitle = context.getString( defaultStringResourceId, TapReason.getConf().getAppName() );
		}
		else
		{
			this.secondaryTitle = pageConf.getSecondaryTitle();
		}
	}
	
	int getSecondaryTitleTextColor()
	{
		return secondaryTitleTextColor;
	}
	
	void setSecondaryTitleTextColor( TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getSecondaryTitleTextColor() == 0 )
		{
			this.secondaryTitleTextColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.secondaryTitleTextColor = pageConf.getSecondaryTitleTextColor();
		}
	}

	int getHeaderBGColor()
	{
		return headerBGColor;
	}
	void setHeaderBGColor(TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getHeaderBGColor() == 0 )
		{
			this.headerBGColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.headerBGColor = pageConf.getHeaderBGColor();
		}
	}
	int getSelectionBGColor()
	{
		return selectionBGColor;
	}
	
	void setSelectionBGColor( TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getSelectionBGColor() == 0 )
		{
			this.selectionBGColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.selectionBGColor = pageConf.getSelectionBGColor();
		}
	}
	
	int getBodyBGColor()
	{
		return bodyBGColor;
	}
	void setBodyBGColor(TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getBodyBGColor() == 0 )
		{
			this.bodyBGColor = context.getResources().getColor( defaultColorResourceId ); 
		}
		else
		{
			this.bodyBGColor = pageConf.getBodyBGColor();
		}		
	}
	
	String getOptionsSeparatorText()
	{
		return optionsSeparatorText;
	}

	void setOptionsSeparatorText( TapReasonPageConf pageConf, Context context, int defaultStringResourceId )
	{
		if ( TextUtils.isEmpty( pageConf.getOptionsSeparatorText() ) )
		{
			this.optionsSeparatorText = context.getString( defaultStringResourceId );
		}
		else
		{
			this.optionsSeparatorText = pageConf.getOptionsSeparatorText();
		}
	}
	
	int getDefaultTextColor()
	{
		return defaultTextColor;
	}
	
	void setDefaultTextColor( TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getDefaultTextColor() == 0 )
		{
			this.defaultTextColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.defaultTextColor = pageConf.getDefaultTextColor();
		}
	}

	String getOption1BtnText()
	{
		return option1BtnText;
	}
	
	void setOption1BtnText( TapReasonPageConf pageConf, Context context, int defaultStringResourceId )
	{
		if ( TextUtils.isEmpty( pageConf.getOption1BtnText() ) )
		{
			this.option1BtnText = context.getString( defaultStringResourceId );
		}
		else
		{
			this.option1BtnText = pageConf.getOption1BtnText();
		}
	}
	
	boolean isOption1BtnTextIsBold()
	{
		return option1BtnTextIsBold;
	}
	void setOption1BtnTextIsBold(boolean option1BtnTextIsBold)
	{
		this.option1BtnTextIsBold = option1BtnTextIsBold;
	}
	boolean isOption1BtnTextShowIcon()
	{
		return option1BtnTextShowIcon;
	}
	void setOption1BtnTextShowIcon(boolean option1BtnTextShowIcon)
	{
		this.option1BtnTextShowIcon = option1BtnTextShowIcon;
	}
	int getOption1BtnTextColor()
	{
		return option1BtnTextColor;
	}
	
	void setOption1BtnTextColor( TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getOption1BtnTextColor() == 0 )
		{
			this.option1BtnTextColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.option1BtnTextColor = pageConf.getOption1BtnTextColor();
		}
	}
	
	int getOption1BtnBGColor()
	{
		return option1BtnBGColor;
	}

	void setOption1BtnBGColor( TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getOption1BtnBGColor() == 0 )
		{
			this.option1BtnBGColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.option1BtnBGColor = pageConf.getOption1BtnBGColor();
		}
	}
	
	int getOption1BtnResourceId()
	{
		return option1BtnResourceId;
	}
	
	String getOption1BtnUrl()
	{
		return option1BtnUrl;
	}
	
	void setOption1BtnDrawable( TapReasonPageConf pageConf, int defaultDrawableResource, Context context )
	{
		if ( !TextUtils.isEmpty( pageConf.getOption1BtnDrawableUrl() ) )
		{
			this.option1BtnUrl = pageConf.getOption1BtnDrawableUrl();
		}
		else
		{
			this.option1BtnUrl = null;
		}	
		this.option1BtnResourceId = defaultDrawableResource;
	}
	
	String getOption2BtnText()
	{
		return option2BtnText;
	}
	
	void setOption2BtnText( TapReasonPageConf pageConf, Context context, int defaultStringResourceId )
	{
		if ( TextUtils.isEmpty( pageConf.getOption2BtnText() ) )
		{
			this.option2BtnText = context.getString( defaultStringResourceId );
		}
		else
		{
			this.option2BtnText = pageConf.getOption2BtnText();
		}
	}
	
	boolean isOption2BtnTextIsBold()
	{
		return option2BtnTextIsBold;
	}
	void setOption2BtnTextIsBold(boolean option2BtnTextIsBold)
	{
		this.option2BtnTextIsBold = option2BtnTextIsBold;
	}
	boolean isShowOption2Btn()
	{
		return showOption2Btn;
	}
	void setShowOption2Btn(boolean showOption2Btn)
	{
		this.showOption2Btn = showOption2Btn;
	}
	int getOption2BtnTextColor()
	{
		return option2BtnTextColor;
	}
	
	void setOption2BtnTextColor( TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getOption2BtnTextColor() == 0 )
		{
			this.option2BtnTextColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.option2BtnTextColor = pageConf.getOption2BtnTextColor();
		}
	}
	
	int getOption2BtnBGColor()
	{
		return option2BtnBGColor;
	}
	
	void setOption2BtnBGColor( TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getOption2BtnBGColor() == 0 )
		{
			this.option2BtnBGColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.option2BtnBGColor = pageConf.getOption2BtnBGColor();
		}
	}
	
	int getOption2BtnResourceId()
	{
		return option2BtnResourceId;
	}
	
	String getOption2BtnUrl()
	{
		return option2BtnUrl;
	}

	void setOption2BtnDrawable( TapReasonPageConf pageConf, int defaultDrawableResource, Context context )
	{
		if ( !TextUtils.isEmpty( pageConf.getOption2BtnDrawableUrl() ) )
		{
			this.option2BtnUrl = pageConf.getOption2BtnDrawableUrl();
		}
		else
		{
			this.option2BtnUrl = null;
		}	
		this.option2BtnResourceId = defaultDrawableResource;
	}
	
	String getOption3BtnText()
	{
		return option3BtnText;
	}

	void setOption3BtnText( TapReasonPageConf pageConf, Context context, int defaultStringResourceId )
	{
		if ( TextUtils.isEmpty( pageConf.getOption3BtnText() ) )
		{
			this.option3BtnText = context.getString( defaultStringResourceId );
		}
		else
		{
			this.option3BtnText = pageConf.getOption3BtnText();
		}
	}	
	
	boolean isOption3BtnTextIsBold()
	{
		return option3BtnTextIsBold;
	}
	void setOption3BtnTextIsBold(boolean option3BtnTextIsBold)
	{
		this.option3BtnTextIsBold = option3BtnTextIsBold;
	}
	boolean isShowOption3Btn()
	{
		return showOption3Btn;
	}
	void setShowOption3Btn(boolean showOption3Btn)
	{
		this.showOption3Btn = showOption3Btn;
	}
	int getOption3BtnTextColor()
	{
		return option3BtnTextColor;
	}
	
	void setOption3BtnTextColor( TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getOption3BtnTextColor() == 0 )
		{
			this.option3BtnTextColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.option3BtnTextColor = pageConf.getOption3BtnTextColor();
		}
	}
	
	int getOption3BtnBGColor()
	{
		return option3BtnBGColor;
	}
	
	void setOption3BtnBGColor( TapReasonPageConf pageConf, int defaultColorResourceId, Context context )
	{
		if ( pageConf.getOption3BtnBGColor() == 0 )
		{
			this.option3BtnBGColor = context.getResources().getColor( defaultColorResourceId );
		}
		else
		{
			this.option3BtnBGColor = pageConf.getOption3BtnBGColor();
		}
	}
	
	boolean shouldShowPoweredByWatermark()
	{
		return TapReason.getConf().isUseLogoWatermark();
	}
}
