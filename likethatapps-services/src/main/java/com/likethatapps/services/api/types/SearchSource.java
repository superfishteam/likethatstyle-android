package com.likethatapps.services.api.types;

/**
 * Created by eladyarkoni on 5/20/14.
 */
public enum SearchSource {

    ALBUM("album"),
    GALLERY("gallery"),
    WEB("web"),
    CAMERA("camera"),
    FTUE("ftue"),
    FEED("feed");

    private String val;

    SearchSource(String val) {
        this.val = val;
    }

    public static SearchSource parse(String val) {
        for (SearchSource searchSource : SearchSource.values()) {
            if (searchSource.toString().equals(val)) {
                return searchSource;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.val;
    }
}
