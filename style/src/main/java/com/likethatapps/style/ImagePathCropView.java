package com.likethatapps.style;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.likethatapps.services.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eladyarkoni on 10/8/15.
 */
public class ImagePathCropView extends View {

    private Bitmap mImage;
    private Path mPath;
    private Paint       mPaint;
    private ImagePathReadyListener mListener;
    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;
    private float mImageRatio = 1;
    private float mCropX1 = -1;
    private float mCropX2 = -1;
    private float mCropY1 = -1;
    private float mCropY2 = -1;
    private List<Point> mPathPoints;

    public ImagePathCropView(Context context) {
        super(context);
        init(context);
    }

    public ImagePathCropView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        mPathPoints = new ArrayList<>();
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.GREEN);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(15);
    }

    public void setImage(Bitmap image, int newWidth, int newHeight) {
        mImageRatio = (float)image.getWidth() / (float)newWidth;
        Log.d("crop-image-activity", "canvas image size: " + newWidth + "X" + newHeight + ", ratio: " + mImageRatio);
        Bitmap scaledImage = ImageUtils.scaleBitmap(image, newWidth, newHeight);
        mImage = scaledImage.copy(Bitmap.Config.ARGB_8888, true);
        invalidate();
    }

    public void setImagePathReadyListener(ImagePathReadyListener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint p = new Paint();
        p.setColor(Color.WHITE);
        if (mImage != null) {
            canvas.drawBitmap(mImage, 0, 0, p);
        }
        if (mPath != null && mPaint != null) {
            canvas.drawPath(mPath, mPaint);
        }
    }

    public interface ImagePathReadyListener {
        public void onImagePathReady(int x1, int y1, int x2, int y2, List<Point> path);
    }

    private void touch_start(float x, float y) {
        mPath.reset();
        if (x < 0) {
            x = 0;
        } else if (x > mImage.getWidth()) {
            x = mImage.getWidth();
        }
        if (y < 0) {
            y = 0;
        } else if (y > mImage.getHeight()) {
            y = mImage.getHeight();
        }
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
        mPathPoints = new ArrayList<>();
        mPathPoints.add(new Point((int)Math.floor(mX * mImageRatio), (int)Math.floor(mY * mImageRatio)));
    }
    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            if (x < 0) {
                x = 0;
            } else if (x > mImage.getWidth()) {
                x = mImage.getWidth();
            }
            if (y < 0) {
                y = 0;
            } else if (y > mImage.getHeight()) {
                y = mImage.getHeight();
            }
            mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
            mX = x;
            mY = y;
            mPathPoints.add(new Point((int)Math.floor(mX * mImageRatio), (int)Math.floor(mY * mImageRatio)));
            if (mCropX1 == -1) {
                mCropX1 = mX;
            } else if (mX < mCropX1) {
                mCropX1 = mX;
            }

            if (mCropX2 == -1) {
                mCropX2 = mX;
            } else if (mX > mCropX2) {
                mCropX2 = mX;
            }

            if (mCropY1 == -1) {
                mCropY1 = mY;
            } else if (mY < mCropY1) {
                mCropY1 = mY;
            }

            if (mCropY2 == -1) {
                mCropY2 = mY;
            } else if (mY > mCropY2) {
                mCropY2 = mY;
            }
        }
    }
    private void touch_up() {
        if (mCropX1 != -1 && mCropX2 != -1 && mCropY1 != -1 && mCropY2 != -1 && mListener != null) {
            mListener.onImagePathReady((int)Math.floor(mCropX1 * mImageRatio),(int)Math.floor(mCropY1 * mImageRatio),(int)Math.floor(mCropX2 * mImageRatio),(int)Math.floor(mCropY2 * mImageRatio), mPathPoints);
            reset();
        }
    }

    private void reset() {
        mCropX1 = -1;
        mCropX2 = -1;
        mCropY1 = -1;
        mCropY2 = -1;
        mPath.reset();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();
                break;
        }
        return true;
    }
}
