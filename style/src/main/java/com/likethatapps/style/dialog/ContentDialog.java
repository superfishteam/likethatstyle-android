package com.likethatapps.style.dialog;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.likethatapps.style.R;

/**
 * Created by eladyarkoni on 5/14/14.
 */
public class ContentDialog extends DialogFragment {

    private static ContentDialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int resournce = getArguments().getInt("viewResource");
        View view = inflater.inflate(resournce, container);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ((ContentDialogListener)getActivity()).onContentDialogViewCreated(view);
        return view;
    }

    public static void show(FragmentActivity activity, int viewResource) {
        dialog = new ContentDialog();
        dialog.setStyle(R.style.DialogTheme, R.style.DialogTheme);
        Bundle b = new Bundle();
        b.putInt("viewResource", viewResource);
        dialog.setArguments(b);
        dialog.show(activity.getSupportFragmentManager(), "");
    }

    public interface ContentDialogListener {
        public void onContentDialogViewCreated(View view);
    }
}
