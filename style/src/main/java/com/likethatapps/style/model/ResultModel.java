package com.likethatapps.style.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by eladyarkoni on 9/8/14.
 */
public class ResultModel {

    private String searchId;
    private boolean wtf;
    List<PhotoModel> photos;
    List<ClusterModel> clusters;
    private String croppedImageUrl;

    public String getSearchId() {
        return searchId;
    }

    public int getStoresCount() {
        int count = 0;
        Map<String, Boolean> map = new HashMap();
        for (PhotoModel p : photos) {
            if (!map.containsKey(p.getMerchantOrSource())) {
                map.put(p.getMerchantOrSource(), true);
                count++;
            }
        }
        return count;
    }

    public List<PhotoModel> getPhotos() {
        return photos;
    }

    public List<ClusterModel> getClusters() {
        return clusters;
    }

    public boolean getWtf() {
        return wtf;
    }

    public String getCroppedImageUrl() {
        return croppedImageUrl;
    }
}
