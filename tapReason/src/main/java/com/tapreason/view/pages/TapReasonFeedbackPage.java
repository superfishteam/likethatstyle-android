package com.tapreason.view.pages;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.tapreason.R;
import com.tapreason.sdk.TapReason;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventResult;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventSubResult;
import com.tapreason.sdk.TapReasonAdvancedListener.TapReasonEventTypes;
import com.tapreason.sdk.TapReasonConfiguration.TapReasonPageConf;
import com.tapreason.sdk.TapReasonCustomEvent.TapReasonCustomEventType;
import com.tapreason.sdk.TapReasonGeneralCons;
import com.tapreason.view.base.TapReasonActivity;
import com.tapreason.view.util.TapReasonUtils;

final class TapReasonFeedbackPage extends TapReasonRulePageBase
{
	private String appPackageToUse;
	
	public TapReasonFeedbackPage( Bundle params, TapReasonEventTypes eventType )
	{
		super( params, eventType, TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_1 );
	}

	@Override
	@SuppressLint("ResourceAsColor")
	protected void configurePage( TapReasonActivity activity )
	{
		try
		{
			TapReasonPageConf pageConf = TapReason.getConf().getUiConfByEventType( getEventType() );			
			TapReasonStyledPageConfiguration conf = new TapReasonStyledPageConfiguration();
			
			// Default initializations
			// Top Icon						
			conf.setTopIconDrawable( pageConf, R.drawable.tap_reason_feedback_4, activity );
			
			// Default selection color
			conf.setSelectionBGColor( pageConf, R.color.tap_reason_default_selected_color, activity );
									
			// Main title
			conf.setMainTitle( pageConf, R.string.tap_reason_feedback_dialog_main_title_text, activity, true );
			
			// Secondary title
			conf.setSecondaryTitle( pageConf, activity, R.string.tap_reason_feedback_dialog_secondary_title_text );
			
			// Option 1 Btn		
			conf.setOption1BtnTextShowIcon( pageConf.isOption1BtnTextShowIcon() );
			conf.setOption1BtnText( pageConf, activity, R.string.tap_reason_feedback_dialog_leave_feedback );
			conf.setOption1BtnTextIsBold( pageConf.isOption1BtnTextIsBold() );
			conf.setOption1BtnDrawable( pageConf, R.drawable.tap_reason_23, activity );

			// Option 2 Btn
			conf.setOption2BtnText( pageConf, activity, R.string.tap_reason_feedback_dialog_leave_negative_feedback );
			conf.setOption2BtnTextIsBold( pageConf.isOption2BtnTextIsBold() );
			conf.setShowOption2Btn( pageConf.isShowOption2Btn() );
			
			// Option 3 Btn
			conf.setOption3BtnText( pageConf, activity, R.string.tap_reason_feedback_dialog_remind_later_option );
			conf.setOption3BtnTextIsBold( pageConf.isOption3BtnTextIsBold() );
			conf.setShowOption3Btn( pageConf.isShowOption3Btn() );
			
			
			switch ( getStyleTypeId() )
			{
				case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_1:
				{
					// Top Icon
					conf.setShowTopIcon( pageConf.isShowTopIcon() );
					
					// Header BG
					conf.setHeaderBGColor( pageConf, R.color.tap_reason_green, activity );
																	
					// Main title text color
					conf.setMainTitleTextColor( pageConf, R.color.tap_reason_white, activity );						

					// Secondary title text color
					conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_white, activity );
					
					// Option 1 Btn				
					conf.setOption1BtnBGColor( pageConf, R.color.tap_reason_white, activity );
					conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_black, activity );

					// Option 2 Btn
					conf.setOption2BtnBGColor( pageConf, R.color.tap_reason_white, activity );
					conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_black, activity );
					
					// Option 3 Btn
					conf.setOption3BtnBGColor( pageConf, R.color.tap_reason_white, activity );
					conf.setOption3BtnTextColor( pageConf, R.color.tap_reason_black, activity );	
					break;
				}
				case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_2:
				{		
					// Top Icon
					conf.setShowTopIcon( true );
					
					// Header BG
					conf.setHeaderBGColor( pageConf, android.R.color.transparent, activity );
					
					// Body BG color
					conf.setBodyBGColor( pageConf, R.color.tap_reason_white, activity );
																	
					// Main title text color
					conf.setMainTitleTextColor( pageConf, R.color.tap_reason_pomegranate_red, activity );
					

					// Secondary title text color
					conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_concrete_grey, activity );
					
					// Option 1 Btn				
					conf.setOption1BtnBGColor( pageConf, R.color.tap_reason_nephritis_green, activity );
					conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_3_btn_style2_btn_text_color, activity );

					// Option 2 Btn
					conf.setOption2BtnBGColor( pageConf, R.color.tap_reason_orange_orange, activity );
					conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_3_btn_style2_btn_text_color, activity );
					
					// Option 3 Btn
					conf.setOption3BtnBGColor( pageConf, R.color.tap_reason_midnight_blue_grey, activity );
					conf.setOption3BtnTextColor( pageConf, R.color.tap_reason_3_btn_style2_btn_text_color, activity );								
					break;
				}	
				case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_1:
				{
					// Top Icon
					conf.setShowTopIcon( pageConf.isShowTopIcon() );
					
					// Main title text color
					conf.setMainTitleTextColor( pageConf, R.color.tap_reason_white, activity );						

					// Secondary title text color
					conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_black, activity );				

					// Header BG
					conf.setHeaderBGColor( pageConf, R.color.tap_reason_green, activity );
					
					// Body BG color
					conf.setBodyBGColor( pageConf, R.color.tap_reason_white, activity );						
					
					// Option 1 Btn				
					conf.setOption1BtnBGColor( pageConf, R.color.tap_reason_green, activity );
					conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_white, activity );

					// Option 2 Btn
					conf.setOption2BtnBGColor( pageConf, android.R.color.transparent, activity );
					conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_black, activity );	
					conf.setOption2BtnDrawable( pageConf, R.drawable.tap_reason_29, activity );
					conf.setOption2BtnText( pageConf, activity, R.string.tap_reason_feedback_dialog_remind_later_option );
			
					conf.setOptionsSeparatorText( pageConf, activity, R.string.tap_reason_options_separator_text );					
					break;
				}
				case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_2:
				{
					conf.setMainTitle( pageConf, R.string.tap_reason_feedback_dialog_main_title_text, activity, false );
					
					// Main title text color
					conf.setMainTitleTextColor( pageConf, R.color.tap_reason_black, activity );								

					// Secondary title text color
					conf.setSecondaryTitleTextColor( pageConf, R.color.tap_reason_black, activity );				

					// Header BG
					conf.setHeaderBGColor( pageConf, android.R.color.transparent, activity );
					
					// Body BG color
					conf.setBodyBGColor( pageConf, R.color.tap_reason_white, activity );						
					
					// Option 1 Btn				
					conf.setOption1BtnTextColor( pageConf, R.color.tap_reason_midnight_blue_grey, activity );
					conf.setOption1BtnDrawable( pageConf, R.drawable.tap_reason_feedback_4, activity );

					// Option 2 Btn
					conf.setOption2BtnTextColor( pageConf, R.color.tap_reason_midnight_blue_grey, activity );	
					conf.setOption2BtnDrawable( pageConf, R.drawable.tap_reason_29, activity );
					conf.setOption2BtnText( pageConf, activity, R.string.tap_reason_rate_dialog_remind_later_option );					
					break;
				}				
			}
			
			configurePage( activity, conf );			
		}
		catch ( Exception exception )
		{
			Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "Error parsing feedback page configuration", exception );
		}
	}

	@Override
	public void onClick(TapReasonActivity activity, View v)
	{
		switch ( getStyleTypeId() )
		{
			case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_1:
			case TapReasonPageConf.UI_TEMPLATE_TWO_BUTTONS_STYLE_2:				
			{
				activity.finish();
				
				int id = v.getId();
				if ( id == R.id.tapReasonOption1BtnLayout )
				{
					onLeaveFeedbackClick( activity );
				}
				else if ( id == R.id.tapReasonOption2BtnLayout )
				{		
					onRemindMeLaterClick( activity );
				}						
				break;
			}
			case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_1:
			case TapReasonPageConf.UI_TEMPLATE_THREE_BUTTONS_STYLE_2:
			{
				activity.finish();
				
				int id = v.getId();
				if ( ( id == R.id.tapReasonOption1Btn ) || ( id == R.id.tapReasonOption2Btn ))	
				{	
					try
					{			
						notifyEventResult( TapReasonEventTypes.FEEDBACK_EVENT, TapReasonEventResult.POSITIVE );
						
						if ( id == R.id.tapReasonOption1Btn )
						{
							TapReason.reportEventResult( TapReasonEventTypes.FEEDBACK_EVENT, TapReasonEventResult.POSITIVE, TapReasonEventSubResult.POSITIVE );
							TapReason.reportCustomEvent( TapReasonCustomEventType.POSITIVE_FEEDBACK, activity );
						}
						else if ( id == R.id.tapReasonOption2Btn )
						{
							TapReason.reportEventResult( TapReasonEventTypes.FEEDBACK_EVENT, TapReasonEventResult.POSITIVE, TapReasonEventSubResult.NEGATIVE );
							TapReason.reportCustomEvent( TapReasonCustomEventType.NEGATIVE_FEEDBACK, activity );					
						}
					
						boolean userOverrideDefaultAction = false;
						
						if ( TapReason.getConf().getOnEventResultListener() != null  )
						{
							userOverrideDefaultAction = TapReason.getConf().getOnEventResultListener().onTapReasonUserFeedback( TapReasonEventTypes.FEEDBACK_EVENT );
						}
						
						if ( !userOverrideDefaultAction )
						{
							String title = null;
							
							if ( id == R.id.tapReasonOption1Btn )
							{
								title = activity.getString( R.string.tap_reason_feedback_send_email_positive_feedback, TapReason.getConf().getAppName() );				
							}
							else if ( id == R.id.tapReasonOption2Btn )
							{
								title = activity.getString( R.string.tap_reason_feedback_send_email_negative_feedback, TapReason.getConf().getAppName() );				
							}
							
							Intent emailIntent = TapReasonUtils.generateFeedbackEmailIntent( activity.getPackageManager(), appPackageToUse, title );
							
							if ( emailIntent == null )
							{
								TapReason.reportEventResult( getEventType(), TapReasonEventResult.ERROR, TapReasonEventSubResult.ERROR_APP_PACKAGE_DOESNT_EXIST );
							}
							else
							{
								activity.startActivity(emailIntent);								
							}
						}
					}
					catch( Throwable tr )
					{
						Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "Error while trying to get feedback from user", tr );
					}		
				}
				else if ( id == R.id.tapReasonOption3Btn )
				{		
					onRemindMeLaterClick( activity );
				}
				break;
			}
		}
	}
	
	@Override
	public boolean parseBundle()
	{
		boolean validData = true;
		
		Bundle params = getParams();
		
		if ( params == null || params.isEmpty() )
		{
			validData = false;			
			Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "FeedbackAppEvent - Please make sure that params bundle exists in the intent" );				
		}
		else
		{
			// App Package
			if ( params.containsKey( TapReasonGeneralCons.INTENT_APP_PACKAGE_NAME_PARAM ) )
			{
				appPackageToUse = params.getString( TapReasonGeneralCons.INTENT_APP_PACKAGE_NAME_PARAM );
			}
			
			if ( TextUtils.isEmpty( appPackageToUse ) )
			{
				validData = false;
				
				// Now package to trigger
				Log.e( TapReasonUtils.TAP_REASON_SDK_LOG_TAG, "FeedbackAppEvent - No app package was provided" );
			}
		}
		
		return validData;
	}
}
