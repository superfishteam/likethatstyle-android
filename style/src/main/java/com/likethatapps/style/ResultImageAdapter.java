package com.likethatapps.style;

import android.app.Activity;
import android.graphics.Color;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.likethatapps.style.model.PhotoModel;
import com.likethatapps.style.service.WatchListService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by eladyarkoni on 7/5/15.
 */
public class ResultImageAdapter extends BaseAdapter {

    ResultImageAdapterActivity activity;
    List<PhotoModel> photos;
    boolean hasHeader;
    boolean hasFooter;
    boolean isFirstImageLoaded = false;
    boolean askExpertFeature = false;
    public static final int SORT_BY_SIMILARITY = 0;
    public static final int SORT_BY_PRICE_LOW_TO_HIGH = 1;
    public static final int SORT_BY_PRICE_HIGH_TO_LOW = 2;
    private static final int COLUMN_TYPE_RESULT_IMAGE = 0;
    private static final int COLUMN_TYPE_ASK_EXPERT = 1;

    public ResultImageAdapter(ResultImageAdapterActivity activity, List<PhotoModel> photos, boolean header, boolean footer, boolean askExpertFeature) {
        this.activity = activity;
        this.hasFooter = footer;
        this.hasHeader = header;
        this.askExpertFeature = askExpertFeature;
        this.photos = photos;
    }

    public void update(List<PhotoModel> photos) {
        this.photos = photos;
        notifyDataSetChanged();
    }

    public void sortBy(int sortBy) {
        if (this.photos != null && this.photos.size() > 0) {
            List<PhotoModel> newList = new ArrayList<>();
            newList.addAll(this.photos);
            switch (sortBy) {
                case SORT_BY_SIMILARITY:
                    Collections.sort(newList, new Comparator<PhotoModel>() {
                        @Override
                        public int compare(PhotoModel lhs, PhotoModel rhs) {
                            return lhs.getDistance().compareTo(rhs.getDistance());
                        }
                    });
                    break;
                case SORT_BY_PRICE_LOW_TO_HIGH:
                    Collections.sort(newList, new Comparator<PhotoModel>() {
                        @Override
                        public int compare(PhotoModel lhs, PhotoModel rhs) {
                            return lhs.getPrice().compareTo(rhs.getPrice());
                        }
                    });
                    break;
                case SORT_BY_PRICE_HIGH_TO_LOW:
                    Collections.sort(newList, new Comparator<PhotoModel>() {
                        @Override
                        public int compare(PhotoModel lhs, PhotoModel rhs) {
                            return rhs.getPrice().compareTo(lhs.getPrice());
                        }
                    });
                    break;
            }
            this.photos = newList;
            this.notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        if (hasHeader) {
            if (askExpertFeature) {
                return this.photos.size();
            } else {
                return this.photos.size() - 1;
            }
        }
        return this.photos.size() + (askExpertFeature ? 1 : 0);
    }

    @Override
    public Object getItem(int i) {
        if (hasHeader) {
            return photos.get(i-1);
        } else {
            return photos.get(i);
        }
    }

    @Override
    public int getViewTypeCount() {
        if (askExpertFeature) {
            return 2;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int i) {
        if (askExpertFeature) {
            return i == (getCount() - 1) ? COLUMN_TYPE_ASK_EXPERT : COLUMN_TYPE_RESULT_IMAGE;
        } else {
            return COLUMN_TYPE_RESULT_IMAGE;
        }
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        int columnType = getItemViewType(i);
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from((Activity)activity);
            if (columnType == COLUMN_TYPE_RESULT_IMAGE) {
                convertView = layoutInflator.inflate(R.layout.srp_image_layout, null);
                holder = new ViewHolder();
                holder.image = (ImageView) convertView.findViewById(R.id.imageView);
                holder.imageContainer = (RelativeLayout) convertView.findViewById(R.id.image_container);
                holder.source = (TextView) convertView.findViewById(R.id.source);
                holder.price = (TextView) convertView.findViewById(R.id.price);
                holder.watchListBtn = (ImageButton) convertView.findViewById(R.id.watch_btn);
                convertView.setTag(holder);
            } else {
                convertView = layoutInflator.inflate(R.layout.ask_expert_result_column, null);
                holder = new ViewHolder();
                holder.askExpertButton = (Button)convertView.findViewById(R.id.ask_expert_btn);
                convertView.setTag(holder);
            }
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (columnType == COLUMN_TYPE_ASK_EXPERT) {
            holder.askExpertButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onAskExpertButtonClicked();
                }
            });
            return convertView;
        } else {
            final PhotoModel photo = this.photos.get(i);
            if (photo.getThumbUrl() == null) {
                convertView.setVisibility(View.INVISIBLE);
            } else {
                convertView.setVisibility(View.VISIBLE);
            }
            holder.image.setBackgroundColor(Color.parseColor("#bdbdbd"));
            if (photo.getWidth() > 0 && photo.getHeight() > 0) {
                Display display = ((Activity)activity).getWindowManager().getDefaultDisplay();
                float width = 0;
                float ratio = 1;
                float height = 0;
                if (photo.getWidth() < 250) {
                    width = photo.getWidth();
                    ratio = 1;
                    height = photo.getHeight();
                } else {
                    width = display.getWidth() / 2;
                    ratio = (photo.getWidth() / width);
                    height = photo.getHeight() / (ratio == 0 ? 1 : ratio);
                }
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(Math.round(width), Math.round(height));
                lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                holder.image.setLayoutParams(lp);
            }
            StyleApplication.imageLoader.DisplayImage(photo.getThumbUrl(), holder.image);
            if (!isFirstImageLoaded) {
                isFirstImageLoaded = true;
                activity.onFirstImageLoaded();
            }

            holder.source.setText(photo.getMerchantOrSource());
            if (photo.getPrice() > 0) {
                holder.price.setText("$" + photo.getPrice().intValue());
            }
            holder.watchListBtn.setSelected(WatchListService.getInstance().isInWatchList(photo));
            holder.watchListBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (WatchListService.getInstance().isInWatchList(photo)) {
                        WatchListService.getInstance().remove(photo);
                        holder.watchListBtn.setSelected(false);
                        activity.onWatchListItemRemoved(photo);
                    } else {
                        WatchListService.getInstance().add(photo);
                        holder.watchListBtn.setSelected(true);
                        activity.onWatchListItemInserted(photo);
                    }
                }
            });
            if ((i == this.photos.size() - 2)) {
                activity.onReachedBottomList();
            }
            return convertView;
        }
    }

    public class ViewHolder {
        ImageView image;
        RelativeLayout imageContainer;
        TextView source;
        TextView price;
        ImageButton watchListBtn;
        Button askExpertButton;
    }

    public interface ResultImageAdapterActivity {
        public void onWatchListItemInserted(PhotoModel photo);
        public void onWatchListItemRemoved(PhotoModel photo);
        public void onReachedBottomList();
        public void onFirstImageLoaded();
        public void onAskExpertButtonClicked();
    }
}
