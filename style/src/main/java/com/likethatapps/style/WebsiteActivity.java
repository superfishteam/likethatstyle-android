package com.likethatapps.style;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.likethatapps.style.model.PhotoModel;
import com.likethatapps.style.service.ReportService;
import com.tapreason.sdk.TapReasonAnnotations;
import com.tapreason.sdk.TapReasonCustomEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by eladyarkoni on 4/28/15.
 */
@TapReasonAnnotations.TapReasonDontInvokeEventsForListener
public class WebsiteActivity extends StyleBaseActivity {

    private String mUrl;
    private WebView mWebView;
    private PhotoModel mPhotoModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreateBaseActivity(this, savedInstanceState, R.layout.activity_website);
        mPhotoModel = (PhotoModel)getIntent().getSerializableExtra(Consts.EXTRA_DATA_PHOTO_MODEL_SERIALIZABLE);
        mUrl = mPhotoModel.getPageUrl();
        mWebView = (WebView)findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        final long startTime = System.currentTimeMillis();
        mWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, final String url) {
                Map<String,String> extraData = new HashMap<>();
                extraData.put("Time", (System.currentTimeMillis() - startTime)+"");
                extraData.put("Url", mUrl);
                ReportService.getInstance().report(3053, "webpage_loading_time_event", null, mPhotoModel.getStatsImageId(), extraData);
                findViewById(R.id.loading).setVisibility(View.GONE);
            }
        });
        mWebView.loadUrl(mUrl);
        updateBarTitle(mPhotoModel.getTitle());
    }

    @Override
    protected boolean hasCameraSearchBtn() {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected String getBarTitle() {
        return mPhotoModel == null ? getString(R.string.app_name) : mPhotoModel.getTitle();
    }
}
