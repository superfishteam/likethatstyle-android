package com.likethatapps.style;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;

import com.likethatapps.style.service.ReportService;
import com.tapreason.sdk.TapReason;
import com.tapreason.sdk.TapReasonAdvancedListener;
import com.tapreason.sdk.TapReasonCustomEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by eladyarkoni on 7/7/15.
 */
public class HomeActivity extends StyleBaseActivity {

    private View mRatingBarSurface;
    private Button mRateBtn;
    private View mHomepageTitle;
    private View mRateWhiteGradient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        onCreateBaseActivity(this, savedInstanceState, R.layout.activity_home);
        mRatingBarSurface = findViewById(R.id.rating_bar_surface);
        mRateBtn = (Button)findViewById(R.id.rating_btn);
        mHomepageTitle = findViewById(R.id.homepage_title);
        mRateWhiteGradient = findViewById(R.id.rate_white_gradient);
    }

    public void hamburgerIconClicked(View view) {
        onPressingHamburgerButton();
    }

    public void eyeIconClicked(View view) {
        navigateToActivity(WatchListActivity.class, null, false, -1);
    }

    public void useYourPhotos(View v) {
        ReportService.getInstance().report(3063, "home_cameraroll_tapped");
        selectPhotoFromGallery();
    }

    public void takeAPhoto(View v) {
        ReportService.getInstance().report(3064, "home_camera_tapped");
        navigateToActivity(CameraActivity.class, null, false, -1);
    }

    @Override
    protected boolean hasCameraSearchBtn() {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ReportService.getInstance().report(3023, "home_screen_opened");

        boolean isUserRated = StyleApplication.isUserRated();
        boolean userDoesntLikeThisApp = StyleApplication.userDoesntLikeThisApp();
        long startAppCounter = StyleApplication.getCounter(StyleApplication.COUNTER_START_APP);
        if (!isUserRated && !userDoesntLikeThisApp && startAppCounter > 1) {
            mHomepageTitle.setVisibility(View.GONE);
            mRatingBarSurface.setVisibility(View.VISIBLE);
            mRateBtn.setVisibility(View.INVISIBLE);
            mRateWhiteGradient.setVisibility(View.VISIBLE);
            ReportService.getInstance().report(3068, "rate_home_viewed");
        } else {
            mRateWhiteGradient.setVisibility(View.GONE);
            mHomepageTitle.setVisibility(View.VISIBLE);
            mRatingBarSurface.setVisibility(View.GONE);
        }
        final RatingBar ratingBar = (RatingBar) findViewById(R.id.rating_bar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Map<String, String> extraData = new HashMap<String, String>();
                extraData.put("Rating", Math.round(ratingBar.getRating()) + " of stars");
                ReportService.getInstance().report(3069, "rate_home_star_clicked", null, null, extraData);
                mRateBtn.setVisibility(View.VISIBLE);
                if (ratingBar.getRating() >= 4) {
                    mRateBtn.setText(R.string.home_screen_review);
                } else {
                    mRateBtn.setText(R.string.home_screen_feedback);
                }
            }
        });
        mRateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ratingBar.getRating() >= 4) {
                    TapReason.reportCustomEvent(TapReasonCustomEvent.TapReasonCustomEventType.POSITIVE_FEEDBACK,HomeActivity.this);
                    Map<String, String> extraData = new HashMap<String, String>();
                    extraData.put("Rating", Math.round(ratingBar.getRating()) + " of stars");
                    ReportService.getInstance().report(3070, "rate_home_review_clicked", null, null, extraData);
                    StyleApplication.startRate(HomeActivity.this);
                } else {
                    TapReason.reportCustomEvent(TapReasonCustomEvent.TapReasonCustomEventType.NEGATIVE_FEEDBACK, HomeActivity.this);
                    Map<String, String> extraData = new HashMap<String, String>();
                    extraData.put("Rating", Math.round(ratingBar.getRating()) + " of stars");
                    ReportService.getInstance().report(3071, "rate_home_feedback_clicked", null, null, extraData);
                    StyleApplication.sendFeedback(HomeActivity.this, true);
                }

            }
        });
    }

    @Override
    protected boolean showBar() {
        return false;
    }

    @Override
    protected int barColor() {
        return getResources().getColor(R.color.black);
    }
}
